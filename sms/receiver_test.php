<?php
error_reporting(E_ALL&~E_NOTICE);
error_reporting(false);
require_once("lib/config.php");
require_once("lib/functions.php");
require_once("lib/sbMysqlPDO.class.php");
$conn = null;
try{
    $conn = new sbMysqlPDO($server, $user, $password, $db);
    $conn->doSelect("SET NAMES 'UTF8'");
    
}catch(Exception $e){
    file_put_contents($realPath."log/{$mod}.txt","\nError when connect to database server|".date("Y-m-d H:i:s",time()));
    sleep(5);
}

$data['sender'] = intval($_REQUEST['sender']);
$data['service'] = intval($_REQUEST['service']);
$data['content'] = $_REQUEST['content'];
$commandCode = explode(" ", $data['content']);
$data['command_code'] = $commandCode[0];
$data['command_code'] = trim($data['content']);
$data['status'] = 0;
$data['created_datetime'] = date("Y-m-d H:i:s",time());

if($data['sender']&&$data['service']&&$data['content']&&$data['command_code']){
    
    $sqlSelectShortcode = " SELECT * FROM shortcode WHERE shortcode='{$data['service']}'";
    $shortcode = $conn->doSelectOne($sqlSelectShortcode);
    
    $sqlInsert = "
                    INSERT INTO sms_mo(sender,service,content,command_code,status,created_datetime)
                    VALUES('{$data['sender']}','{$data['service']}','{$data['content']}','{$data['command_code']}','{$data['status']}','{$data['created_datetime']}')
                 ";
    $data['id'] = $conn->doUpdate($sqlInsert);
    //OFF ALL
    if(strtolower($data['command_code'])=='off all'){
        //$sql = "UPDATE member set status=3,cancel_date=now() WHERE msisdn='{$data['sender']}' AND status=1 ";
        //$conn->doUpdate($sql);
        //echo "Tharn dai yok lerk bo li karn ni leo, tong karn long tha bien nam tho 1920. kor korb chai!";
        //exit();
    }

    $cmdLower = strtolower($data['command_code']);
    $cmdUpper = strtoupper($data['command_code']);
    $sqlSelectCommand = "
                            SELECT command_code.*,service.price,service.type as service_type,service.service as service_name
                            FROM command_code INNER JOIN service ON command_code.service_id=service.id
                            WHERE  shortcode='{$data['service']}' AND (command_code.command_code='{$cmdLower}' OR command_code.command_code='{$cmdUpper}'
                                   OR command_code.command_code_alias LIKE '%[{$cmdLower}]%'
                                   OR command_code.command_code_alias LIKE '%[{$cmdUpper}]%' )
                         ";
    $command = $conn->doSelectOne($sqlSelectCommand);

    usleep($_waiting_process_time);
    $updateMoStatus = true;
    if($command){
       $_service_id = $command['service_id'];
       switch($command['type']){
           case '1':
               //if($data['ServiceID']!='8090'){
               //if($_promotion_status!=1||($_promotion_status==1&&$data['service']!='8090')){
               if(1){
                   //ON command
                   $shortcodePrice = $serviceShortCodePrice[$data['service']];
                   $servicePrice = $command['price'];
                   if($command['service_type']==1){
                       //Daily
                       $totalDayBonus = ceil($shortcodePrice/$servicePrice);
                   }else{
                       //Monthy
                       $totalDayBonus = ceil(($shortcodePrice/$servicePrice)*30);
                   }
                   $sqlCheckMemberExists = "
                                                SELECT * FROM member
                                                WHERE msisdn='{$data['sender']}'
                                                      AND service_id='{$command['service_id']}'
                                                      AND (status=1 OR status=2)
                                           ";
                   $member = $conn->doSelectOne($sqlCheckMemberExists);
                   usleep($_waiting_process_time);
                   if($member){
                        
                   }else{
                       //Register for member
                        $registerDate = $startChargingDate = date("Y-m-d H:i:s",time());
                        $endChargingDate = time()+24*3600*$totalDayBonus;
                        $endChargingDate = date('Y-m-d H:i:s',$endChargingDate);
                        $sqlInsertMember = "
                                                 INSERT INTO member(
                                                                        msisdn,service_id,register_date,
                                                                        start_charging_date,end_charging_date,status,
                                                                        total_money
                                                                    )
                                                 VALUES(
                                                            '{$data['sender']}','{$command['service_id']}','{$registerDate}',
                                                            '{$startChargingDate}','{$endChargingDate}',1,
                                                            '{$shortcodePrice}'
                                                       )
                                           ";
                        while(1){
                            try{
                                $conn->doUpdate($sqlInsertMember);
                                usleep($_waiting_process_time);
                                break;
                            }catch(Exception $e){
                                $errorMsg = "\n Register error|101|{$data['sender']}|".date("Y-m-d H:i:s",time());
                                file_put_contents($realPath."log/{$mod}.txt",$errorMsg);
                                try{
                                    $conn = new sbMysqlPDO($server, $user, $password, $db);
                                }catch(Exception $e){
                                    file_put_contents($realPath."log/{$mod}.txt","\nError when connect to database server|".date("Y-m-d H:i:s",time()));
                                    sleep(1);
                                }
                            }
                        }
                        $sqlSelectMember = " SELECT * FROM member WHERE msisdn='{$data['sender']}' AND service_id='{$command['service_id']}' AND status=1 ";
                        $member = $conn->doSelectOne($sqlSelectMember);
                        usleep($_waiting_process_time);
                   }
                   //Insert charging history
                   /*
                   $sqlInsertChargingHistory = "
                                                    INSERT INTO charging_history(
                                                                member_id,start_charging_date,end_charging_date,
                                                                total_money,service_id,mo_id,service_name
                                                                )
                                                    VALUES(
                                                            '{$member['id']}','{$startChargingDate}','{$endChargingDate}',
                                                            '{$shortcodePrice}','{$member['service_id']}','{$data['id']}','{$command['service_name']}'
                                                          )
                                                ";
                   $conn->doUpdate($sqlInsertChargingHistory);
                    * 
                    */
                   usleep($_waiting_process_time);
                   //Send message to customer
                   $_reply_content = $command['reply_content'];
                   
                   //Select the latest content to send to customer
                   $sqlSelectLatestContent = "
                                                SELECT * FROM content WHERE service_id = {$command['service_id']} ORDER BY id DESC
                                             ";
                   $content = $conn->doSelectOne($sqlSelectLatestContent);
                   if($content){
                        $sendContent = $content['content'];
                        $sentTime = date("Y-m-d H:i:s",time());
                        $sqlInsertSMSContent = "
                                                    INSERT INTO sms_mt(sender,receiver,service,command_code,content_type,content,send_datetime,status,charging_status)
                                                    VALUES('{$data['service']}','{$data['sender']}','{$data['service']}','Free','0',
                                                           '{$sendContent}','{$sentTime}',0,1)
                                                 ";
                         $conn->doUpdate($sqlInsertSMSContent);
                   }
                    usleep($_waiting_process_time);
                   
               }else{
                   $updateMoStatus = false;
               }
               break;
           case '2':
               //OFF
               $sqlCheckMemberExists = "
                                            SELECT * FROM member
                                            WHERE msisdn='{$data['sender']}' AND service_id='{$command['service_id']}' AND (status=1 OR status=2)
                                       ";

               $member = $conn->doSelectOne($sqlCheckMemberExists);
               if($member){
                   $sentTime = date("Y-m-d H:i:s",time());
                   $sqlUpdate = "
                                    UPDATE member set status=3,cancel_date='{$sentTime}' WHERE id='{$member['id']}'
                                ";
                   $conn->doUpdate($sqlUpdate);
                   $_reply_content = $command['reply_content'];
               }else{
                   $_reply_content = $_sms_not_register;
                   $sentTime = date("Y-m-d H:i:s",time());
               }
               break;
           case '4':
               //FREE
               break;
           case '5':
               //HELP
               $_reply_content = $command['reply_content'];
               break;
           case '6':
               //CHARGING MO
               $_reply_content = $command['reply_content'];
               $chargingStatus = 0;
               break;
           default:
               //HELP
               $_reply_content = $command['reply_content'];
               break;
       }
       //Update status MO
       $sqlUpdateMo = "UPDATE sms_mo SET charging_status=2 WHERE id='{$data['id']}' ";
       $conn->doUpdate($sqlUpdateMo);
    }else{
       //HELP FOR INVALID COMMAND CODE
       /*
       if($_promotion_status==1&&$data['service']=='8090'){
           echo "\n Promotion program, don't process this MO";
       }else{
        *
        */
        //Check voting
        
        $votingKeyword = explode("*",$data['content']);
        if(!$votingKeyword[1]){
            $votingKeyword = explode("*",preg_replace('/\s+/', '*', $data['content']));
        }
        $votingKeyword = $votingKeyword[0];
        $cmdLower = strtolower($votingKeyword);
        $cmdUpper = strtoupper($votingKeyword);
        $sqlVoting =  " SELECT voting_keyword.*,service.start_time,service.end_time,service.invalid_content, service.price,service.service as service_name,"
                    . " service.shortcode_charging,service.shortcode,service.max_vote_time, service.max_money, service.has_postpad_vote "
                    . " FROM voting_keyword INNER JOIN service ON service.id=voting_keyword.service_id"
                    . " WHERE  shortcode='{$data['service']}' AND service.type = 7 AND (keyword = '$cmdLower' OR keyword = '$cmdUpper')  "
                    . " ORDER BY id DESC LIMIT 1 ";
        $keyword = $conn->doSelectOne($sqlVoting);
        if($keyword){
            if(strtotime($keyword['start_time'])<=time() && strtotime($keyword['end_time'])>=time()){
                $listKeyword = explode("*",$data['content']);
                if(!$listKeyword[1]) $listKeyword = explode("*",preg_replace('/\s+/', '*', $data['content']));
                //Check Max vote time and max money
                
                //Check member balance
                $msisdn = substr($data['sender'],5);
                $memberBalance = viewBalance($msisdn);
                if($keyword['shortcode_charging'])
                {
                    //var_dump($listKeyword);
                    $chargingStatus=0;
                    if($listKeyword[1]>1){
                        $totalVote = $listKeyword[1];
                        $totalMoney = $totalVote*$keyword['price'];
                        $totalMoneyCharge = ($totalVote-1)*$keyword['price'];
                        $chargingStatus=0;
                    }else{
                        $totalVote = 1;
                        $totalMoney = $keyword['price'];
                        $chargingStatus=1;
                    }
                    $oldBalance = $memberBalance+$keyword['price'];
                    $newBalance = $memberBalance-$totalMoneyCharge;
                }else{
                    //var_dump($listKeyword);
                    $chargingStatus=0;
                    if($listKeyword[1]>1){
                        $totalVote = $listKeyword[1];
                        $totalMoney = $totalVote*$keyword['price'];
                        $totalMoneyCharge = $totalVote*$keyword['price'];
                    }else{
                        $totalVote = 1;
                        $totalMoney = $keyword['price'];
                        $totalMoneyCharge = $totalVote*$keyword['price'];
                    }
                    $oldBalance = $memberBalance;
                    $newBalance = $oldBalance-$totalMoneyCharge;
                }
                
                $msisdn = substr($data['sender'],5);
                $subInfo = viewInfo($msisdn);
                //Check Max vote time and max money
                if(($listKeyword[1]>$keyword['max_vote_time'])||$totalMoneyCharge>$keyword['max_money']){
                     $_reply_content = "You can vote maximum {$keyword['max_vote_time']} or {$keyword['max_money']} KIP per time";
                     $sqlUpdateMo = "UPDATE sms_mo SET charging_status=2 WHERE id='{$data['id']}' ";
                     $conn->doUpdate($sqlUpdateMo);
                }else if($listKeyword[1]>1&&$subInfo[0]==9&&!$keyword['has_postpad_vote']){
                     $_reply_content = "Sorry we have not allowed postpaid voting many times. ";
                     $sqlUpdateMo = "UPDATE sms_mo SET charging_status=2 WHERE id='{$data['id']}' ";
                     $conn->doUpdate($sqlUpdateMo);
                }else{
                    //var_dump($subInfo);
                    if(!$chargingStatus){
                        if($subInfo[0]>=1&&$subInfo[0]<=6){
                            //Pre paid         
                            //$memberBalance = viewBalance($msisdn);
                            if($memberBalance>$totalMoneyCharge){
                                $result = charge($msisdn,$totalMoneyCharge, $subInfo[0]);
                                if($result==1){
                                    //Charge successully
                                    $votingStatus=1;
                                }else{
                                    //Not enought money
                                    $totalVoteCanBe = intval(ceil($memberBalance/ $keyword['price'])) -1 ;
                                    $notEnoughtMoney = true;
                                    $votingStatus=0;
                                }
                            }else{
                                //Not enought money
                                $totalVoteCanBe = intval(ceil($memberBalance/ $keyword['price']))-1;
                                $notEnoughtMoney = true;
                                $votingStatus=0;
                            }
                        }else if($subInfo[0]==9){
                            //Post paid
                            $votingStatus=1;
                        }else{
                            $votingStatus=1;
                        }
                    }else{
                        $votingStatus=1;
                    }
                    if($votingStatus==1){
                        if($memberBalance==0 || $subInfo[0]==9 ){
                            $oldBalance = 0;
                            $newBalance = 0;
                        }
                        //Insert charging history & update mo status
                        /*
                        for($i=1;$i<=$totalVote;$i++){
                            $sqlInsertKeyword = " INSERT INTO voting_log (msisdn,keyword,keyword_id,mo_id,created_datetime)"
                                                . "VALUES('{$data['sender']}','{$data['content']}','{$keyword['id']}','{$data['id']}',now())";
                            $conn->doUpdate($sqlInsertKeyword);
                            $sqlUpdateVote = " UPDATE voting_keyword SET total_vote=total_vote+1 WHERE id='{$keyword['id']}'";
                            $conn->doUpdate($sqlUpdateVote);
                        }
                        */
                        $sqlInsertKeyword = " INSERT INTO voting_log (msisdn,keyword,keyword_id,mo_id,created_datetime,total_vote,sub_type)"
                                            . "VALUES('{$data['sender']}','{$data['content']}','{$keyword['id']}','{$data['id']}',now(),'{$totalVote}','{$subInfo[0]}')";
                        $conn->doUpdate($sqlInsertKeyword);
                        $sqlUpdateVote = " UPDATE voting_keyword SET total_vote=total_vote+$totalVote WHERE id='{$keyword['id']}'";
                        $conn->doUpdate($sqlUpdateVote);

                        $sqlInsertChargingHistory = "
                                                    INSERT INTO charging_history(
                                                                    msisdn,start_charging_date,end_charging_date,
                                                                    total_money,service_id,mo_id,service_name,charging_type,sub_type,shortcode,
                                                                    keyword,old_balance,new_balance,total_vote
                                                    )
                                                    VALUES(
                                                                '{$data['sender']}',now(),now(),
                                                                '{$totalMoney}','{$keyword['service_id']}','{$data['id']}','{$keyword['service_name']}',2,'{$subInfo[0]}','{$keyword['shortcode']}',
                                                                '{$data['content']}','{$oldBalance}','{$newBalance}','{$totalVote}'
                                                    )
                                                ";
                        $conn->doUpdate($sqlInsertChargingHistory);
                        //UPDATE sms_mo
                        $sqlUpdateMo = "UPDATE sms_mo SET charging_status=1,total_money='{$totalMoney}' WHERE id='{$data['id']}' ";
                        $conn->doUpdate($sqlUpdateMo);

                        $_reply_content = $keyword['reply_content']." ($totalVote sms) ";
                    }else{
                        if($totalVoteCanBe<=0) $totalVoteCanBe=0;
                        $_reply_content = "Sorry your balance is not enought money, you can voting only {$totalVoteCanBe}sms.";
                        $sqlUpdateMo = "UPDATE sms_mo SET charging_status=99 WHERE id='{$data['id']}' ";
                        $conn->doUpdate($sqlUpdateMo);
                    }
                }
            }else{
                $_reply_content = $keyword['invalid_content'];
                $sqlUpdateMo = "UPDATE sms_mo SET charging_status=2 WHERE id='{$data['id']}' ";
                $conn->doUpdate($sqlUpdateMo);
            }
            $_service_id = $keyword['service_id'];
        }else{
            //Check keyword
            $sql="SELECT * FROM service WHERE type=6 AND shortcode='{$data['service']}'";
            $service = $conn->doSelectOne($sql);
            preg_match_all('!\d+!', $data['content'], $matches);
            if(intval($matches[0][0])>0&&$service){
               //Charging keyword
               preg_match_all('!\d+!',$data['content'], $matches);
               //var_dump($matches);
               $money = intval($matches[0][0]);               
               //echo $money; 
               if($money>0){
                   $msisdn = substr($data['sender'],5);
                   $info = viewInfo($msisdn);
                   $memberBalance = viewBalance($msisdn);
                   //var_dump($info);
                   if($info[0]>=1&&$info[0]<=6){
                        //Pre paid         
                        $subType=1;
                    }else if($info[0]===0){
                        //Post paid
                        $subType = 0;
                    }
                    $subType = $info[0];
                    //Charge money
                    $result = charge($msisdn, $money, $subType);
                    if($result==1) {
                        //Charge successfully
                        //usleep(500000);
                        //echo "\n Insert charging history";
                        //Insert charging history
                        $oldBalance = $memberBalance;
                        $newBalance = $memberBalance-$money;
                        if($memberBalance==0){
                            $oldBalance = 0;
                            $newBalance = 0;
                        }
                        $sqlInsertChargingHistory = "
                                                        INSERT INTO charging_history(
                                                                        msisdn,start_charging_date,end_charging_date,
                                                                        total_money,service_id,mo_id,service_name,charging_type,sub_type,shortcode,
                                                                        keyword,old_balance,new_balance
                                                        )
                                                        VALUES(
                                                                    '{$data['sender']}',now(),now(),
                                                                    '{$money}','{$service['id']}','{$data['id']}','{$service['service']}','2','{$subType}','{$service['shortcode']}',
                                                                    '{$data['content']}','{$oldBalance}','{$newBalance}'
                                                        )
                                                    ";
                        //echo $sqlInsertChargingHistory;
                        $conn->doUpdate($sqlInsertChargingHistory);
                        //UPDATE sms_mo
                        $sqlUpdateMo = "UPDATE sms_mo SET charging_status=1,total_money='{$money}' WHERE id='{$data['id']}' ";
                        $conn->doUpdate($sqlUpdateMo);
                        $_reply_content = $service['reply_content'];
                        $_service_id = $service['id'];
                    }else{
                        //Charging fail
                        $_reply_content = "Sorry your balance is not enought money.";
                        $sqlUpdateMo = "UPDATE sms_mo SET charging_status=99 WHERE id='{$data['id']}' ";
                        $conn->doUpdate($sqlUpdateMo);
                        $_service_id = $service['id'];
                    }
               }else{
                    $_reply_content = $service['reply_content'];
                    $_service_id = $service['id'];
               }
            }else{
                $sql="SELECT * FROM service WHERE type=4 AND shortcode='{$data['service']}'";
                $service = $conn->doSelectOne($sql);
                if($service){
                    //Charge by MO
                    $money = $service['price'];
                    $msisdn = substr($data['sender'],5);
                    $info = viewInfo($msisdn);
                    $memberBalance = viewBalance($msisdn);
                    $subType = $info[0];
                    if(!$service['shortcode_charging']&&$money>0){
                        $result = charge($msisdn, $money, $subType);
                    }else{
                        $result = 1;
                    }
                    if($result==1) {
                         //Charge successfully
                         //usleep(500000);
                         //echo "\n Insert charging history";
                         //Insert charging history
                         $oldBalance = $memberBalance;
                         $newBalance = $memberBalance-$money;
                         if($memberBalance==0){
                            $oldBalance = 0;
                            $newBalance = 0;
                         }
                         $sqlInsertChargingHistory = "
                                                         INSERT INTO charging_history(
                                                                         msisdn,start_charging_date,end_charging_date,
                                                                         total_money,service_id,mo_id,service_name,charging_type,sub_type,shortcode,
                                                                         keyword,old_balance,new_balance
                                                         )
                                                         VALUES(
                                                                     '{$data['sender']}',now(),now(),
                                                                     '{$money}','{$service['id']}','{$data['id']}','{$service['service']}','2','{$subType}','{$service['shortcode']}',
                                                                     '{$data['content']}','{$oldBalance}','{$newBalance}'
                                                         )
                                                     ";
                         //echo $sqlInsertChargingHistory;
                         $conn->doUpdate($sqlInsertChargingHistory);
                         //UPDATE sms_mo
                         $sqlUpdateMo = "UPDATE sms_mo SET charging_status=1,total_money='{$money}' WHERE id='{$data['id']}' ";
                         $conn->doUpdate($sqlUpdateMo);
                         $_reply_content = $service['reply_content'];
                         $_service_id = $service['id'];
                    }else{
                         //Charging fail
                         $_reply_content = "Sorry your balance is not enought money.";
                         $sqlUpdateMo = "UPDATE sms_mo SET charging_status=99 WHERE id='{$data['id']}' ";
                         $conn->doUpdate($sqlUpdateMo);
                         $_service_id = $service['id'];
                    }
                    $_reply_content = $service['reply_content'];
                    $_service_id = $service['id'];
                }
            }
        }
        if(strtolower($data['content'])=='h'){
            $_reply_content = $shortcode['help_content'];
            $sqlUpdateMo = "UPDATE sms_mo SET charging_status=2 WHERE id='{$data['id']}' ";
            $conn->doUpdate($sqlUpdateMo);
        }else if(!$_reply_content){
            $_reply_content = $shortcode['invalid_content'];
            $sqlUpdateMo = "UPDATE sms_mo SET charging_status=2 WHERE id='{$data['id']}' ";
            $conn->doUpdate($sqlUpdateMo);
        }
        
        
    }
   
    
    $sqlUpdateMo = "update sms_mo SET status=1,service_id='{$_service_id}' WHERE id={$data['id']}";
    $conn->doUpdate($sqlUpdateMo);
    usleep($_waiting_process_time);
    
    
    $sentTime = date("Y-m-d H:i:s",time());
    $dbReplyContent = str_replace("'","\'",$_reply_content);
    $sqlInsertSMS = "
                        INSERT INTO sms_mt(sender,receiver,service,command_code,content_type,content,send_datetime,status,charging_status,service_id)
                        VALUES('{$data['service']}','{$data['sender']}','{$data['service']}','Free','0',
                               '{$dbReplyContent}','{$sentTime}',1,0,'{$_service_id}')
                    ";
                               
    $conn->doUpdate($sqlInsertSMS);
    
    echo $_reply_content;
}
?>