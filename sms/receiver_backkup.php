<?php
//error_reporting(false);
require_once("lib/config.php");
require_once("lib/functions.php");
require_once("lib/sbMysqlPDO.class.php");
error_reporting(E_ALL&~E_NOTICE);
$conn = null;
try{
    $conn = new sbMysqlPDO($server, $user, $password, $db);
    $conn->doSelect("SET NAMES 'UTF8'");
    
}catch(Exception $e){
    file_put_contents($realPath."log/{$mod}.txt","\nError when connect to database server|".date("Y-m-d H:i:s",time()));
    sleep(5);
}

$data['sender'] = intval($_REQUEST['sender']);
$data['service'] = intval($_REQUEST['service']);
$data['content'] = $_REQUEST['content'];
$commandCode = explode(" ", $data['content']);
$data['command_code'] = $commandCode[0];
$data['command_code'] = trim($data['content']);
$data['status'] = 0;
$data['created_datetime'] = date("Y-m-d H:i:s",time());

if($data['sender']&&$data['service']&&$data['content']&&$data['command_code']){
    
    $sqlSelectShortcode = " SELECT * FROM shortcode WHERE shortcode='{$data['service']}'";
    $shortcode = $conn->doSelectOne($sqlSelectShortcode);
    
    $sqlInsert = "
                    INSERT INTO sms_mo(sender,service,content,command_code,status,created_datetime)
                    VALUES('{$data['sender']}','{$data['service']}','{$data['content']}','{$data['command_code']}','{$data['status']}','{$data['created_datetime']}')
                 ";
    $data['id'] = $conn->doUpdate($sqlInsert);
    //OFF ALL
    if(strtolower($data['command_code'])=='off all'){
        //$sql = "UPDATE member set status=3,cancel_date=now() WHERE msisdn='{$data['sender']}' AND status=1 ";
        //$conn->doUpdate($sql);
        //echo "Tharn dai yok lerk bo li karn ni leo, tong karn long tha bien nam tho 1920. kor korb chai!";
        //exit();
    }

    $cmdLower = strtolower($data['command_code']);
    $cmdUpper = strtoupper($data['command_code']);
    $sqlSelectCommand = "
                            SELECT command_code.*,service.price,service.type as service_type,service.service as service_name
                            FROM command_code INNER JOIN service ON command_code.service_id=service.id
                            WHERE  shortcode='{$data['service']}' AND (command_code.command_code='{$cmdLower}' OR command_code.command_code='{$cmdUpper}'
                                   OR command_code.command_code_alias LIKE '%[{$cmdLower}]%'
                                   OR command_code.command_code_alias LIKE '%[{$cmdUpper}]%' )
                         ";
    $command = $conn->doSelectOne($sqlSelectCommand);

    usleep($_waiting_process_time);
    $updateMoStatus = true;
    if($command){
       $_service_id = $command['service_id'];
       switch($command['type']){
           case '1':
               //if($data['ServiceID']!='8090'){
               //if($_promotion_status!=1||($_promotion_status==1&&$data['service']!='8090')){
               if(1){
                   //ON command
                   $shortcodePrice = $serviceShortCodePrice[$data['service']];
                   $servicePrice = $command['price'];
                   if($command['service_type']==1){
                       //Daily
                       $totalDayBonus = ceil($shortcodePrice/$servicePrice);
                   }else{
                       //Monthy
                       $totalDayBonus = ceil(($shortcodePrice/$servicePrice)*30);
                   }
                   $sqlCheckMemberExists = "
                                                SELECT * FROM member
                                                WHERE msisdn='{$data['sender']}'
                                                      AND service_id='{$command['service_id']}'
                                                      AND (status=1 OR status=2)
                                           ";
                   $member = $conn->doSelectOne($sqlCheckMemberExists);
                   usleep($_waiting_process_time);
                   if($member){
                        
                   }else{
                       //Register for member
                        $registerDate = $startChargingDate = date("Y-m-d H:i:s",time());
                        $endChargingDate = time()+24*3600*$totalDayBonus;
                        $endChargingDate = date('Y-m-d H:i:s',$endChargingDate);
                        $sqlInsertMember = "
                                                 INSERT INTO member(
                                                                        msisdn,service_id,register_date,
                                                                        start_charging_date,end_charging_date,status,
                                                                        total_money
                                                                    )
                                                 VALUES(
                                                            '{$data['sender']}','{$command['service_id']}','{$registerDate}',
                                                            '{$startChargingDate}','{$endChargingDate}',1,
                                                            '{$shortcodePrice}'
                                                       )
                                           ";
                        while(1){
                            try{
                                $conn->doUpdate($sqlInsertMember);
                                usleep($_waiting_process_time);
                                break;
                            }catch(Exception $e){
                                $errorMsg = "\n Register error|101|{$data['sender']}|".date("Y-m-d H:i:s",time());
                                file_put_contents($realPath."log/{$mod}.txt",$errorMsg);
                                try{
                                    $conn = new sbMysqlPDO($server, $user, $password, $db);
                                }catch(Exception $e){
                                    file_put_contents($realPath."log/{$mod}.txt","\nError when connect to database server|".date("Y-m-d H:i:s",time()));
                                    sleep(1);
                                }
                            }
                        }
                        $sqlSelectMember = " SELECT * FROM member WHERE msisdn='{$data['sender']}' AND service_id='{$command['service_id']}' AND status=1 ";
                        $member = $conn->doSelectOne($sqlSelectMember);
                        usleep($_waiting_process_time);
                   }
                   //Insert charging history
                   /*
                   $sqlInsertChargingHistory = "
                                                    INSERT INTO charging_history(
                                                                member_id,start_charging_date,end_charging_date,
                                                                total_money,service_id,mo_id,service_name
                                                                )
                                                    VALUES(
                                                            '{$member['id']}','{$startChargingDate}','{$endChargingDate}',
                                                            '{$shortcodePrice}','{$member['service_id']}','{$data['id']}','{$command['service_name']}'
                                                          )
                                                ";
                   $conn->doUpdate($sqlInsertChargingHistory);
                    * 
                    */
                   usleep($_waiting_process_time);
                   //Send message to customer
                   $_reply_content = $command['reply_content'];
                   
                   //Select the latest content to send to customer
                   $sqlSelectLatestContent = "
                                                SELECT * FROM content WHERE service_id = {$command['service_id']} ORDER BY id DESC
                                             ";
                   $content = $conn->doSelectOne($sqlSelectLatestContent);
                   if($content){
                        $sendContent = $content['content'];
                        $sentTime = date("Y-m-d H:i:s",time());
                        $sqlInsertSMSContent = "
                                                    INSERT INTO sms_mt(sender,receiver,service,command_code,content_type,content,send_datetime,status,charging_status)
                                                    VALUES('{$data['service']}','{$data['sender']}','{$data['service']}','Free','0',
                                                           '{$sendContent}','{$sentTime}',0,1)
                                                 ";
                         $conn->doUpdate($sqlInsertSMSContent);
                   }
                    usleep($_waiting_process_time);
                   
               }else{
                   $updateMoStatus = false;
               }
               break;
           case '2':
               //OFF
               $sqlCheckMemberExists = "
                                            SELECT * FROM member
                                            WHERE msisdn='{$data['sender']}' AND service_id='{$command['service_id']}' AND (status=1 OR status=2)
                                       ";

               $member = $conn->doSelectOne($sqlCheckMemberExists);
               if($member){
                   $sentTime = date("Y-m-d H:i:s",time());
                   $sqlUpdate = "
                                    UPDATE member set status=3,cancel_date='{$sentTime}' WHERE id='{$member['id']}'
                                ";
                   $conn->doUpdate($sqlUpdate);
                   $_reply_content = $command['reply_content'];
               }else{
                   $_reply_content = $_sms_not_register;
                   $sentTime = date("Y-m-d H:i:s",time());
               }
               break;
           case '4':
               //FREE
               break;
           case '5':
               //HELP
               $_reply_content = $command['reply_content'];
               break;
           case '6':
               //CHARGING MO
               $_reply_content = $command['reply_content'];
               $chargingStatus = 0;
               break;
           default:
               //HELP
               $_reply_content = $command['reply_content'];
               break;
       }
       //Update status MO
    }else{
       //HELP FOR INVALID COMMAND CODE
       /*
       if($_promotion_status==1&&$data['service']=='8090'){
           echo "\n Promotion program, don't process this MO";
       }else{
        *
        */
        //Check voting
        $cmdLower = strtolower($data['content']);
        $cmdUpper = strtoupper($data['content']);
        $sqlVoting = " SELECT voting_keyword.*,service.start_time,service.end_time,service.invalid_content FROM voting_keyword INNER JOIN service ON service.id=voting_keyword.service_id"
                     . " WHERE  shortcode='{$data['service']}' AND service.type = 7 AND (keyword='$cmdLower' OR keyword='$cmdUpper')  "
                     . " ORDER BY id DESC LIMIT 1 ";
        //echo $sqlVoting;
        $keyword = $conn->doSelectOne($sqlVoting);
        if($keyword){
            if(strtotime($keyword['start_time'])<=time() && strtotime($keyword['end_time'])>=time()){
                $sqlInsertKeyword = " INSERT INTO voting_log (msisdn,keyword,keyword_id,mo_id,created_datetime)"
                                    . "VALUES('{$data['sender']}','{$data['content']}','{$keyword['id']}','{$data['id']}',now())";
                $conn->doUpdate($sqlInsertKeyword);
                $sqlUpdateVote = " UPDATE voting_keyword SET total_vote=total_vote+1 WHERE id='{$keyword['id']}'";
                $conn->doUpdate($sqlUpdateVote);
                $_reply_content = $keyword['reply_content'];
            }else{
                $_reply_content = $keyword['invalid_content'];
            }
            $_service_id = $keyword['service_id'];
        }else{
            //Check keyword
            $sql="SELECT * FROM service WHERE type=6 AND shortcode='{$data['service']}'";
            $service = $conn->doSelectOne($sql);
            preg_match_all('!\d+!', $data['content'], $matches);
            if(intval($matches[0][0])>0&&$service){
               //Checking MO
               $_reply_content = $service['reply_content'];
               $_service_id = $service['id'];
            }else{
                $sql="SELECT * FROM service WHERE type=4 AND shortcode='{$data['service']}'";
                $service = $conn->doSelectOne($sql);
                if($service){
                    //Checking MO
                    $_reply_content = $service['reply_content'];
                    $_service_id = $service['id'];
                }
            }
        }
        if(strtolower($data['content'])=='h'){
            $_reply_content = $shortcode['help_content'];
        }else if(!$_reply_content){
            $_reply_content = $shortcode['invalid_content'];
        }
        
        
    }
   
    
    $sqlUpdateMo = "update sms_mo SET status=1,service_id='{$_service_id}' WHERE id={$data['id']}";
    $conn->doUpdate($sqlUpdateMo);
    usleep($_waiting_process_time);
    
    
    $sentTime = date("Y-m-d H:i:s",time());
    $dbReplyContent = str_replace("'","\'",$_reply_content);
    $sqlInsertSMS = "
                        INSERT INTO sms_mt(sender,receiver,service,command_code,content_type,content,send_datetime,status,charging_status,service_id)
                        VALUES('{$data['service']}','{$data['sender']}','{$data['service']}','Free','0',
                               '{$dbReplyContent}','{$sentTime}',1,0,'{$_service_id}')
                    ";
                               
    $conn->doUpdate($sqlInsertSMS);
    
    echo $_reply_content;
}
?>
