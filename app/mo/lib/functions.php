<?php

function viewInfo($msisdn){
    global $_vasgateway_info,$_vasgateway_balance;
    //$_vasgateway_info['address'] = "http://192.9.200.75:8989/gsmPackageService.asmx?wsdl";
    $result = array();
    try{
        $client = new SoapClient($_vasgateway_info['address']);
        $param = array("msisdn"=>$msisdn,"user_id"=>$_vasgateway_info['username'],"user_pass"=>$_vasgateway_info['password']);
        $data = $client->CheckNetWorkType($param);
        var_dump($data);
        if(@$data->CheckNetWorkTypeResult){
            $inforResult = explode(" ",$data->CheckNetWorkTypeResult);
            if($inforResult[0]=='05'){
                //CHECK OK
                if($inforResult[1]=="0"){
                    $result[0]="9";
                }else{
                    $result[0]="1";
                }
            }else{
                $result[0]="99";
            }
        }else{
            $result[0]="99";
        }
    }catch(Exception $e){
        $result[0]="99";
    }
    return $result;
}

function viewBalance($msisdn){
    global $_vasgateway_info,$_vasgateway_balance;
    try{
        $client = new SoapClient($_vasgateway_balance['address']);
        $param = array("msisdn"=>$msisdn);
        try{
            $data = $client->ChkBalan($param);
            if($data) return $data->ChkBalanResult;
        }catch(Exception $e){
            return 0;
        }
    }catch(Exception $e){
        $result="0";
        return $result;
    }
    
}

function charge($msisdn,$money,$subType){
    global $_vasgateway_info,$_vasgateway_balance;
    if($subType=='9'){
        return 1;
    }else{
        //if($subType>=1&&$subType<=6){
        //Charging pre-paid 
        try{
            $client = new SoapClient($_vasgateway_balance['address']);
            $param = array("msisdn"=>$msisdn,"amount"=>$money,"userID"=>$_vasgateway_balance['username'],"PW"=>$_vasgateway_balance['password']);
            try{
                $data = $client->DeductBL($param);
                var_dump($data);
                if(@$data->DeductBLResult) return 1;
                else return 0;
            }catch(Exception $e){
                return 0;
            }
        }catch(Exception $e){
            return 0;
        }
    }
}

function checkBalance($msisdn){
    $xmlContent = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:acc="http://www.huawei.com/bme/cbsinterface/cbs/accountmgrmsg" xmlns:com="http://www.huawei.com/bme/cbsinterface/common" xmlns:acc1="http://www.huawei.com/bme/cbsinterface/cbs/accountmgr">
                    <soapenv:Header/>
                    <soapenv:Body>
                       <acc:QueryBalanceRequestMsg>
                          <RequestHeader>
                             <com:CommandId>QueryBalance</com:CommandId>
                             <com:Version>1</com:Version>
                             <com:TransactionId>1</com:TransactionId>
                             <com:SequenceId>1</com:SequenceId>
                             <com:RequestType>Event</com:RequestType>
                             <com:SessionEntity>
                                <com:Name>huaweiwifi</com:Name>
                                <com:Password>huaweiwifi</com:Password>
                                <com:RemoteAddress>10.30.2.55</com:RemoteAddress>
                             </com:SessionEntity>
                             <com:SerialNo>20140121000000QB</com:SerialNo>
                          </RequestHeader>
                          <QueryBalanceRequest>
                             <acc1:SubscriberNo>20'.$msisdn.'</acc1:SubscriberNo>
                          </QueryBalanceRequest>
                       </acc:QueryBalanceRequestMsg>
                    </soapenv:Body>
                 </soapenv:Envelope>
                 ';
    $cmd = 'curl -0 -H "SOAPAction: "QueryBalance"" -H "Content-Type: text/xml;charset=UTF-8" -d \''.$xmlContent.'\'  http://192.168.8.203:8680/services/CBSInterfaceAccountMgrService/CBSInterface_AcctMgr.wsdl --trace trace.log --compress';
    $result = exec($cmd);
    //Free Balance
    $basicKey = "<AccountType>2001</AccountType>";
    $subStr = substr($result, 0, strpos($result,$basicKey));
    $subStr = substr($subStr,strripos($subStr,'<Balance>')+strlen('<Balance>'));
    $freeBalance = intval(substr($subStr,0,strpos($subStr,'</Balance>')));
    //Balance
    $basicKey = "<AccountType>2000</AccountType>";
    $subStr = substr($result, 0, strpos($result,$basicKey));
    $subStr = substr($subStr,strripos($subStr,'<Balance>')+strlen('<Balance>'));
    $basicBalance = intval(substr($subStr,0,strpos($subStr,'</Balance>')));
    
    $data[0] = $basicBalance;
    $data[1] = $freeBalance;
    return $data;
    
}


function shortContent($str,$lenght){
    return substr($str,0,strripos(substr($str,0,$lenght)," "));
}

function explodeContent($content,$lenght=480){
    $resutls = array();
    $contents = explode("#",$content);
    foreach($contents as $content){
        if(strlen($content)<=$lenght){
            $results[] = $content;
        }else{
            while(1){
               $shortContent = shortContent($content,$lenght);
               $results[] = $shortContent;
               $content = substr($content,strlen($shortContent)+1);
               if(strlen($content)<$lenght){
                   $results[] = $content;
                   break;
               }
            }
        }
    }
    return $results;
}


function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

?>
