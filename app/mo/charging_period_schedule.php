<?php

require_once("lib/config.php");
require_once("lib/functions.php");
require_once("lib/sbMysqlPDO.class.php");

while (true) {
    try {
        $conn = new sbMysqlPDO($server, $user, $password, $db);
        $queryStatus = "SELECT p.ProcessId, p.Status FROM process AS p WHERE Id = 1";
        $rowStatus = $conn->doSelectOne($queryStatus);
        if ($rowStatus) {
            $pid = $rowStatus["ProcessId"];
            if ($pid != "") {
                exec("ps $pid", $pState);
                if ((count($pState) >= 2)) {
                    exec("kill $pid");
                }
            }

            $command = " php /app/mo/charging_period.php ";
            while (true) {
                $pid = exec("nohup $command > /dev/null 2>&1 & echo $!");
                if ($pid) {
                    $queryUpdate = "UPDATE process AS p SET p.ProcessId = $pid, p.Status = 1 WHERE p.Id = 1";
                    $conn->doUpdate($queryUpdate);
                    break;
                } else {
                    sleep(30);
                }
            }
        }
        $conn = NULL;
        break;
    } catch (Exception $e) {
        file_put_contents($realPath . "log/connect_log.txt", "\nError when connect to database server|" . date("Y-m-d H:i:s", time()));
        sleep(600);
    }
}