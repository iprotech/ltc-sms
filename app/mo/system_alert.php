<?php

//error_reporting(FALSE);
require_once("lib/config.php");
require_once("lib/sbMysqlPDO.class.php");

try {
    $conn = new sbMysqlPDO($server, $user, $password, $db);
} catch (Exception $e) {
    file_put_contents("log/connect_log.txt", "\nError when connect to database server|" . date("Y-m-d H:i:s", time()));
    sleep(5);
}


//Select service theo ngay

$sqlSelect = " SELECT * FROM system_monitor WHERE inform_status=0";
$errors = $conn->doSelect($sqlSelect);
foreach($errors as $item){
    $sqlUpdate = "UPDATE system_monitor SET inform_status=1 WHERE id='{$item['id']}'";
    $conn->doUpdate($sqlUpdate);
    switch($item['error_type']){
        case "1":
            $message = " Check subtype error ";
            break;
        case "2":
            $message = " Check balance error ";
            break;
        case "3":
            $message = " Charge money error ";
            break;
    }
    $listEmail = array("thien@iprotech.vn,thiendv@gmail.com");
    foreach($listEmail as $reEmail){
        echo $reEmail."\n";
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        // Additional headers
        $headers .= 'To: '.$reEmail . "\r\n";
        $headers .= 'From: LTC SMS REPORT <sms@iprotech.vn>' . "\r\n";
        $subject = "LTC SMS ERROR";
        // Mail it
        $to = $reEmail;
        mail($to, $subject, $message, $headers);
    }
    
}
?>