<?php
//error_reporting(false);
require_once("lib/config.php");
require_once("lib/functions.php");
require_once("lib/sbMysqlPDO.class.php");

$endChargeTime = time()+3*60*60;

while (1) {
    if(time()>$endChargeTime) exit();
    echo date("Y-m-d H:i:s");
    try{
        try {
            $conn = new sbMysqlPDO($server, $user, $password, $db);
            var_dump($conn);
        } catch (Exception $e) {
            file_put_contents($realPath . "log/connect_log.txt", "\nError when connect to database server|" . date("Y-m-d H:i:s", time()));
            sleep(5);
        }
        
        $sqlSelectMin = 'SELECT min(id) as id FROM member WHERE status=1 AND now()>date(end_charging_date) AND total_try_charging>=10 order by id ASC';
        echo $sqlSelectMin;
        $min = $conn->doSelectOne($sqlSelectMin);
        if ($min['id'] > 0)
            $minId = $min['id'] - 1;
        else
            $minId = $min['id'];
        $mindId=0;
        while(1){
            if(date('H')=='00'){
                $sqlUpdateTotal = "UPDATE member SET total_try_charging = 0 WHERE status=1 AND total_try_charging>=10 ";
                $conn->doUpdate($sqlUpdateTotal);
                exit();
            }
            $date_expire = date("Y-m-d H:i:s",time());
            $now = date("Y-m-d H:i:s", time());
            if(!$minId) $minId=0;
            $sqlSelectMember = "	
                                        SELECT member.*,service.service as serviceName,service.shortcode,service.price,service.type 
                                        FROM member INNER JOIN service ON member.service_id=service.id
                                        WHERE member.id > {$minId} &&(member.status=1 || member.status=2) && '{$date_expire}'>date(member.end_charging_date)
                                              AND charging_status=0 AND total_try_charging>10 AND
                                              (service.type=1 OR service.type=2 OR service.type=3)
                                        ORDER BY id ASC LIMIT 5
                               ";
            $members = $conn->doSelect($sqlSelectMember);
            echo $sqlSelectMember;
            echo count($members);
            if (count($members)){
                foreach ($members as $item) {
                    $timeEndCharging = strtotime($item['end_charging_date']);
                    $vCompare = time() - $timeEndCharging;
                    if ($vCompare > 30 * 3600 * 24) {
                        //Cancel when expired date is more than 60 day
                        $sqlCancel = " UPDATE member SET status=4,cancel_date='{$now}' WHERE id='{$item['id']}' ";
                        $conn->doUpdate($sqlCancel);
                    } else {
                        $minId = $item['id'];
                        $msisdn = substr($item['msisdn'],3);
                        $info = viewInfo($msisdn);
                        var_dump($info);
                        $subType=$info[0];
                        $money = $item['price'];
                        echo "Sub type: $msisdn | $subType|$money";
                        $sqlUpdateMemberType = "	
                                                    UPDATE member
                                                    SET sub_type='{$subType}'
                                                    WHERE id='{$item['id']}'
                                               ";
                        $conn->doUpdate($sqlUpdateMemberType);
                        echo "\n" . $sqlUpdateMemberType;
                        //sleep(5);
                        $result = charge($msisdn, $money, $subType);
                        var_dump($result);
                        //sleep(5);
                        if($result==1) {
                            //Charge tiền thành công, cộng thêm ngày sử dụng cho thuê bao
                            if ($item['type'] == 1) {
                                //Daily
                                $totalDayBonus = ceil($money / $item['price']);
                            } else if($item['type']==2){
                                //Week
                                $totalDayBonus = ceil(($money / $item['price']) * 7);
                            }else if($item['type']==3){
                                //Monthy
                                $totalDayBonus = ceil(($money / $item['price']) * 30);
                            }
                            //Member have been registed already, bonous using days for this member
                            echo "\n Bonous using days for this member";
                            $startChargingDate = date("Y-m-d H:i:s", time());
                            $endChargingDate = time() + 24 * 3600 * $totalDayBonus;
                            $endChargingDate = date('Y-m-d H:i:s', $endChargingDate);
                            $sqlUpdateMember = "	
                                                    UPDATE member
                                                    SET status=1,start_charging_date='{$startChargingDate}',
                                                    end_charging_date='{$endChargingDate}',total_money=total_money+{$money},
                                                    total_try_charging=0,sub_type='{$subType}'
                                                    WHERE id='{$item['id']}'
                                               ";
                            echo "\n" . $sqlUpdateMember;
                            while (1){
                                try {
                                    $conn->doUpdate($sqlUpdateMember);
                                    break;
                                } catch (Exception $e) {
                                    $errorMsg = "\n Update bonous error|102|{$item['msisdn']}|" . date("Y-m-d H:i:s", time());
                                    file_put_contents($realPath . "log.txt", $errorMsg);
                                    try {
                                        $conn = new sbMysqlPDO($server, $user, $password, $db);
                                    }catch (Exception $e) {
                                        file_put_contents($realPath . "log.txt", "\nError when connect to database server|" . date("Y-m-d H:i:s", time()));
                                        sleep(1);
                                    }
                                }
                            }
                            usleep(50000);
                            echo "\n Insert charging history";
                            //Insert charging history
                            $sqlInsertChargingHistory = "
                                                            INSERT INTO charging_history(
                                                                            msisdn,member_id,start_charging_date,end_charging_date,
                                                                            total_money,service_id,mo_id,service_name,charging_type,sub_type,shortcode
                                                            )
                                                            VALUES(
                                                                        '{$item['msisdn']}','{$item['id']}','{$startChargingDate}','{$endChargingDate}',
                                                                        '{$money}','{$item['service_id']}','0','{$item['serviceName']}',1,{$subType},'{$item['shortcode']}'
                                                            )
                                                        ";
                            $conn->doUpdate($sqlInsertChargingHistory);
                        } else {
                            if($result[0]==201){
                                $sqlUpdateMember = " UPDATE member SET charging_status = 99 WHERE id='{$item['id']}' ";
                                $conn->doUpdate($sqlUpdateMember);
                            }
                            //Charge ko thành công, ghi log để sử lý
                            echo "Charging fail:";
                            echo $info = "\n$money|{$item['msisdn']}|$shortcode|{$item['id']}|" . date('Y-m-d H:i:s', time());
                            file_put_contents("error_log.txt", $info, FILE_APPEND);
                            $sqlTotalTryChargingMember = "	 
                                                                UPDATE member
                                                                SET total_try_charging=total_try_charging+1,sub_type='{$subType}'
                                                                WHERE id='{$item['id']}'
                                                    ";
                            $conn->doUpdate($sqlTotalTryChargingMember);
                        }
                        usleep(50000);
                    }
                }
            }else{
                exit();
            }
            sleep(1);
        }
        sleep(1);
    }catch(Exception $e){
        
    }
}
?>
