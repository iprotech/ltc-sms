<?php

//error_reporting(FALSE);
require_once("lib/config.php");
require_once("lib/sbMysqlPDO.class.php");

try {
    $conn = new sbMysqlPDO($server, $user, $password, $db);
} catch (Exception $e) {
    file_put_contents("log/connect_log.txt", "\nError when connect to database server|" . date("Y-m-d H:i:s", time()));
    sleep(5);
}


//Select service theo ngay

$sqlSelect = " SELECT * FROM email_report";
$emails = $conn->doSelect($sqlSelect);
foreach($emails as $email){
    //Daily
    var_dump($email);
    if($email['type']==1){
       $aIds = explode(',',$email['service']);
       if($aIds){
         foreach($aIds as $id){
           $ids .= ','.$id;
         }
       }
       $reportDate = date("Y-m-d",time()-24*3600);
       $sql = " SELECT * FROM reporting INNER JOIN service ON service.id=reporting.service_id"
              ." WHERE service_id in (0{$ids}) AND date(time)='{$reportDate}' ";
       echo $sql;
       $reports = $conn->doSelect($sql);
       //Email to member
    }
    if($email['type']==2){
        //Weekly
        if(date('N',time())==1){
        //if(true){
            //Monday
            $lastDay = date("Y-m-d",time()-24*3600);
            $startDay = date("Y-m-d",time()-7*24*3600);
            $sqlSelectWeek = " SELECT sum(new_register) as new_register, sum(cancel_by_user) as cancel_by_user, sum(cancel_by_system) as cancel_by_system,"
                    . " sum(real_development) as real_development, sum(`current_user`) as `current_user`, sum(chargable_subs) as chargable_subs,"
                    . " sum(monthly_fee_revenue) as monthly_fee_revenue,service_id,service.service"
                    . " FROM reporting INNER JOIN service ON service.id=reporting.service_id"
                    . " WHERE date(time)>='{$startDay}' AND date(time)<='{$lastDay}' "
                    . " GROUP by service_id";
            echo "\n".$sqlSelectWeek;
            $reports = $conn->doSelect($sqlSelectWeek);
            var_dump($reports);
            $reportDate = " From $startDay to $lastDay";
        }
    }
    if($email['type']==3){
        //Monthly
        if(intval(date('d',time()))==1){
        //if(true){
            //Monday
            $lastDay = date("Y-m-d",time()-24*3600);
            $startDay = date("Y-m-01",strtotime($lastDay));
            $reportDate = date("m-Y",strtotime($startDay));
            $sqlSelectMonth = " SELECT sum(new_register) as new_register, sum(cancel_by_user) as cancel_by_user, sum(cancel_by_system) as cancel_by_system,"
                    . " sum(real_development) as real_development, sum(`current_user`) as `current_user`, sum(chargable_subs) as chargable_subs,"
                    . " sum(monthly_fee_revenue) as monthly_fee_revenue,service_id,service.service"
                    . " FROM reporting INNER JOIN service ON service.id=reporting.service_id"
                    . " WHERE date(time)>='{$startDay}' AND date(time)<='{$lastDay}' "
                    . " GROUP by service_id";
            echo $sqlSelectMonth;
            $reports = $conn->doSelect($sqlSelectMonth);
            
        }
    }
    
    $content = "<table><tr><td>Service</td><td>Date</td><td>Register</td><td>Cancel</td><td>System cancel</td><td>Development</td>"
               . "<td>Member No.</td><td>Charge No</td><td>Revenue</td></tr>";
    foreach($reports as $report){
        $content .= "<tr><td>{$report['service']}</td><td>{$reportDate}</td>"
        . "<td>{$report['new_register']}</td><td>{$report['cancel_by_user']}</td>"
        . "<td>{$report['cancel_by_system']}</td><td>{$report['real_development']}</td>"
        . "<td>{$report['current_user']}</td><td>{$report['chargable_subs']}</td><td>{$report['monthly_fee_revenue']}</td></tr>";
    }
    $content .= "</table>";
    file_put_contents("/var/www/html/report/".date("Y-m-d",time())."_".$reportDate.".xls", $content);   
    $listEmail = explode(",",$email['email']);
    var_dump($listEmail);
    foreach($listEmail as $reEmail){
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        // Additional headers
        $headers .= 'To: '.$reEmail . "\r\n";
        $headers .= 'From: LTC SMS REPORT <sms@iprotech.vn>' . "\r\n";
        $subject = "Report about LTC sms service";
        $message = " Dear all, Please check the report about LTC SMS service {$reportDate} at the url: http://115.84.105.172/report/".date("Y-m-d",time())."_".$reportDate.".xls";
        // Mail it
        $to = $reEmail;
        mail($to, $subject, $message, $headers);
    }
    
    
}
?>