<?php

require_once("lib/config.php");
require_once("lib/functions.php");
require_once("lib/sbMysqlPDO.class.php");

try {
    $conn = new sbMysqlPDO($server, $user, $password, $db);
} catch (Exception $e) {
    file_put_contents("log/connect_log.txt", "\nError when connect to database server|" . date("Y-m-d H:i:s", time()));
    sleep(5);
}
$sqlSelect = " select * from voting_log where keyword LIKE '%*%' or keyword like '% %'; ";
$datas = $conn->doSelect($sqlSelect);
foreach($datas as $item){
    $totalVote = explode("*", $item['keyword']);
    $totalVote = $totalVote[1];
    if(!$totalVote){
        $totalVote = explode(" ", $item['keyword']);
        $totalVote = $totalVote[1];
    }
    if(!$totalVote) $totalVote=1;
    $sqlUpdate = " update voting_log SET total_vote = '{$totalVote}' Where id='{$item['id']}' ";
    $conn->doUpdate($sqlUpdate);
}
?>
