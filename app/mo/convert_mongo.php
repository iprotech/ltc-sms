<?php
    error_reporting(E_ALL ^ E_NOTICE);
    require_once 'lib/config.php';
    require_once("lib/functions.php");
    require_once("lib/sbMysqlPDO.class.php");
    
    while(1){
        try {
            $conn = new sbMysqlPDO($server, $user, $password, $db);
            var_dump($conn);
        } catch (Exception $e) {
            file_put_contents($realPath . "log/connect_log.txt", "\nError when connect to database server|" . date("Y-m-d H:i:s", time()));
            sleep(5);
        }

        $m = new MongoClient();
        $mongodb='ltc_sms';
        $mongocollection='charging_history';
        $collection = $m->$mongodb->$mongocollection;
        
        $pipeline = array(array(
                                '$group' => array(
                                            '_id' => null,
                                            'maxid' => array('$max' => '$id'),
                    )));
        
        $results = $collection->aggregate($pipeline);
        $maxId = $results['result'][0]['maxid'];
        
        $cursor = $collection->findOne();
        $id = $cursor['_id'];

        if(!isset($id)){
            $result=" SELECT charging_history.*,service.category_id 
                      FROM charging_history,service 
                      WHERE charging_history.service_id = service.id 
                      ORDER BY id asc LIMIT 1000 ";
        }else{
            $result=" SELECT charging_history.*,service.category_id 
                      FROM charging_history,service 
                      WHERE charging_history.service_id = service.id AND charging_history.id > '$maxId'
                      ORDER BY id asc LIMIT 1000 ";
        }

        $mongo = $conn->doSelect($result);
        
        if(count($mongo)){
            if (is_array($mongo)) {
            foreach ($mongo as $item){
                $array = array('id'=>intval($item['id']),
                           'msisdn'=>utf8_encode($item['msisdn']), 
                           'member_id'=>intval($item['member_id']),
                           'start_charging_date'=>utf8_encode($item['start_charging_date']),
                           'end_charging_date'=>utf8_encode($item['end_charging_date']),
                           'total_money'=>intval($item['total_money']),
                           'service_id'=>intval($item['service_id']),
                           'mo_id'=>intval($item['mo_id']),
                           'mt_id'=>intval($item['mt_id']),
                           'service_name'=>utf8_encode($item['service_name']),
                           'charging_type'=>intval($item['charging_type']),
                           'convert_status'=>intval($item['convert_status']),
                           'delete_status'=>intval($item['delete_status']),
                           'sub_type'=>intval($item['sub_type']),
                           'cdr_status'=>intval($item['cdr_status']),
                           'shortcode'=>utf8_encode($item['shortcode']),
                           'keyword'=>utf8_encode($item['keyword']),
                           'old_balance'=>utf8_encode($item['old_balance']),
                           'new_balance'=>utf8_encode($item['new_balance']),
                           'total_vote'=>intval($item['total_vote']), 
                           'category_id'=>intval($item['category_id']));
                $collection->insert($array);
            }
            }
            sleep(1);
        }else{
              sleep(60);
            }
    }
   
?>