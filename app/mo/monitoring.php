<?php
//error_reporting(false);
error_reporting(E_ALL ^ E_NOTICE);
require_once("lib/config.php");
require_once("lib/functions.php");
require_once("lib/sbMysqlPDO.class.php");
require_once('mailer/class.phpmailer.php');
require_once('mailer/class.smtp.php');


$_msisdn = "2058191391";
$_email_account = "tathanh328@gmail.com"; 
$_email_password = "trungthanh328";
//$_mail_list = array("iprovasmonitor@gmail.com","sivv@iprotech.vn");
$_mail_list = array("iprovasmonitor@gmail.com","tathanh328@gmail.com");
$errorMsg = array();
//MYSQL
$conn = new sbMysqlPDO($server, $user, $password, $db);
if(!$conn){
    $errorMsg[] = "Can not connect to MYSQL ";
}

//SMSC
$ping = exec('ping -c 3 192.168.101.250'); //10.78.17.17
if(empty($ping)){
    $errorMsg[] = "Can not ping to SMSC 192.168.101.250";
}

//CHARING GW
$ping = exec('ping -c 3 192.9.200.75'); //10.78.17.17
if(empty($ping)){
    $errorMsg[] = "Can not ping to CHARGING GW 192.9.200.75";
}

// Check Info
$info = viewInfo($_msisdn);
if($info[0] == 99){
    $errorMsg[] = "Can not get phone number information (viewInfo) {$info[1]} ";
}


//Check charging
echo $sqlCheckToday = " SELECT sum(total_money) as total FROM charging_history WHERE start_charging_date >date(now()) ";
$rToday = $conn->doSelectOne($sqlCheckToday);

$timeCheck = date("Y-m-d H:i:s",time()-24*3600);
echo $sqlCheckYesterday = " SELECT sum(total_money) as total FROM charging_history WHERE start_charging_date>=date('$timeCheck') AND start_charging_date<='$timeCheck' ";
$rYesterday = $conn->doSelectOne($sqlCheckYesterday);

if($rToday['total']*2<$rYesterday['total']){
    $errorMsg[] = "Have problem on charging process, today: {$rToday['total']}, yesterday: {$rYesterday['total']}  ";
}

//SMS MT
$timeCheckContent = date("Y-m-d H:i:s",time()-5*60);
$sqlContent = "SELECT count(*) as total FROM content WHERE send_datetime<'{$timeCheckContent}' AND status=0  ";
$contentPending = $conn->doSelectOne($sqlContent);
if($contentPending['total']){ 
   $errorMsg[] = "Process send content is not running ";
}

$sqlMT = "SELECT * FROM sms_mt WHERE status = 0 ORDER BY send_datetime ASC LIMIT 1;";
$smsPending = $conn->doSelectOne($sqlMT);
if($smsPending){
    if(strtotime($smsPending['send_datetime'])<(time()-20*60)){
        //SEND SMS PROCESS NOT RUN
        $errorMsg[] = "Process sending sms is not running  ";
    }
}

$timeCheck = date("Y-m-d H:i:s",time()-30*60);
echo $sqlMT = "SELECT count(*) as total FROM sms_mt WHERE status = 99 and send_datetime>='$timeCheck' ";
$smsError = $conn->doSelectOne($sqlMT);
if($smsError['total']>100){ 
   $errorMsg[] = "Sending sms error ";
}


//HDD
$hdd = explode("\n",shell_exec('df -h'));
$hdd = explode(" ",preg_replace('/\s+/', ' ', $hdd[2]));
if(intval($hdd[4])>80){
    $errorMsg[]= "HDD is full: {$hdd[4]}, available {$hdd[3]} ";
}

//CPU
$cpus = explode("\n",shell_exec('top -b -n3 | grep "Cpu(s)"'));
var_dump($cpus);
for($i=0;$i<(count($cpus)-1);$i++){
    $arCpu = explode(" ",preg_replace('/\s+/', ' ',$cpus[$i]));
    $totalCPU += intval($arCpu[1])+intval($arCpu[2]);
}
$totalCPU = intval($totalCPU/3);
if($totalCPU>70){
    $errorMsg[]= "CPU is hight: ".$totalCPU;
}

//RAM
$rams = explode("\n",shell_exec('top -b -n3 | grep "Mem"'));
var_dump($rams);
for($i=0;$i<(count($rams)-1);$i++){
    $arRam = explode(" ",preg_replace('/\s+/', ' ',$rams[$i]));
    $totalRamUse += intval($arRam[3])/intval($arRam[1]);
}
$totalRamUse = intval($totalRamUse/3);
if($totalRamUse>70){
    $errorMsg[]= "RAM is hight: ".$totalRamUse;
}

//SMS GW
$sms = str_replace("\n","<br/>",file_get_contents("http://localhost:13000/status?username=thiendv&password=thiendv"));
if(substr_count($sms,"Box connections")&&substr_count($sms,"SMSC connections")&&substr_count($sms,"Status: running")){
    echo "SMSC is working";
}else{
    $errorMsg[]= "SMSC ERROR<br/>".$sms;
}
$smsFail = explode(",",substr($sms,stripos($sms,"Kannel (")));
$totalSmsFail = intval(str_replace('failed','',$smsFail[3]));
$totalSmsSent = intval(str_replace('sent','',$smsFail[2]));
$totalSmsLast = explode("|",file_get_contents("log_sms_fail.log"));
file_put_contents("log_sms_fail.log",$totalSmsSent."|".$totalSmsFail);
if(($totalSmsFail-$totalSmsLast[1])>100){
    $errorMsg[]= "SMSC reject sending sms, total fail: $totalSmsFail, total last fail: {$totalSmsLast[1]}, total sent: $totalSmsSent";
}
echo "SMS: $totalSmsFail, total last fail: {$totalSmsLast[1]}, total sent: $totalSmsSent";
        

//Report at 08h and 17h daily
if(date("H",time())=="08"||date("H",time())=="17"||date("H",time())=="12"){
    if(date("i",time())>=0 and date("i",time())<=15){
        if(!count($errorMsg)) $subject = "LTC REPORTING ";
        $errorMsg[]="System is working fine."
                   ."<br/> Revenue: Today = {$rToday['total']}KIP, Yesterday = {$rYesterday['total']}KIP "
                   ."<br/> HDD: {$hdd[4]}, available {$hdd[3]} "
                   ."<br/> CPU: $totalCPU%"
                   ."<br/> RAM: $totalRamUse%"
                   ."<br/> SMS: total fail: $totalSmsFail, total last fail: {$totalSmsLast[1]}, total sent: $totalSmsSent";
    }
}



var_dump($errorMsg);

if(count($errorMsg)){
    foreach($errorMsg as $msg){
        $sendContent .= "$msg <br/>";
    }
    $mail = new PHPMailer(true);                //New instance, with exceptions enabled
    $mail->IsSMTP();                            // tell the class to use SMTP
    $mail->SMTPAuth   = true;                   // enable SMTP authentication
    $mail->Port       = 465;                     // set the SMTP server port
    $mail->Host       = "smtp.gmail.com";       // SMTP server
    $mail->SMTPSecure = 'ssl'; 
    $mail->Username   = $_email_account;     // SMTP server username
    $mail->Password   = $_email_password;           // SMTP server password

    $mail->From       = $_email_account;
    $mail->FromName   = "LTC SMS";
	
    foreach($_mail_list as $to){
        $mail->AddAddress($to);
    }
    if($subject) $mail->Subject  = $subject;
    else $mail->Subject  = "LTC SMS ERROR";
    $mail->MsgHTML($sendContent);
    $mail->IsHTML(true);
    $mail->Send();
}
?>
