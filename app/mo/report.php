<?php

//error_reporting(FALSE);
require_once("lib/config.php");
require_once("lib/sbMysqlPDO.class.php");

try {
    $conn = new sbMysqlPDO($server, $user, $password, $db);
} catch (Exception $e) {
    file_put_contents("log/connect_log.txt", "\nError when connect to database server|" . date("Y-m-d H:i:s", time()));
    sleep(5);
}


$date = Date("Y-m-d",time()-24*3600);

$sqlSelectService = "SELECT DISTINCT(id) as service_id FROM service";
$service = $conn->doSelect($sqlSelectService);
var_dump($service);

foreach ($service as $item) {
    $sqlSelectMember = "SELECT count(*) FROM member WHERE date(register_date)='{$date}' && service_id=" . $item['service_id'];
    $Member = $conn->doSelectOne($sqlSelectMember);

    $sqlCancelByUser = "SELECT count(*) FROM member WHERE date(cancel_date)='{$date}' && status=3 && service_id=" . $item['service_id'];
    $CancelByUser = $conn->doSelectOne($sqlCancelByUser);

    $sqlCancelBySystem = "SELECT count(*) FROM member WHERE date(cancel_date)='{$date}' && status=4 && service_id=" . $item['service_id'];
    $CancelBySystem = $conn->doSelectOne($sqlCancelBySystem);

    $RealDevelopment = $Member[0] - ($CancelByUser[0] + $CancelBySystem[0]);

    $sqlNotCanceldate = "SELECT count(*) FROM `member` WHERE date(cancel_date)>'{$date}' and date(register_date)<='{$date}' and status!=1 and service_id=" . $item['service_id'];
    $NotCanceldate = $conn->doSelectOne($sqlNotCanceldate);

    $sqlUser = "SELECT count(*) FROM `member` WHERE date(register_date)<='{$date}' and status=1 and service_id=" . $item['service_id'];
    $User = $conn->doSelectOne($sqlUser);

    $TotalUser = $NotCanceldate[0] + $User[0];


    $sqlChargableSubs = "SELECT count(*) FROM charging_history WHERE date(start_charging_date)='{$date}' && service_id=" . $item['service_id'];
    $ChargableSubs = $conn->doSelectOne($sqlChargableSubs);

    $sqlDayFee = "SELECT SUM(total_money) FROM charging_history WHERE date(start_charging_date)='{$date}' && service_id=" . $item['service_id'];
    $DayFii = $conn->doSelectOne($sqlDayFee);

    $sqlInsert = "INSERT INTO `reporting`(`time`,`service_id`,`new_register`,`cancel_by_user`,`cancel_by_system`,`real_development`,`current_user`,`chargable_subs`,`monthly_fee_revenue`) 
VALUES('{$date}','{$item['service_id']}','{$Member[0]}','{$CancelByUser[0]}','{$CancelBySystem[0]}','{$RealDevelopment}','{$TotalUser}','{$ChargableSubs[0]}','{$DayFii[0]}')";
    echo $sqlInsert;
    try {
        $sqlCheck = 'SELECT id FROM reporting WHERE date(`time`)="' . $date . '" and service_id=' . $item['service_id'];
        $Check = $conn->doSelectOne($sqlCheck);
        if (!$Check) {
            $conn->doUpdate($sqlInsert);
            echo "\nInsert Service = " . $item['service_id'] . " | " . $date;
        }
    } catch (Exception $e) {
    }
}
?>