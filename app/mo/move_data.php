<?php
//error_reporting(false);
require_once("lib/config.php");
require_once("lib/functions.php");
require_once("lib/sbMysqlPDO.class.php");

try {
    $conn = new sbMysqlPDO($server, $user, $password, $db);
    var_dump($conn);
} catch (Exception $e) {
    file_put_contents($realPath . "log/connect_log.txt", "\nError when connect to database server|" . date("Y-m-d H:i:s", time()));
    sleep(5);
}



$dateMove = date("Y-m-d",time()-24*3600);



//SMS MO
$sqlMo =  " INSERT INTO sms_mo_backup SELECT * FROM sms_mo WHERE date(created_datetime)='{$dateMove}' ";
$conn->doUpdate($sqlMo);

$dateMoDelete = date("Y-m-d",time()-120*24*3600);
$sqlDeleteMo = " DELETE FROM sms_mo WHERE  created_datetime<='{$dateMoDelete}'";
$conn->doUpdate($sqlDeleteMo);


//SMS MT


$sqlMt =  " INSERT INTO sms_mt_backup SELECT * FROM sms_mt WHERE date(send_datetime)='{$dateMove}' ";
$conn->doUpdate($sqlMt);

$dateMTDelete = date("Y-m-d",time()-10*24*3600);
$sqlDeleteMt = " DELETE FROM sms_mt WHERE  send_datetime<='{$dateMTDelete}'";
$conn->doUpdate($sqlDeleteMt);


//CHARGING

$sqlCharge =  " INSERT INTO charging_history_backup SELECT * FROM charging_history"
            . " WHERE date(start_charging_date)='{$dateMove}' ";
$conn->doUpdate($sqlCharge);

$dateChargeDelete = date("Y-m-d",time()-60*24*3600);
$sqlDeleteCharge = " DELETE FROM charging_history WHERE  start_charging_date<='{$dateChargeDelete}'";
$conn->doUpdate($sqlDeleteMt);


?>
