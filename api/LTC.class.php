<?php
error_reporting(true);
require_once("lib/MysqlPDO.php");
require_once("lib/constant.php");
require_once("lib/common.php");
require_once("lib/Database.php");


class LTC{

    private $pdo;

    function __construct(){
        
    }
	
    public function sendSMS($username='',$password='',$sender='',$phonenumber='',$content='',$senddate=''){
       
        $conn = new MysqlPDO(DB_HOST,DB_NAME,DB_USER,DB_PWD);
        $conn->doSelectOne("SET NAMES 'utf8'");
        //check login 
        $password = md5($password);
        $sqlCheckLogin = " SELECT * FROM account WHERE username='{$username}' AND password='{$password}' ";
        $account = $conn->doSelectOne($sqlCheckLogin);

        if($account){
            //Login ok
            $sqlCheckSender = " SELECT * FROM service WHERE send_from='{$sender}' or shortcode='{$sender}' ";
            $service = $conn->doSelectOne($sqlCheckSender);
            if(substr_count($account['service'],"[{$service['id']}]")){
                //Have grant
                $sendTime = date("Y-m-d H:i:s",strtotime($senddate));
                $phonenumber = "856".$phonenumber;
                $sqlInsertSMS = "
                                    INSERT INTO sms_mt(sender,receiver,service,command_code,content_type,content,send_datetime,status,charging_status,service_id)
                                    VALUES('{$sender}','{$phonenumber}','{$service['service']}','Free','0',
                                           '{$content}','{$sendTime}',0,2,'{$service['id']}')
                                ";
                $conn->doUpdate($sqlInsertSMS);
                $result = array("status"=>"1","description"=>"Send sms successfully");
            }else{
                //Not allow
                $result = array("status"=>"98","description"=>"Your account can't send from {$sender}'");
            }
        }else{
            //Not ok
            $result = array("status"=>"99","description"=>"Login fail, incorect username or password");
        }
        return $result;
    }
    
    public function fakeSMS($username='',$password='',$sender='',$shortcode='',$content='',$senddate=''){
       
        $conn = new MysqlPDO(DB_HOST,DB_NAME,DB_USER,DB_PWD);
        $conn->doSelectOne("SET NAMES 'utf8'");
        //check login 
        $password = md5($password);
        $sqlCheckLogin = " SELECT * FROM account WHERE username='{$username}' AND password='{$password}' ";
        $account = $conn->doSelectOne($sqlCheckLogin);

        if($account){
            //Login ok
            $sqlCheckSender = " SELECT * FROM service WHERE send_from='{$shortcode}' or shortcode='{$sender}' ";
            $service = $conn->doSelectOne($sqlCheckSender);
            $sender = "856".intval($sender);
            //Have grant
            $sendTime = date("Y-m-d H:i:s",strtotime($senddate));
            $sqlInsertSMS = "
                                INSERT INTO sms_mt(sender,receiver,service,command_code,content_type,content,send_datetime,status,charging_status,service_id)
                                VALUES('{$sender}','{$shortcode}','{$service['service']}','Free','0',
                                       '{$content}','{$sendTime}',0,2,'{$service['id']}')
                            ";
            $conn->doUpdate($sqlInsertSMS);
            $result = array("status"=>"1","description"=>"Send sms successfully");
            
        }else{
            //Not ok
            $result = array("status"=>"99","description"=>"Login fail, incorect username or password");
        }
        return $result;
    }

    public function getSMS($username,$password,$shortcode,$msisdn,$content){
        $conn = new MysqlPDO(DB_HOST,DB_NAME,DB_USER,DB_PWD);
        $conn->doSelectOne("SET NAMES 'utf8'");
        
        $password = md5($password);
        $sqlCheckLogin = " SELECT * FROM account WHERE username='{$username}' AND password='{$password}' ";
        $account = $conn->doSelectOne($sqlCheckLogin);
        if($account){
            //Login ok
           $sqlCheckSender = " SELECT * FROM service WHERE send_from='{$shortcode}' or shortcode='{$shortcode}' ";
            $service = $conn->doSelectOne($sqlCheckSender);
            if(substr_count($account['service'],"[{$service['id']}]")){
                /*
                if($msisdn){
                    $condition .= " sender='856{$msisdn}' ";
                }
                if($content){
                    if($msisdn){
                        $condition = " AND content LIKE '%{$content}%' ";
                    }else{
                        $condition = " content LIKE '%{$content}%' ";
                    }
                }
                if($condition){
                    $condition = " OR (".$condition.") ";
                }
                 * 
                 */
                $sqlSelect = " SELECT * FROM sms_mo WHERE service='{$shortcode}' AND get_status=0 {$condition}  ";
                $items = $conn->doSelect($sqlSelect);
                if($items){
                    foreach($items as $item){
                        
                        $string = $item['content'];
                        //$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
                        $string = preg_replace('/[^A-Za-z0-9\-\n ]/', '', $string); // Removes special chars.
                        $string = preg_replace('/-+/', '-', $string);
                        $sms .="{$item['service']}|{$item['sender']}|{$string}|{$item['created_datetime']}>>>";
                        
                        $sqlUpdate = "UPDATE sms_mo SET get_status=1 WHERE id='{$item['id']}' ";
                        $conn->doUpdate($sqlUpdate);
                    }
                }
                $result = array("status"=>"1","description"=>"get sms successfully'","sms"=>$sms);
            }else{
                //Not allow
                $result = array("status"=>"98","description"=>"Your account can't get from {$sender}'","sms"=>"");
            }
        }else{
            //Not ok
            $result = array("status"=>"99","description"=>"Login fail, incorect username or password","sms"=>"");
        }
        return $result;
    }

}
?>