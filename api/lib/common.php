<?php

function getSubType($msisdn){
    global $_vasgateway;
    $postVar = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:vas="http://www.unitel.com.la/vasgateway/">
		   <soapenv:Header/>
		   <soapenv:Body>
			  <vas:Input>
				 <username>'.$_vasgateway['username'].'</username>
				 <password>'.$_vasgateway['password'].'</password>
				 <wscode>viewinfo</wscode>
				 <!--1 or more repetitions:-->
				<param name="GWORDER" value="1"/>
				<param name="input" value="'.$msisdn.'"/>
			  </vas:Input>
		   </soapenv:Body>
		</soapenv:Envelope>';
    //sfContext::getInstance()->getLogger()->err("Charge:SOAPXML:".$postVar);
    //open connection
    $ch = curl_init();
    //set the url, number of POST vars, POST data
    $options = array(
            CURLOPT_URL	       => $_vasgateway['address'],
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects {$channelInfo['static_name']}
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => $postVar,
            CURLOPT_HTTPHEADER	   => array('Content-Type: text/xml; charset=utf-8', 'Content-Length: '.strlen($postVar) ),
    );

    //open connection
    $ch = curl_init();
    curl_setopt_array( $ch, $options );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );
    $responeContent = curl_exec($ch);
    $result = array();
    if($err || !$responeContent)
    {
        $result = array();
    }
    else
    {
        //$responeContent = '77|The isdn does not exist';
        $subStr = substr($responeContent, strpos($responeContent, '<return>')+8);
        $responeContent = substr($subStr, 0, strpos($subStr, '</return>'));
        $result = explode('|', $responeContent);
    }
    
    return $result;
}

function subscribe($msisdn,$subType){
    $msisdn = "856".$msisdn;
    $time_start = microtime_float();
    $returnValue = array();
    $results = "";
    $e = "";
    try{
        $client = new SoapClient('http://10.78.6.226/ZSmartService/UserService.asmx?wsdl');
        $subinfo->MSISDN = $msisdn;
        $subinfo->PricePlanID = $subType;
	$subinfo->EffDate = "2011-09-09";
        $subinfo->ExpDate = "2012-07-28";
        try{
            $results = $client->AddUserIndiPricePlan($subinfo);
        }catch (Exception $e){
            $returnValue = array("error_code"=>"998","error_description"=>$e->faultstring);
        }
        if(!$returnValue['error_code']){
            if($results){
                $returnValue = array("error_code"=>"0");
            }else{
                $returnValue = array("error_code"=>"997","error_description"=>"NULL OBJECT");
            }
        }
    }catch(Exception $e){
         $returnValue = array("error_code"=>"999","error_description"=>"CAN'T CONNECT TO OCS SERVICE");
    }
    $time_end = microtime_float();
    $time = $time_end - $time_start;
    if($time>=6){
       file_put_contents("ocs_slow_log.txt",$time."|".$msisdn."|".date("Y-m-d H:i:s",time())."|".$returnValue['error_code']."|".$e."\n",FILE_APPEND);
    }else{
       file_put_contents("ocs_normal_log.txt",$time."|".$msisdn."|".date("Y-m-d H:i:s",time())."|".$returnValue['error_code']."|".$e."\n",FILE_APPEND);
    }
    //$returnValue = array("error_code"=>"0");
    return $returnValue;
}


function unsubscribe($msisdn,$subType){
    $msisdn = "856".$msisdn;
    $time_start = microtime_float();
    $returnValue = array();
    $results = "";
    $e = "";
    try{
        $client = new SoapClient('http://10.78.6.226/ZSmartService/UserService.asmx?wsdl');
        $subinfo->MSISDN = $msisdn;
        $subinfo->PricePlanID = $subType;
        $subinfo->ExpDate = "2012-07-28";
        try{
            $results = $client->ModUserIndiPricePlan($subinfo);
        }catch (Exception $e){
            $returnValue = array("error_code"=>"998","error_description"=>$e->faultstring);
        }
        if(!$returnValue['error_code']){
            var_dump($results);
            if($results){
                $returnValue = array("error_code"=>"0");
            }else{
                $returnValue = array("error_code"=>"997","error_description"=>"NULL OBJECT");
            }
        }
    }catch(Exception $e){
         $returnValue = array("error_code"=>"999","error_description"=>"CAN'T CONNECT TO OCS SERVICE");
    }
    $time_end = microtime_float();
    $time = $time_end - $time_start;
    if($time>=6){
       file_put_contents("ocs_unsubscribe_slow_log.txt",$time."|".$msisdn."|".date("Y-m-d H:i:s",time())."|".$returnValue['error_code']."|".$e."\n",FILE_APPEND);
    }else{
       file_put_contents("ocs_unsubscribe_normal_log.txt",$time."|".$msisdn."|".date("Y-m-d H:i:s",time())."|".$returnValue['error_code']."|".$e."\n",FILE_APPEND);
    }
    //$returnValue = array("error_code"=>"0");
    return $returnValue;
}

function xml2assoc($xml) {
    $tree = null;
    while($xml->read())
        switch ($xml->nodeType) {
            case XMLReader::END_ELEMENT: return $tree;
            case XMLReader::ELEMENT:
                $node = array('tag' => $xml->name, 'value' => $xml->isEmptyElement ? '' : xml2assoc($xml));
                if($xml->hasAttributes)
                    while($xml->moveToNextAttribute())
                        $node['attributes'][$xml->name] = $xml->value;
                $tree[] = $node;
            break;
            case XMLReader::TEXT:
            case XMLReader::CDATA:
                $tree .= $xml->value;
        }
    return $tree;
}

function microtime_float(){
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}
?>