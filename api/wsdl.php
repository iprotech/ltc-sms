<?php
error_reporting(false);
require_once("lib/nusoap/nusoap.php");
$namespace = "http://115.84.105.172:8888/sms.wsdl";
 
// create a new soap server
$server = new soap_server();
 
// configure our WSDL
$server->configureWSDL("LTCSMS");
 
// set our namespace
$server->wsdl->schemaTargetNamespace = $namespace;
 
//Register a method that has parameters and return types
$server->register(
// method name:
'sendSMS',
// parameter list:
array('username'=>'xsd:string','password'=>'xsd:string','sender'=>'xsd:string','phonenumber'=>'xsd:string','content'=>'xsd:string','senddate'=>'xsd:string'),
array('status'=>'xsd:string','description'=>'xsd:string'),
// namespace:
$namespace,
// soapaction: (use default)
false,
// style: rpc or document
'rpc',
// use: encoded or literal
'encoded',
// description: documentation for the method
'Send sms to phonenumber');
 

$server->register(
// method name:
'getSMS',
// parameter list:
array('username'=>'xsd:string','password'=>'xsd:string','shortcode'=>'xsd:string','msisdn'=>'xsd:string','content'=>'xsd:string'),
array('status'=>'xsd:string','description'=>'xsd:string','sms'=>'xsd:string'),
// namespace:
$namespace,
// soapaction: (use default)
false,
// style: rpc or document
'rpc',
// use: encoded or literal
'encoded',
// description: documentation for the method
'Get all sms send from customer to the shortcode');

 
// Get our posted data if the service is being consumed
// otherwise leave this data blank.
$POST_DATA = isset($GLOBALS['HTTP_RAW_POST_DATA']) ? $GLOBALS['HTTP_RAW_POST_DATA'] : '';
 
// pass our posted data (or nothing) to the soap service
$server->service($POST_DATA);
exit(); 


?>
