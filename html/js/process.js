(function($, window, document, undefined) {
    var process = function() {

    };

    process.prototype = {
        time: 0,
        lastLine: "",
        url: undefined,
        init: function() {
            var that = this;
            that.url = $("#process").attr("url-read");
            that.connect();
        },
        connect: function() {
            var that = this;
            $.ajax({
                type: "GET",
                url: that.url,
                async: true,
                cache: false,
                data: {
                    lastLine: that.lastLine,
                    time: that.time
                },
                dataType: "json",
                success: function(response) {
                    if (response.status === true && response.data) {
                        that.time = response.time;
                        that.lastLine = response.lastLine;
                        console.log(response.lastLine);
                        
                        /*
                        $(response.data).each(function(key, val) {

                        });
                        */
                        var addToList = function(data, key) {
                            var val = data[key];
                            var li = $("<li style='display:none;'>");
                            li.append($("<span style='width: 15%'>").html(val.time));
                            li.append($("<span style='width: 10%'>").html(val.action));
                            li.append($("<span style='width: 10%'>").html(val.memberId));
                            li.append($("<span style='width: 10%'>").html(val.msisdn));
                            li.append($("<span style='width: 10%'>").html(val.statusName));
                            li.append($("<span style='width: 35%'>").html(val.info));

                            if (val.status == 99) {
                                li.addClass("error");
                            } else if (val.status == 1) {
                                li.addClass("complete");
                            } else {
                                li.addClass("warring");
                            }

                            var $target = $("#process > ul");
                            $target.prepend(li);
                            li.slideDown(50, function() {
                                if (key < data.length - 1) {
                                    addToList(data, key + 1);
                                }
                            });
                        };
                        addToList(response.data, 0);
                    } else {

                    }
                    setTimeout(function() {
                        that.connect.apply(that);
                    }, 2000);
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    //alert("error: " + textStatus + "  " + errorThrown);
                    setTimeout(function() {
                        that.connect.apply(that);
                    }, 5000);
                }
            });
        }
    };

    $.process = new process();

    $(document).ready(function() {
        $.process.init();
    });

})(jQuery, window, document);