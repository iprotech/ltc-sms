(function($, document, undefined) {

    var RandomWinner = function() {

    };

    RandomWinner.prototype = {
        options: {
            numbers: {
                n1: ["2", "2", "2", "2", "3", "2", "2", "2"],
                n2: ["0"],
                n3: ["5"],
                n4: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
                n5: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
                n6: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
                n7: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
                n8: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
                n9: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
                n10: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
            }
        },
        items: {
        },
        primaryNumber: [
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
        ],
        getRandomInt: function(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        },
        randomTemp: function() {
            var that = this;
            that.items.i2x.html(that.randomTempOne());
            that.items.i2y.html(that.randomTempOne());
            that.items.i3x.html(that.randomTempOne());
            that.items.i3y.html(that.randomTempOne());
            that.items.i4x.html(that.randomTempOne());
            that.items.i4y.html(that.randomTempOne());
            that.items.i5x.html(that.randomTempOne());
            that.items.i5y.html(that.randomTempOne());
        },
        randomTempOne: function() {
            var that = this;
            var firstNumber = that.randomFirstNumber();
            if (firstNumber == "205") {
                return firstNumber + that.getRandomInt(0, 9999999).toString();
            } else if (firstNumber == "305") {
                return firstNumber + that.getRandomInt(0, 999999).toString();
            }
        },
        randomFirstNumber: function() {
            var that = this;
            var n1 = that.options.numbers.n1[that.getRandomInt(0, that.options.numbers.n1.length - 1)];
            var n2 = that.options.numbers.n2[that.getRandomInt(0, that.options.numbers.n2.length - 1)];
            var n3 = that.options.numbers.n3[that.getRandomInt(0, that.options.numbers.n3.length - 1)];
            return n1 + n2 + n3;
        },
        randomPrimary: function(input) {
            var that = this;
            input = input.toString();
            if (that.primaryNumber[0] !== input.substr(0, 3)) {
                that.primaryNumber[0] = that.randomFirstNumber();
            }
            if (that.primaryNumber[1] !== input.substr(3, 1)) {
                that.primaryNumber[1] = that.options.numbers.n4[that.getRandomInt(0, that.options.numbers.n4.length - 1)];
            }
            if (that.primaryNumber[2] !== input.substr(4, 1)) {
                that.primaryNumber[2] = that.options.numbers.n5[that.getRandomInt(0, that.options.numbers.n5.length - 1)];
            }
            if (that.primaryNumber[3] !== input.substr(5, 1)) {
                that.primaryNumber[3] = that.options.numbers.n6[that.getRandomInt(0, that.options.numbers.n6.length - 1)];
            }
            if (that.primaryNumber[4] !== input.substr(6, 1)) {
                that.primaryNumber[4] = that.options.numbers.n7[that.getRandomInt(0, that.options.numbers.n7.length - 1)];
            }
            if (that.primaryNumber[5] !== input.substr(7, 1)) {
                that.primaryNumber[5] = that.options.numbers.n8[that.getRandomInt(0, that.options.numbers.n8.length - 1)];
            }
            if (that.primaryNumber[6] !== input.substr(8, 1)) {
                that.primaryNumber[6] = that.options.numbers.n9[that.getRandomInt(0, that.options.numbers.n9.length - 1)];
            }
            if (that.primaryNumber[7] !== input.substr(9, 1)) {
                that.primaryNumber[7] = that.options.numbers.n10[that.getRandomInt(0, that.options.numbers.n10.length - 1)];
            }

            var result = that.primaryNumber[0]
                    + that.primaryNumber[1]
                    + that.primaryNumber[2]
                    + that.primaryNumber[3]
                    + that.primaryNumber[4]
                    + that.primaryNumber[5]
                    + that.primaryNumber[6]
                    + that.primaryNumber[7];

            return result;
        },
        init: function() {
            var that = this;
            that.items.i1 = $("#list-number > .top1").first();
            that.items.i2x = $("#list-number > .top2").first();
            that.items.i2y = $("#list-number > .top2").last();
            that.items.i3x = $("#list-number > .top3").first();
            that.items.i3y = $("#list-number > .top3").last();
            that.items.i4x = $("#list-number > .top4").first();
            that.items.i4y = $("#list-number > .top4").last();
            that.items.i5x = $("#list-number > .top5").first();
            that.items.i5y = $("#list-number > .top5").last();

            var sleepTime = 300;
            var randomTime = 5;

            var winner = $("#txtResult").val().split(",");
            var totalTime = parseInt($("#txtTotalTime").val());

            var i = 0;
            var t = 0;
            var tMax = (totalTime - (winner.length * sleepTime) - 1200) / winner.length;
            if (tMax < 0) {
                tMax = randomTime;
            }
            setTimeout(rd, 1200);

            function rd() {
                that.randomTemp();
                that.items.i1.html(that.randomTempOne());
                t += randomTime;
                if (t > tMax) {
                    $("#wrapper-winner > ul").prepend($("<li>").html((winner.length - i).toString() + ". " + winner[i]));
                    that.items.i1.html(winner[i]);
                    i++;
                    t = 0;
                    if (i < winner.length) {
                        setTimeout(rd, sleepTime);
                    }
                } else {
                    setTimeout(rd, randomTime);
                }

                /*
                 var result = that.randomPrimary(winner[i]);
                 that.randomTemp();
                 that.items.i1.html(result);
                 if (result != winner[i]) {
                 setTimeout(rd, 10);
                 } else {
                 $("#wrapper-winner > ul").prepend($("<li>").html((winner.length - i).toString() + ". " + result));
                 that.primaryNumber = [
                 "",
                 "",
                 "",
                 "",
                 "",
                 "",
                 "",
                 ""
                 ];
                 i++;
                 if (i < winner.length) {
                 setTimeout(rd, 300);
                 }
                 }
                 */
            }
        }
    };

    $.randomWinner = new RandomWinner();

    $(document).ready(function() {
        $.randomWinner.init();
    });
})(jQuery, document);