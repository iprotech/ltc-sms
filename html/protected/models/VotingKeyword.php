<?php

/**
 * This is the model class for table "voting_keyword".
 *
 * The followings are the available columns in table 'voting_keyword':
 * @property integer $id
 * @property string $keyword
 * @property string $reply_content
 * @property integer $total_vote
 * @property integer $service_id
 */
class VotingKeyword extends CActiveRecord {

    public $time_start, $time_end;
    public $filename = 'Report_Voting';
	public $export_id;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return VotingKeyword the static model class
     */
    public function getService() {
        $sql = '';
        $service_ids = User::model()->findByPk(Yii::app()->user->id)->service;
		$service_gp = User::model()->findByPk(Yii::app()->user->id)->group_id;
		if($service_gp !=1){
        if ($service_ids) {
            preg_match_all('/\[(.*?)\]/', $service_ids, $ids);
            for ($i = 0; $i < count($ids[1]); $i++) {
                $sql .= 'id=' . $ids[1][$i] . ' || ';
            }
            $sqlfull = '(' . substr($sql, 0, -3) . ')';
        }
		}
		else {
            $sqlfull = '1';
        }

        $list = array();
		$cmd = Yii::app()->db->createCommand('SELECT service.id,service.service FROM service WHERE type = 7 or category_id = 3');
        //$cmd = Yii::app()->db->createCommand('SELECT service.id,service.service FROM service WHERE ' . $sqlfull . ' && type=7');
        $data = $cmd->queryAll();
        $list[''] = 'Select';
        foreach ($data as $item) {
            $list[$item['id']] = $item['service'];
        }
        return $list;
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'voting_keyword';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            //array('total_vote, service_id', 'numerical', 'integerOnly'=>true),
            array('keyword, reply_content', 'length', 'max' => 1024),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, keyword, reply_content, total_vote, service_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'keyword' => 'Keyword',
            'reply_content' => 'Reply Content',
            'total_vote' => 'Total Vote',
            'service_id' => 'Service',
        );
    }

    public function getTotal($service_id) {
        if (isset($service_id) && $service_id)
            $sql = 'SELECT SUM(total_vote) as total FROM voting_keyword WHERE service_id=' . $service_id;
        else
            $sql = 'SELECT SUM(total_vote) as total FROM voting_keyword';
        $cmd = Yii::app()->db->createCommand($sql);
        $data = $cmd->queryRow();
        if ($data['total'] > 0)
            return $data['total'];
        else
            return '1';
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public $setHeight1, $setHeight2, $serName, $percent;

    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
		$export = Yii::app()->request->getParam('export');
        $criteria = new CDbCriteria;
        $service_ids = User::model()->findByPk(Yii::app()->user->id)->service;
        preg_match_all('/\[(.*?)\]/', $service_ids, $ids);
		$service_gp = User::model()->findByPk(Yii::app()->user->id)->group_id;
        $criteria->alias = "k";
        $criteria->select = "k.id, k.keyword, k.name, k.reply_content, s.service AS serName, SUM(l.total_vote) AS total_vote";
        $criteria->join = "INNER JOIN service AS s ON s.id = k.service_id INNER JOIN voting_log AS l ON l.keyword_id = k.id";
        $criteria->group = "k.id";
		$criteria->addCondition('voting_error_status ='."0");
		$criteria->addCondition('l.voting_error_status!=1');
        if ($this->service_id) {
            $criteria->addCondition('service_id ='. $this->service_id);
        } else {
            if($service_gp !=1){
                for ($i = 0; $i < count($ids[1]); $i++) {
					if ($i == 0)
						$criteria->condition =('k.service_id ='. $ids[1][$i]);
					else
						$criteria->addCondition('k.service_id=' . $ids[1][$i], 'OR');

				}	
			}
        }
        if ($this->time_start) {
            $criteria->compare('l.created_datetime', '>=' . $this->time_start);
        }
        if ($this->time_end) {
            $criteria->compare('l.created_datetime', '<=' . $this->time_end);
        }
	if($export){
            $arCrite = $criteria->toArray();
            $sql = " SELECT {$arCrite['select']} FROM ". $this->tableName() ." AS {$arCrite['alias']} {$arCrite['join']} WHERE {$arCrite['condition']} GROUP BY {$arCrite['group']}";
            if(isset($export))
               $status = 0;
               else $status =1;
            $query = "insert into command_query (content,created_datetime,filename,status) values (:content,:created,:filename,:status)";
            $parameters = array(":content"=>$sql,":created" => date('Y-m-d H:i:s'),":filename"=>  $this->filename,":status"=>$status);
            $cmd = Yii::app()->db->createCommand($query)->execute($parameters);
            if(isset($cmd)){
                $this->export_id  = Yii::app()->db->getLastInsertID();
            }
	}

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 50),
        ));
    }

    public function afterFind() {
        $total = $this->getTotal($this->service_id);
        $this->setHeight2 = 500 * $this->total_vote / $total;
        $this->setHeight1 = 500 - $this->setHeight2;
        $this->percent = 100 * $this->total_vote / $total;

        return parent::afterFind();
    }

}
