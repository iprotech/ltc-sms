<?php

/**
 * This is the model class for table "charging_history".
 *
 * The followings are the available columns in table 'charging_history':
 * @property integer $id
 * @property integer $member_id
 * @property string $msisdn
 * @property string $start_charging_date
 * @property string $end_charging_date
 * @property integer $total_money
 * @property integer $service_id
 * @property integer $mo_id
 * @property integer $mt_id 
 * @property string $service_name
 * @property integer $charging_type
 * @property integer $convert_status
 * @property integer $delete_status 
 * @property integer $sub_type
 * @property integer $cdr_status
 * @property string $shortcode 
 * @property string $keyword 
 * @property string $old_balance 
 * @property string $new_balance 
 * @property integer $total_vote 
 */
class chargingHistory extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return chargingHistory the static model class
     */
    public $member;
    public $time_start;
    public $time_end;
    public $totalmoney;
    public $totalvote;
    public $category_id;
    public $option;
    public $filename = 'Report_Revenue';
	public $export_id;
    /*
     * Properties
     */
    public $sub_type_name;

    public function getType() {
        if ($this->sub_type >= 1 && $this->sub_type <= 6) {
            return 'Pre-paid';
        } else if ($this->sub_type == 9) {
            return 'Post-paid';
        } else {
            return 'Unknown';
        }
    }

    public function afterFind() {
        $this->start_charging_date = date("H:i:s d/m/Y", strtotime($this->start_charging_date));
        $this->sub_type_name = $this->getType();
        
        parent::afterFind();
    }

    public function beforeFind() {
        parent::beforeFind();
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'charging_history';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('member_id, start_charging_date, end_charging_date, total_money, service_id, mo_id, service_name', 'required'),
            array('member_id, total_money, service_id, mo_id, charging_type, convert_status', 'numerical', 'integerOnly' => true),
            array('service_name', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, sub_type, option, category_id,service_name, shortcode, member_id, delete_status, time_start, time_end, start_charging_date, end_charging_date, service_id, msisdn', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'member_id' => 'Member',
            'start_charging_date' => 'Start Charging Date',
            'end_charging_date' => 'End Charging Date',
            'total_money' => 'Total Money',
            'service_id' => 'Service',
            'mo_id' => 'Mo',
            'service_name' => 'Service Name',
            'charging_type' => 'Charging Type',
            'convert_status' => 'Convert Status',
            'category_id' => 'Category',
            'sub_type_name' => 'Sub Type',
            'keyword' => 'Keyword',
            'total_vote' => 'Vote Time'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public $shortcode;

    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
		$export = Yii::app()->request->getParam('export');
        $criteria = new CDbCriteria;
        $criteria->alias = 'a';
        $criteria->compare('id', $this->id);
        if ($this->service_id) {
            $criteria->compare('a.service_id', $this->service_id);
        }  else {
            $service_ids = User::model()->findByPk(Yii::app()->user->id)->service;
            preg_match_all('/\[(.*?)\]/', $service_ids, $ids);
			$service_gp = User::model()->findByPk(Yii::app()->user->id)->group_id;
            $data = array();
            $cond = '';
            
            if ($this->category_id) {
                $sqlCate = '';
                $rsCate = Service::model()->findAllByAttributes(array('category_id' => $this->category_id));
                $sqlCate .= ' a.service_id = 99999999999 || ';
                foreach ($rsCate as $k) {
                    if (in_array($k->id, $ids[1]))
                        $sqlCate .= 'a.service_id = ' . $k->id . ' || ';
                }
                $sqlFull = substr($sqlCate, 0, -3);
                if ($sqlFull != '') {
                    $criteria->condition = $sqlFull;
                }
            } else {
			if($service_gp !=1){
                for ($i = 0; $i < count($ids[1]); $i++) {
                    if ($i == 0)
                        $criteria->condition = ('a.service_id ='. $ids[1][$i]);
                    else
                        $criteria->addCondition('a.service_id = ' . $ids[1][$i], 'OR');
                }
            }
			}
        }
        
        if ($this->msisdn) {
            $criteria->addCondition("a.msisdn =". $this->msisdn);
        }
        if ($this->shortcode) {
            $criteria->addCondition("a.shortcode =". $this->shortcode);
        }
        if ($this->time_start) {
            $criteria->addCondition("a.start_charging_date > '$this->time_start'" );
        }
        if ($this->time_end) {
            $criteria->addCondition("a.start_charging_date < '$this->time_end'" );
        }
        
        if ($this->sub_type) {
            if ($this->sub_type == 3) {
                $criteria->addCondition('(a.sub_type > 6 AND a.sub_type < 9) OR a.sub_type = 0 OR a.sub_type > 9');
            } else if ($this->sub_type == 1) {
                //$criteria->addCondition("a.sub_type >= 1 AND a.sub_type <= 6");
                $criteria->addBetweenCondition("a.sub_type", "1", "6");
            } else if ($this->sub_type == 2) {
                $criteria->addCondition("a.sub_type =9");
            }
        }

        if ($this->option == 1) {
            
        } else if ($this->option == 2) {
            $criteria->addCondition("a.msisdn IN (SELECT mc.msisdn FROM member_check AS mc)");
        } else if ($this->option == 3) {
            $criteria->addCondition("a.msisdn NOT IN (SELECT mc.msisdn FROM member_check AS mc)");
        }

        $criteria->addCondition("a.delete_status = 0");

        /* Get Total Moeny */
        $criteria->select = "SUM(a.total_money) AS totalmoney, SUM(a.total_vote) AS totalvote";
        
        $tempData = $this->findAll($criteria);
        if (count($tempData) > 0) {
            $this->totalmoney = number_format($tempData[0]->totalmoney);
            $this->totalvote = number_format($tempData[0]->totalvote);
        }
        
        /*
        $tempData = $this->find($criteria);
        $this->totalmoney = number_format($tempData->totalmoney);
        */
        /* ----------------- */
        
        $criteria->order = 'id DESC';
        $criteria->select = 'a.id,a.keyword,a.total_vote,a.old_balance,a.new_balance,a.sub_type, a.shortcode, a.msisdn, a.member_id, a.start_charging_date, a.end_charging_date, a.service_name, a.total_money, a.service_id';
	
	if($export){
           $arCrite = $criteria->toArray();
           $sql = " SELECT {$arCrite['select']} FROM ". $this->tableName() ." AS {$arCrite['alias']} {$arCrite['join']} WHERE {$arCrite['condition']} ORDER BY {$arCrite['order']} ";
           if(isset($export))
                $status = 0;
               else $status = 1;
            $query = "insert into command_query (content,created_datetime,filename,status) values (:content,:created,:filename,:status)";
            $parameters = array(":content"=>$sql,":created" => date('Y-m-d H:i:s'),":filename"=>  $this->filename,":status"=>$status);
            $cmd = Yii::app()->db->createCommand($query)->execute($parameters);
            if(isset($cmd)){
                $this->export_id  = Yii::app()->db->getLastInsertID();
            }
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => '25'),
        ));
    }

}
