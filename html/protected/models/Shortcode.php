<?php

/**
 * This is the model class for table "shortcode".
 *
 * The followings are the available columns in table 'shortcode':
 * @property integer $id
 * @property string $shortcode
 * @property string $help_content
 * @property string $invalid_content
 */
class Shortcode extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Shortcode the static model class
     */
    public $filename = 'Report_Shortcode';
    public $export_id;
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'shortcode';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('shortcode, help_content, invalid_content', 'required'),
            array('shortcode', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, shortcode, help_content, invalid_content', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'shortcode' => 'Shortcode',
            'help_content' => 'Help Content',
            'invalid_content' => 'Invalid Content',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
   
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $export = Yii::app()->request->getParam('export');
        $criteria = new CDbCriteria;
        $criteria->alias = 'a';
        $criteria->select = 'a.*';
        if($this->id)
        $criteria->condition = ('id ='. $this->id);
        if($this->shortcode)
        $criteria->addCondition ('shortcode ='. $this->shortcode);
        $criteria->compare('help_content', $this->help_content, true);
        $criteria->compare('invalid_content', $this->invalid_content, true);
        if($export){
            $arCrite = $criteria->toArray();
			if($this->id && $this->shortcode)
                $arCrite['condition'] = $arCrite['condition'];
            else $arCrite['condition'] = '1=1';
            $sql = " SELECT {$arCrite['select']} FROM ". $this->tableName() ." AS {$arCrite['alias']} WHERE {$arCrite['condition']}";
            if(isset($export))
               $status = 0;
               else $status = 1;
            $query = "insert into command_query (content,created_datetime,filename,status) values (:content,:created,:filename,:status)";
            $parameters = array(":content"=>$sql,":created" => date('Y-m-d H:i:s'),":filename"=>  $this->filename,":status"=>$status);
            $cmd = Yii::app()->db->createCommand($query)->execute($parameters);
            if(isset($cmd)){
                $this->export_id  = Yii::app()->db->getLastInsertID();
            }
	}
        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

}