<?php

/**
 * This is the model class for table "sms_mt".
 *
 * The followings are the available columns in table 'sms_mt':
 * @property integer $id
 * @property integer $mo_id
 * @property string $sender
 * @property string $service
 * @property string $receiver
 * @property string $command_code
 * @property integer $content_type
 * @property integer $status
 * @property string $content
 * @property string $send_datetime
 * @property string $receive_datetime
 * @property integer $send_type
 */
class SmsMt extends CActiveRecord {

    public $start_date;
    public $end_date;
    public $category_id;
    public $option;
    public $filename = 'Report_SMSMT';
	public $export_id;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return SmsMt the static model class
     */
    public function getChargingStatus() {
        switch ($this->charging_status) {
            case 0:
                return 'Pending';
                break;
            case 1:
                return 'Successful';
                break;
            case 2:
                return 'Not charge';
            default:
                return 'Error';
        }
    }

    public function getService() {
        $list = array();
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('shortcode')
                ->from('service')
                ->where('1');
        $data = $cmd->queryAll();
        $list[''] = 'Select';
        foreach ($data as $item) {
            $list[$item['shortcode']] = $item['shortcode'];
        }
        return $list;
    }

    public function getStatus() {
        switch ($this->status) {
            case 1:
                return 'Complete';
            case 3:
                return 'Stop';
            case 0:
                return 'Pending';
            default:
                return 'Error';
        }
    }
 
    public function afterFind (){
        $this->send_datetime = date("H:i:s d/m/Y",strtotime($this->send_datetime));
        parent::afterFind ();
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'sms_mt';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('mo_id, content_type, status, send_type', 'numerical', 'integerOnly' => true),
            array('sender, service, receiver', 'length', 'max' => 16),
            array('command_code, content', 'length', 'max' => 255),
            array('send_datetime, receive_datetime', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, mo_id, option, category_id,content_id, charging_status, service_id, sender, start_date, end_date, service, receiver, command_code, content_type, status, content, send_datetime, receive_datetime, send_type', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'mo_id' => 'Mo',
            'sender' => 'Sender',
            'service' => 'Shortcode',
            'receiver' => 'Receiver',
            'command_code' => 'Command Code',
            'content_type' => 'Content Type',
            'status' => 'Status',
            'content' => 'Content',
            'send_datetime' => 'Sent date',
            'receive_datetime' => 'Receive Datetime',
            'send_type' => 'Send Type',
            'category_id' => 'Category'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public $serviceName,$cmd;

    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $export = Yii::app()->request->getParam('export');
        $service_ids = User::model()->findByPk(Yii::app()->user->id)->service;
        preg_match_all('/\[(.*?)\]/', $service_ids, $ids);
		$service_gp = User::model()->findByPk(Yii::app()->user->id)->group_id;
        $criteria = new CDbCriteria;
        $criteria->alias = 'a';
        $criteria->select = 'a.*,b.service as serviceName';
        $criteria->join = 'INNER JOIN service AS b ON a.service_id=b.id';

        if ($this->service_id) {
            $criteria->condition = ('a.service_id='. $this->service_id);
        } else {
            if ($this->category_id) {
                $sqlCate = '';
                $rsCate = Service::model()->findAllByAttributes(array('category_id' => $this->category_id));
                foreach ($rsCate as $k) {
                    $sqlCate .= 'a.service_id = ' . $k->id . ' || ';
                }
                $sqlFull = substr($sqlCate, 0, -3);
                if ($sqlFull != '') {
                    $criteria->condition = $sqlFull;
                }
            } else {
			if($service_gp !=1){
                $sql    = '';
                for ($i = 0; $i < count($ids[1]); $i++) {
                    if ($i == 0)
                        $criteria->condition =('a.service_id='. $ids[1][$i]);
                    else
                        $criteria->addCondition('a.service_id=' . $ids[1][$i], 'OR');
                    
                    $rsService  = Service::model()->findByPk($ids[1][$i]);
		      if(isset($rsService) && $rsService)
                    		$sql    .= 'a.service='.$rsService->shortcode.' || ';
                }
                if($sql!='')
                    $sql    = ' && ('.substr($sql,0,-3).')';
                $criteria->addCondition('a.service_id=0'.$sql, 'OR');
            }
			}
        }
        $criteria->compare('a.id', $this->id);
        $criteria->compare('a.mo_id', $this->mo_id);
        $criteria->compare('a.sender', $this->sender, true);
        if($this->service)
        $criteria->addCondition('a.service ='. $this->service);
        
        if($this->receiver) {
            if($this->option==2)
                $criteria->join = 'INNER JOIN JOIN service AS b ON a.service_id=b.id INNER JOIN member_check ON a.receiver=member_check.msisdn AND a.receiver LIKE "%'.$this->receiver.'%"';
            else if($this->option==3) {
                $criteria->addCondition('a.receiver NOT IN (SELECT member_check.msisdn FROM member_check) AND a.receiver LIKE "%'.$this->receiver.'%"');
            } else
                $criteria->addCondition('a.receiver='. $this->receiver);
        } else {
            if($this->option==2)
                $criteria->join = 'INNER JOIN service AS b ON a.service_id=b.id INNER JOIN member_check ON a.receiver=member_check.msisdn';
            else if($this->option==3) {
                $criteria->addCondition('a.receiver NOT IN (SELECT member_check.msisdn FROM member_check)');
            }
        }
        
//        $criteria->compare('a.command_code', $this->command_code, true);
        if($this->status==''){
            $criteria->addCondition('a.status=1 || a.status = 0');}
        elseif($this->status == 1){
            $criteria->addCondition('a.status=1'); 
        }elseif($this->status == 0){
            $criteria->addCondition('a.status=0'); 
        }
        if($this->content_id == ''){
            $criteria->addCondition('a.content_id > 0 || a.content_id = 0');}
        elseif($this->content_id == 1){
            $criteria->addCondition('a.content_id > 0'); 
        }elseif($this->content_id == 0){
            $criteria->addCondition('a.content_id = 0'); 
        }
        if($this->content)
        $criteria->addCondition("a.content like '%$this->content%'");
        if($this->charging_status == ''){
            $criteria->addCondition('a.charging_status= 0 || a.charging_status =1');
        }elseif($this->charging_status == 0){
            $criteria->addCondition('a.charging_status = 0');
        }elseif($this->charging_status == 1){
            $criteria->addCondition('a.charging_status = 1');
        }
        if ($this->start_date)
            $criteria->addCondition("a.send_datetime >'$this->start_date'"  );
        if ($this->end_date)
            $criteria->compare("a.send_datetime < '$this->end_date'");
        $criteria->order = 'ID DESC';
            if($export){
                $arCrite = $criteria->toArray();
                $sql = " SELECT {$arCrite['select']} FROM ". $this->tableName() ." AS {$arCrite['alias']} {$arCrite['join']} WHERE {$arCrite['condition']} ORDER BY {$arCrite['order']}";
                if(isset($export))
                        $status = 0;
                        else $status = 1;
                $query = "insert into command_query (content,created_datetime,filename,status) values (:content,:created,:filename,:status)";
                $parameters = array(":content"=>$sql,":created" => date('Y-m-d H:i:s'),":filename"=>  $this->filename,":status"=>$status);
                $cmd = Yii::app()->db->createCommand($query)->execute($parameters);
                if(isset($cmd)){
                    $this->export_id  = Yii::app()->db->getLastInsertID();
                }
            }	
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => '25'),
        ));
    }

}