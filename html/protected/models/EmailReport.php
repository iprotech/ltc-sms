<?php

/**
 * This is the model class for table "email_report".
 *
 * The followings are the available columns in table 'email_report':
 * @property integer $id
 * @property string $email
 * @property string $service
 * @property string $type
 * @property integer $send_time
 */
class EmailReport extends CActiveRecord
{
    public $serviceList;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'email_report';
	}

    public function afterFind() {
        $this->serviceList = explode(',', $this->service);
        parent::afterFind();
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('email', 'required'),
			array('email, service, type', 'safe'),
			array('id, email, service, type, send_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'service' => 'Service',
			'type' => 'Type',
			'send_time' => 'Send Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('service',$this->service,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('send_time',$this->send_time);
        $criteria->order = 'id DESC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array('pageSize' => '25'),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EmailReport the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getService(){
        $sql = "SELECT `id`,`service` FROM `service` ORDER BY id ASC";
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        $list =array();
        foreach($data as $item){
            $list[$item['id']] = $item['service'];
        }
        return $list;
    }

    public function getType() {
        switch ($this->type) {
            case 1:
                return 'Daily';
                break;
            case 2:
                return 'Weekly';
                break;
            case 3:
                return 'Monthly';
                break;
            default:
                break;
        }
    }
    public $list,$name_service;
    public function getListService(){
        $id = explode(',',$this->service);
        $this->list='';
        foreach($id as $l){
            $service = Service::model()->findByPk($l);
            if($service){
                $this->list .= $service->service.',';
            }else{
                $this->list='';
            }
        }
        $this->name_service = substr($this->list, 0, -1);
        return $this->name_service;
    }
}
