<?php

/**
 * This is the model class for table "sms_mt".
 *
 * The followings are the available columns in table 'sms_mt':
 * @property integer $id
 * @property integer $mo_id
 * @property string $sender
 * @property string $service
 * @property string $receiver
 * @property string $command_code
 * @property integer $content_type
 * @property integer $status
 * @property string $content
 * @property string $send_datetime
 * @property string $receive_datetime
 * @property integer $send_type
 */
class SendSms extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return SmsMt the static model class
     */
    public $list_handle,$send_datetime;
    public $list_receiver, $receiveFile;
    public $sendby;
    public $is_billalert, $list_billalert, $BillFile;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'sms_mt';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('service_id', 'required'),
        //    array('sender, sendby','validSender'),
            array('content', 'length', 'max' => 560),
            array('list_receiver, list_billalert', 'file', 'allowEmpty' => true, 'types' => 'txt,csv')
        );
    }
    
    public function validSender($attribute, $params) {
        if($this->sender=='' && $this->sendby=='') {
                $this->addError ($attribute, 'Sender cannot be blank');
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'mo_id' => 'Mo',
            'sender' => 'Sender',
            'service' => 'Service',
            'receiver' => 'Receiver',
            'command_code' => 'Command Code',
            'content_type' => 'Content Type',
            'status' => 'Status',
            'content' => 'Content',
            'send_datetime' => 'Sent date',
            'receive_datetime' => 'Receive Datetime',
            'send_type' => 'Send Type',
        );
    }

}