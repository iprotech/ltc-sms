<?php

/**
 * This is the model class for table "command_code".
 *
 * The followings are the available columns in table 'command_code':
 * @property integer $id
 * @property integer $service_id
 * @property string $command_code
 * @property string $command_code_alias
 * @property integer $type
 * @property string $reply_content
 * @property string $description
 * @property string $created_datetime
 * @property integer $cmd_price 
 */
class CommandCode extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CommandCode the static model class
     */
    public $serviceName;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function getListService() {
        $list = array();
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('id,service')
                ->from('service')
                ->where('1');
        $data = $cmd->queryAll();
        foreach ($data as $item) {
            $list[$item['id']] = $item['service'];
        }
        return $list;
    }

    public function getCommandType() {
        switch ($this->type) {
            case 1:
                return 'ON';
            case 2:
                return 'OFF';
            case 3:
                return 'REMAIN';
            case 4:
                return 'RFREE';
            case 5:
                return 'HELP';
	    case 6:
                return 'MO';
            default:
                return '';
        }
    }

    public function tableName() {
        return 'command_code';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('service_id, command_code, type, reply_content,cmd_price', 'required'),
            array('service_id, type', 'numerical', 'integerOnly' => true),
            array('command_code, command_code_alias, description', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, service_id, command_code,cmd_price, command_code_alias, type, reply_content, description, created_datetime', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'service_id' => 'Service',
            'command_code' => 'Command Code',
            'command_code_alias' => 'Command Code Alias',
            'type' => 'Type',
            'reply_content' => 'Reply Content',
            'description' => 'Description',
            'created_datetime' => 'Created Datetime',
            'cmd_price' => 'Price'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $service_ids = User::model()->findByPk(Yii::app()->user->id)->service;
        preg_match_all('/\[(.*?)\]/', $service_ids, $ids);

        $criteria = new CDbCriteria;
        $criteria->alias = 's';
        $criteria->select = 's.id,s.service_id,s.command_code,s.cmd_price,s.command_code_alias,s.type,s.reply_content,s.description,s.created_datetime,c.service as serviceName';
        $criteria->join = 'INNER JOIN service as c ON c.id=s.service_id';

        $criteria->compare('id', $this->id);
        if ($this->service_id) {
            $criteria->compare('service_id', $this->service_id);
        } else {
            for ($i = 0; $i < count($ids[1]); $i++) {
                if ($i == 0)
                    $criteria->compare('service_id', $ids[1][$i]);
                else
                    $criteria->addCondition('service_id=' . $ids[1][$i], 'OR');
            }
        }
        $criteria->compare('command_code', $this->command_code, true);
        $criteria->compare('command_code_alias', $this->command_code_alias, true);
        $criteria->compare('type', $this->type);
        $criteria->compare('reply_content', $this->reply_content, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('cmd_price', $this->cmd_price, true);
        $criteria->compare('created_datetime', $this->created_datetime, true);
        $criteria->order = 'id DESC';

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array('pageSize' => '25'),
                ));
    }

}