<?php

/**
 * This is the model class for table "reporting".
 *
 * The followings are the available columns in table 'reporting':
 * @property integer $id
 * @property string $time
 * @property integer $service_id
 * @property integer $new_register
 * @property integer $cancel_by_user
 * @property integer $cancel_by_system
 * @property integer $real_development
 * @property integer $current_user
 * @property integer $chargable_subs
 * @property integer $monthly_fee_revenue
 */
class Reporting extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Reporting the static model class
     */
    public $serviceName;
    public $time_start;
    public $time_end;
    public $category_id;
    public $month,$year;
    public $filename = 'List_Reporting';
    public function getListService() {
        $list = array();
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('id,service')
                ->from('service')
                ->where('1');
        $data = $cmd->queryAll();
        $list[''] = 'Select';
        foreach ($data as $item) {
            $list[$item['id']] = $item['service'];
        }
        return $list;
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'reporting';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('time, service_id, new_register, cancel_by_user, cancel_by_system, real_development, current_user, chargable_subs, monthly_fee_revenue', 'required'),
            array('service_id, new_register, cancel_by_user, cancel_by_system, real_development, current_user, chargable_subs, monthly_fee_revenue', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id,month,year, time, category_id, time_start, time_end, service_id, new_register, cancel_by_user, cancel_by_system, real_development, current_user, chargable_subs, monthly_fee_revenue', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'time' => 'Date',
            'service_id' => 'Service',
            'new_register' => 'New Register',
            'cancel_by_user' => 'Cancel By User',
            'cancel_by_system' => 'Cancel By System',
            'real_development' => 'Real Development',
            'current_user' => 'Current User',
            'chargable_subs' => 'Chargable Subs',
            'monthly_fee_revenue' => 'Fee Revenue',
            'time_start' => 'Date Start',
            'time_end' => 'Date End',
            'category_id' => 'Category'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
	$export = Yii::app()->request->getParam('export');
        $service_ids = User::model()->findByPk(Yii::app()->user->id)->service;
        preg_match_all('/\[(.*?)\]/', $service_ids, $ids);
        $criteria = new CDbCriteria;
        $criteria->alias = 'a';
        $criteria->select = 'a.*,b.service as serviceName';
        $criteria->join = 'INNER JOIN service as b ON b.id=a.service_id';
        $criteria->compare('id', $this->id);
        $criteria->compare('time', $this->time, true);
        if ($this->service_id) {
            $criteria->compare('service_id', $this->service_id);
        } else {
            if ($this->category_id) {
                $sqlCate = '';
                $rsCate = Service::model()->findAllByAttributes(array('category_id' => $this->category_id));
                foreach ($rsCate as $k) {
                    $sqlCate .= 'a.service_id = ' . $k->id . ' || ';
                }
                $sqlFull = substr($sqlCate, 0, -3);
                if ($sqlFull != '') {
                    $criteria->condition = $sqlFull;
                }
            } else {
                for ($i = 0; $i < count($ids[1]); $i++) {
                    if ($i == 0)
                        $criteria->compare('service_id', $ids[1][$i]);
                    else
                        $criteria->addCondition('service_id=' . $ids[1][$i], 'OR');
                }
            }
        }
       
        $criteria->compare('new_register', $this->new_register);
        $criteria->compare('cancel_by_user', $this->cancel_by_user);
        $criteria->compare('cancel_by_system', $this->cancel_by_system);
        $criteria->compare('real_development', $this->real_development);
        $criteria->compare('current_user', $this->current_user);
        $criteria->compare('chargable_subs', $this->chargable_subs);
        $criteria->compare('monthly_fee_revenue', $this->monthly_fee_revenue);
        if ($this->time_start)
            $criteria->compare('time', '>=' . $this->time_start);
        if ($this->time_end)
            $criteria->compare('time', '<' . $this->time_end);
        $criteria->order = 'id DESC';
	
	if($export){
            $arCrite = $criteria->toArray();
            $sql = " SELECT {$arCrite['select']} FROM ". $this->tableName() ." AS {$arCrite['alias']} {$arCrite['join']} WHERE {$arCrite['condition']} ORDER BY {$arCrite['order']}";
            if(isset($export))
                $status = 1;
                else $status = 0;
            $query = "insert into command_query (content,created_datetime,filename,status) values (:content,:created,:filename,:status)";
            $parameters = array(":content"=>$sql,":created" => date('Y-m-d H:i:s'),":filename"=>  $this->filename,":status"=>$status);
            Yii::app()->db->createCommand($query)->execute($parameters);
        }
        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array('pageSize' => '25'),
                ));
    }

}