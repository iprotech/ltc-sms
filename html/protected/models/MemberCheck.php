<?php

/**
 * This is the model class for table "member_check".
 *
 * The followings are the available columns in table 'member_check':
 * @property integer $id
 * @property string $msisdn
 * @property integer $service_id
 * @property string $register_date
 * @property string $start_free_date
 * @property string $end_free_date
 * @property string $start_charging_date
 * @property string $end_charging_date
 * @property integer $status
 * @property integer $total_try_charging
 * @property integer $total_money
 * @property string $cancel_date
 * @property integer $convert_status
 * @property integer $charging_status
 * @property integer $register_type
 */
class MemberCheck extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return MemberCheck the static model class
     */
    public $register_start;
    public $register_end;
    public $cancel_start;
    public $cancel_end;
    public $charging_start;
    public $charging_end;
    public $endcharge_start;
    public $endcharge_end;
    public $category_id;
    public $filename = 'Report_Member.csv';
    public $shortcode = '';
    
    public function getStatus() {
        switch ($this->status) {
            case 1:
                return 'Active';
            case 2:
                return 'Suspend';
            case 3:
                return 'Cancel';
            case 4:
                return 'Cancel By System';
            case 5:
                return 'Cancel by admin';
            default:
                return 'Active';
        }
    }
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'member_check';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('msisdn', 'required'),
            array('msisdn', 'length', 'max' => 50),
            array('register_date, start_free_date, end_free_date, start_charging_date, end_charging_date, cancel_date', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, msisdn', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'msisdn' => 'Msisdn',
            'service_id' => 'Service',
            'register_date' => 'Register Date',
            'start_free_date' => 'Start Free Date',
            'end_free_date' => 'End Free Date',
            'start_charging_date' => 'Start Charging Date',
            'end_charging_date' => 'End Charging Date',
            'status' => 'Status',
            'total_try_charging' => 'Total Try Charging',
            'total_money' => 'Total Money',
            'cancel_date' => 'Cancel Date',
            'convert_status' => 'Convert Status',
            'charging_status' => 'Charging Status',
            'register_type' => 'Register Type',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    
    /*
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('msisdn', $this->msisdn, true);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }
     * 
     */
    public $serviceName;
    public function search() {

        $criteria = new CDbCriteria;
        $criteria->alias = 'a';
        $criteria->select = 'a.*, a.service_id as serviceName';

        $criteria->compare('a.id', $this->id);
        $criteria->compare('a.msisdn', $this->msisdn, true);
        $criteria->order = 'id DESC';

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array('pageSize'=>25)
                ));
    }

}