<?php

/**
 * This is the model class for table "content".
 *
 * The followings are the available columns in table 'content':
 * @property integer $id
 * @property integer $service_id
 * @property string $content
 * @property string $created_datetime
 * @property integer $creater_id
 * @property integer $status
 * @property integer $min_id
 * @property integer $max_id
 * @property integer $current_id
 * @property integer $send_datetime
 */
class Content extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Content the static model class
     */
    public $creater;
    public $time_start;
    public $time_end;
    public $serviceName;
    public $fileContent;
    public $category_id;
    public $send_status;

    public function getStatus() {
        switch ($this->send_status) {
            case 1:
                switch ($this->status) {
                    case 1:
                        return 'Processing';
                        break;
                    case 2:
                        return 'Completed';
                        break;
                    default:
                        return 'Pending';
                        break;
                }
                break;
            case 2:
                return 'Stop';
                break;
            case 3:
                return 'Deleted';
                break;
            default:
                return 'Pending';
                break;
        }
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'content';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('service_id', 'required'),
            array('service_id, creater_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('service_id, category_id, content, time_start, time_end, creater_id, created_datetime, status, min_id, max_id, current_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'service_id' => 'Service',
            'content' => 'Content',
            'created_datetime' => 'Created date',
            'creater_id' => 'Creater',
            'status' => 'Status',
            'min_id' => 'Min',
            'max_id' => 'Max',
            'current_id' => 'Current',
            'time_start' => 'Start date',
            'time_end' => 'End date',
            'send_datetime' => 'Send Date Time',
            'fileContent' => 'Upload file Content',
            'category_id' => 'Category'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public $shortcode;

    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $service_ids = User::model()->findByPk(Yii::app()->user->id)->service;
        preg_match_all('/\[(.*?)\]/', $service_ids, $ids);

        $criteria = new CDbCriteria;
        $criteria->alias = 'a';
        $criteria->select = 'a.send_status, a.process_status,a.id, c.shortcode, a.service_id,a.content,a.send_datetime,a.created_datetime,a.creater_id,a.status,a.min_id,a.max_id,a.current_id,b.username as creater,c.service as serviceName';
        $criteria->join = 'INNER JOIN account as b ON b.id=a.creater_id INNER JOIN service as c ON c.id=a.service_id';


        if ($this->service_id) {
            $criteria->compare('service_id', $this->service_id);
        } else {
            if ($this->category_id) {
                $sqlCate = '';
                $rsCate = Service::model()->findAllByAttributes(array('category_id' => $this->category_id));
                foreach ($rsCate as $k) {
                    $sqlCate .= 'a.service_id = ' . $k->id . ' || ';
                }
                $sqlFull = substr($sqlCate, 0, -3);
                if ($sqlFull != '') {
                    $criteria->condition = $sqlFull;
                }
            } else {
                for ($i = 0; $i < count($ids[1]); $i++) {
                    if ($i == 0)
                        $criteria->compare('service_id', $ids[1][$i]);
                    else
                        $criteria->addCondition('service_id=' . $ids[1][$i], 'OR');
                }
            }
        }

        $criteria->compare('id', $this->id);
        $criteria->compare('content', $this->content, true);
        $criteria->compare('creater_id', $this->creater_id);
        $criteria->compare('status', $this->status);
        if ($this->time_start)
            $criteria->compare('created_datetime', '>' . $this->time_start);
        if ($this->time_end)
            $criteria->compare('created_datetime', '<' . $this->time_end);
        $criteria->order = 'id DESC';

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array('pageSize' => '20'),
                ));
    }

}