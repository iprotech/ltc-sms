<?php

/**
 * This is the model class for table "account".
 *
 * The followings are the available columns in table 'account':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $fullname
 * @property integer $status
 * @property integer unlimit_sms
 * @property integer $group_id
 * @property integer $user_type
 */
class User extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return User the static model class
     */
    public $moduleSelected;
    private $oldUserName;
    public $oldPassword;
    public $rePassword;
    public $isChangePassword = FALSE;
    public $servicesSelect, $pass_old, $pass_new;
    public $is_CateSelected, $CateSelected;

    public function getGroup() {
        switch ($this->group_id) {
            case 0:
                return 'Read Only';
                break;
            case 1:
                return 'Administrator';
                break;
            case 2:
                return 'Content provider';
                break;
            default:
                break;
        }
    }

    public function getUserType(){
        switch($this->user_type){
            case 1:
                return 'Lao Tel';
                break;
            case 2:
                return 'Partner';
                break;
            default:
                break;
        }
    }

    public function getStatus() {
        switch ($this->status) {
            case 1:
                return 'Active';
                break;
            default:
                return 'Locked';
                break;
        }
    }

    public function getUnlimit() {
        switch ($this->unlimit_sms) {
            case 1:
                return 'Yes';
                break;
            default:
                return 'No';
                break;
        }
    }
    
    public function getCateSelected() {
        $list = array();
        $cmd = Yii::app()->db->createCommand('SELECT * FROM category WHERE status=1');
        $data = $cmd->queryAll();
        foreach ($data as $item) {
            $list[$item['id']] = $item['name'];
        }
        return $list;
    }

    public function getService() {
        $name = '';
        $arService = array();
        preg_match_all('/\[(.*?)\]/', $this->service, $arService);
        foreach ($arService[1] as $item) {
            $rs = Service::model()->findByPk($item);
            if ($rs)
                $name .= $rs->service . ', ';
        }
        $serviceName = substr($name, 0, -2);
        return $serviceName;
    }

    public function getListService() {
        $list = array();
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('id,service')
                ->from('service')
                ->where('1');
        $data = $cmd->queryAll();
        foreach ($data as $item) {
            $list['[' . $item['id'] . ']'] = $item['service'];
        }
        return $list;
    }

    public function afterFind() {
        $this->servicesSelect = explode(',', str_replace('][', '],[', $this->service));
        $this->moduleSelected = explode(',', $this->module);
        $this->oldUserName = $this->username;
        $this->pass_old = $this->password;
        return parent::afterFind();
    }

    public function beforeSave() {
        $this->module = implode(',', $this->moduleSelected);
        if ($this->isNewRecord || $this->isChangePassword)
            $this->password = md5($this->password);
        return parent::beforeSave();
    }

    public function afterSave() {
        $this->password = NULL;
        return parent::afterSave();
    }

    public function getListModule() {
        $moduleList = Yii::app()->params['module'];
        $result = array();
        foreach ($moduleList as $key => $value) {
            $result[$key] = $value['name'];
        }

        return $result;
    }

    public function getModuleListNames() {
        $moduleList = Yii::app()->params['module'];
        $result = array();
        foreach ($this->moduleSelected as $value) {
            $result[] = $moduleList[$value]['name'];
        }
        return implode(', ', $result);
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'account';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('username, password, fullname, status, group_id, user_type', 'required'),
            array('status, group_id, user_type', 'numerical', 'integerOnly' => true),
            array('username, password, fullname', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, username, password, fullname, module, status, group_id', 'safe', 'on' => 'search'),
        );

        if ($this->isNewRecord) {
            $rules[] = array('username', 'unique');
        } else if (!$this->isChangePassword) {
            $rules[] = array('username', 'checkUserName');
        }

        if ($this->isChangePassword) {
            $rules[] = array('rePassword', 'compare', 'compareAttribute' => 'password');
            $rules[] = array('oldPassword', 'checkOldPassword');
            $rules[] = array('oldPassword, rePassword', 'required');
        }
        return $rules;
    }

    public function checkUserName($attribute, $params) {
        if ($this->username != $this->oldUserName) {
            if ($this->findAllByAttributes(array('username' => $this->username)))
                $this->addError($attribute, 'Username "' . $this->username . '" has already been taken.');
        }
    }

    public function checkOldPassword($attribute, $params) {
        if (!$this->findAllByAttributes(array('id' => $this->id, 'password' => md5($this->oldPassword)))) {
            $this->addError($attribute, 'Old Password invalid.');
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'fullname' => 'Fullname',
            'status' => 'Status',
            'unlimit_sms' => 'Unlimit Sms',
            'group_id' => 'Group',
	     'user_type' => 'User Type',
            'is_CateSelected' => 'or By Category'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('fullname', $this->fullname, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('group_id', $this->group_id);
	 $criteria->compare('user_type', $this->user_type);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

}