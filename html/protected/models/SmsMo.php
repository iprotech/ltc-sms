<?php

/**
 * This is the model class for table "sms_mo".
 *
 * The followings are the available columns in table 'sms_mo':
 * @property integer $id
 * @property string $sender
 * @property string $service
 * @property string $command_code
 * @property string $content
 * @property integer $status
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property string $error_description
 * @property string $process_time
 * @property integer $respond_status
 */
class SmsMo extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return SmsMo the static model class
     */
    public $start_date;
    public $end_date;
    public $category_id;
    public $option;
	public $export_id;
    public $filename = 'Report_SMSMO';
    public function getStatus() {
        switch ($this->status) {
            case 1:
                return 'Complete';
            case 0:
                return 'Pending';
            default:
                return 'Error';
        }
    }

    public function getChargingStatus() {
        switch ($this->charging_status) {
            case 0:
                return 'Pending';
                break;
            case 1:
                return 'Successful';
                break;
            case 2:
                return 'Not charge';
            default:
                return 'Error';
        }
    }

    public function afterFind (){
        $this->created_datetime = date("H:i:s d/m/Y",strtotime($this->created_datetime));
        parent::afterFind ();
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'sms_mo';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('status, respond_status', 'numerical', 'integerOnly' => true),
            array('sender, service', 'length', 'max' => 16),
            array('command_code, content, error_description', 'length', 'max' => 255),
            array('created_datetime, updated_datetime, process_time', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, sender, option, category_id, charging_status, start_date, service_id, end_date, content, status, created_datetime, updated_datetime', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'sender' => 'Sender',
            'service' => 'Shortcode',
            'command_code' => 'Command Code',
            'content' => 'Content',
            'status' => 'Status',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'error_description' => 'Error Description',
            'process_time' => 'Process Time',
            'respond_status' => 'Respond Status',
            'category_id' => 'Category',
            'option'=>'Option'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public $serviceName;

    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $export = Yii::app()->request->getParam('export');
        $service_ids = User::model()->findByPk(Yii::app()->user->id)->service;
		$service_gp = User::model()->findByPk(Yii::app()->user->id)->group_id;
        preg_match_all('/\[(.*?)\]/', $service_ids, $ids);
//        $cat = Service::model()->findByPk($this->service_id)->category_id;
        $criteria = new CDbCriteria;
        $criteria->alias = 'a';
        $criteria->select = 'a.*,b.service as serviceName';
        $criteria->join = 'LEFT JOIN service AS b ON a.service_id=b.id';
	
        if ($this->service_id) {
            $criteria->condition = ('a.service_id ='. $this->service_id);
        } else {
            if ($this->category_id) {
                $sqlCate = '';
                $rsCate = Service::model()->findAllByAttributes(array('category_id' => $this->category_id));
                foreach ($rsCate as $k) {
                    $sqlCate .= 'a.service_id = ' . $k->id . ' || ';
                }
                $sqlFull = substr($sqlCate, 0, -3);
                if ($sqlFull != '') {
                    $criteria->condition = $sqlFull;
                }
            } else {
               if($service_gp !=1){
                    $sql    = '';
                    for ($i = 0; $i < count($ids[1]); $i++) {
                        if ($i == 0)
                            $criteria->compare('a.service_id', $ids[1][$i]);
                        else
                            $criteria->addCondition('a.service_id=' . $ids[1][$i], 'OR');

                        $rsService  = Service::model()->findByPk($ids[1][$i]);
                          if(isset($rsService) && $rsService)
                                    $sql    .= 'a.service='.$rsService->shortcode.' || ';
                    }
                    if($sql!='')
                        $sql    = ' && ('.substr($sql,0,-3).')';
                    $criteria->addCondition('a.service_id=0'.$sql, 'OR');
                }            }
        }


        $criteria->compare('id', $this->id);
        if($this->sender) {
            if($this->option==2)
                $criteria->join = 'LEFT JOIN service AS b ON a.service_id=b.id INNER JOIN member_check ON a.sender=member_check.msisdn AND a.sender LIKE "%'.$this->sender.'%"';
            else if($this->option==3)
                $criteria->addCondition('a.sender NOT IN (SELECT member_check.msisdn FROM member_check) AND a.sender LIKE "%'.$this->sender.'%"');
            else
                $criteria->addCondition ('a.sender='. $this->sender);
        } else {
            if($this->option==2)
                $criteria->join = 'LEFT JOIN service AS b ON a.service_id=b.id INNER JOIN member_check ON a.sender=member_check.msisdn';
            else if($this->option==3)
                $criteria->addCondition('a.sender NOT IN (SELECT member_check.msisdn FROM member_check)');
        }
        if($this->content)
            $criteria->addCondition ("a.content ='$this->content'");
        if($this->status){
            $criteria->AddCondition('a.status ='. $this->status);
        }
        if($this->charging_status)
            $criteria->AddCondition('a.charging_status ='. $this->charging_status);
	 if ($this->service) {
		$criteria->addCondition('a.service ='. $this->service);
	 }
	
        if ($this->start_date)
            $criteria->addCondition('a.created_datetime >' ."'".$this->start_date."'");
        if ($this->end_date)
            $criteria->addCondition('a.created_datetime <' ."'". $this->end_date."'");
        $criteria->order = 'ID DESC';
	
	if($export){
           $arCrite = $criteria->toArray();
           $sql = " SELECT {$arCrite['select']} FROM ". $this->tableName() ." AS {$arCrite['alias']} {$arCrite['join']} WHERE {$arCrite['condition']} ORDER BY {$arCrite['order']}";
           if(isset($export))
               $status = 0;
               else $status = 1;
           $query = "insert into command_query (content,created_datetime,filename,status) values (:content,:created,:filename,:status)";
            $parameters = array(":content"=>$sql,":created" => date('Y-m-d H:i:s'),":filename"=>  $this->filename,":status"=>$status);
            $cmd = Yii::app()->db->createCommand($query)->execute($parameters);
            if(isset($cmd)){
                $this->export_id  = Yii::app()->db->getLastInsertID();
            }
	}
        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array('pageSize' => '25'),
                ));
    }

}