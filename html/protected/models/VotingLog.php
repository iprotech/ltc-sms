<?php

/**
 * This is the model class for table "voting_log".
 *
 * The followings are the available columns in table 'voting_log':
 * @property integer $id
 * @property string $msisdn
 * @property string $keyword
 * @property integer $keyword_id
 * @property string $created_datetime
 * @property integer $mo_id
 * @property integer $total_vote
 * @property integer $sub_type
 */
class VotingLog extends CActiveRecord {

    public $time_start, $time_end, $serviceId, $option = 1, $matching, $limit, $is_match, $prize;
    public $memberType = 1;

    public function getListOption() {
        $Option = array(1 => 'Random', 2 => 'Fastest', 3 => 'MaxVote');
        $result = array();
        foreach ($Option as $key => $value) {
            $result[$key] = $value;
        }
        return $result;
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return VotingLog the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'voting_log';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('keyword_id, mo_id', 'numerical', 'integerOnly' => true),
            array('msisdn', 'length', 'max' => 16),
            array('keyword', 'length', 'max' => 256),
            array('created_datetime', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, time_start, is_match, time_end, serviceId, option, matching, limit, msisdn, keyword, keyword_id, created_datetime, mo_id, sub_type, memberType', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'msisdn' => 'Msisdn',
            'keyword' => 'Keyword',
            'keyword_id' => 'Keyword',
            'created_datetime' => 'Created Datetime',
            'mo_id' => 'Mo',
            'option' => 'Reward',
            'matching' => 'Matching Text',
            'limit' => 'Quantity',
            'sub_type' => 'Sub Type',
            'prize' => 'Prize'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public $maxVote;

    public function getCriteria() {
        $criteria = new CDbCriteria;
        $criteria->alias = 'l';
        $criteria->select = 'l.msisdn, l.created_datetime, SUM(l.total_vote) AS total_vote, k.keyword';
        $criteria->join = 'INNER JOIN voting_keyword AS k ON k.id = l.keyword_id';
        $criteria->group = 'l.msisdn';

        $randomNotIn = Random::getAllMsisdnInCurrentRandom();
        if ($randomNotIn && count($randomNotIn) > 0) {
            $criteria->addNotInCondition("l.msisdn", $randomNotIn);
        }

        if ($this->memberType == 1 || $this->memberType == 2) {
            $memberTest = array(0);
            $tempDt = MemberCheck::model()->findAll();
            if ($tempDt) {
                foreach ($tempDt as $tempItem) {
                    $memberTest[] = $tempItem->msisdn;
                }
            }
            if ($this->memberType == 1) {
                $criteria->addNotInCondition("l.msisdn", $memberTest);
            } else if ($this->memberType == 2) {
                $criteria->addInCondition("l.msisdn", $memberTest);
            }
        }

        if ($this->serviceId) {
            $criteria->compare("k.service_id", $this->serviceId);
        }

        if ($this->sub_type) {
            $criteria->compare("l.sub_type", $this->sub_type);
        }

        if ($this->time_start) {
            $criteria->compare('l.created_datetime', '>=' . $this->time_start);
        }
        if ($this->time_end) {
            $criteria->compare('l.created_datetime', '<=' . $this->time_end);
        }

        if ($this->option) {
            switch ($this->option) {
                case 1:
                    $criteria->order = 'rand()';
                    break;
                case 2:
                    $criteria->order = 'l.created_datetime ASC';
                    break;
                case 3:
                    $criteria->order = 'total_vote DESC';
                    break;
                default:
                    $criteria->order = 'rand()';
                    break;
            }
        } else {
            $criteria->order = 'rand()';
        }

        if ($this->is_match && $this->matching) {
            $criteria->compare("k.keyword", $this->matching);
        }

        if (isset($this->limit) && is_numeric($this->limit)) {
            $criteria->limit = $this->limit;
        } else {
            $criteria->limit = 5;
        }

        return $criteria;
    }

    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        return new CActiveDataProvider($this, array(
            'criteria' => $this->getCriteria(),
            'pagination' => false
        ));
    }

    public function getSubTypeList() {
        return array(
            1 => "Prepaid",
            9 => "Postpaid"
        );
    }

}
