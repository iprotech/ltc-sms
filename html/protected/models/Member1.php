<?php

/**
 * This is the model class for table "member".
 *
 * The followings are the available columns in table 'member':
 * @property integer $id
 * @property string $msisdn
 * @property integer $service_id
 * @property string $register_date
 * @property string $start_free_date
 * @property string $end_free_date
 * @property string $start_charging_date
 * @property string $end_charging_date
 * @property integer $status
 * @property integer $total_try_charging
 * @property integer $total_money
 * @property string $cancel_date
 * @property integer $convert_status
 */
class Member extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Member the static model class
     */
    public $charging_status;
    public $serviceName;
    public $register_start;
    public $register_end;
    public $cancel_start;
    public $cancel_end;
    public $charging_start;
    public $charging_end;
    public $endcharge_start;
    public $endcharge_end;
    public $category_id;
    public $option;
    public $list_member, $fileMember;
    public $filename = 'Report_Member';
    public function getSubType() {
        if ($this->sub_type >= 1 && $this->sub_type <= 6) {
            return 'Pre-paid';
        } else if ($this->sub_type == 9) {
            return 'Post-paid';
        } else {
            return 'Unknown';
        }
    }

    public function getStatus() {
        switch ($this->status) {
            case 1:
                return 'Active';
            case 2:
                return 'Suspend';
            case 3:
                return 'Cancel';
            case 4:
                return 'Cancel By System';
            case 5:
                return 'Cancel by admin';
            default:
                return '0';
        }
    }

    public function afterFind() {
        parent::afterFind();
    }

    public function beforeFind() {
        parent::beforeFind();
    }

    public function getListService() {
        $sql = '';
        $service_ids = User::model()->findByPk(Yii::app()->user->id)->service;
        if ($service_ids) {
            preg_match_all('/\[(.*?)\]/', $service_ids, $ids);
            for ($i = 0; $i < count($ids[1]); $i++) {
                $sql .= 'id=' . $ids[1][$i] . ' || ';
            }
            $sqlfull = '(' . substr($sql, 0, -3) . ')';
        } else {
            $sqlfull = '1';
        }

        $list = array();
        $cmd = Yii::app()->db->createCommand('SELECT id,service FROM service WHERE ' . $sqlfull);
        $data = $cmd->queryAll();
        $list[''] = 'Select';
        foreach ($data as $item) {
            $list[$item['id']] = $item['service'];
        }
        return $list;
    }

    public function getShortcode() {
        $sql = '';
        $service_ids = User::model()->findByPk(Yii::app()->user->id)->service;
        if ($service_ids) {
            preg_match_all('/\[(.*?)\]/', $service_ids, $ids);
            for ($i = 0; $i < count($ids[1]); $i++) {
                $sql .= 'id=' . $ids[1][$i] . ' || ';
            }
            $sqlfull = '(' . substr($sql, 0, -3) . ')';
        } else {
            $sqlfull = '1';
        }

        $list = array();
        $cmd = Yii::app()->db->createCommand('SELECT DISTINCT shortcode FROM service WHERE ' . $sqlfull);
        $data = $cmd->queryAll();
        $list[''] = 'Select';
        foreach ($data as $item) {
            $list[$item['shortcode']] = $item['shortcode'];
        }
        return $list;
    }

    public function getMemberStatus() {
        $list = array();
        $cmd = Yii::app()->db->createCommand();
        $cmd->select('id,value')
                ->from('member_status')
                ->where('1');
        $data = $cmd->queryAll();
        foreach ($data as $item) {
            $list[$item['id']] = $item['value'];
        }
        return $list;
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'member';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('msisdn, service_id, register_date, status', 'required'),
            array('service_id, status', 'numerical', 'integerOnly' => true),
            array('msisdn', 'length', 'max' => 50),
	    array('msisdn','match','pattern'=>'/^856(20)[0-9]{7,8}$/'),
            array('list_member', 'file', 'allowEmpty' => true, 'types' => 'txt'),
// The following rule is used by search().
// Please remove those attributes that should not be searched.
            array('id, msisdn, sub_type, category_id, option, endcharge_start, endcharge_end, charging_start, charging_end, service_id, register_start, register_end, cancel_start, cancel_end, register_date, status, cancel_date, convert_status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'msisdn' => 'Phone number',
            'service_id' => 'Service',
            'register_date' => 'Register Date',
            'start_free_date' => 'Start Free Date',
            'end_free_date' => 'End Free Date',
            'start_charging_date' => 'Start Charging Date',
            'end_charging_date' => 'End Charging Date',
            'status' => 'Status',
            'total_try_charging' => 'Total Try Charging',
            'total_money' => 'Total Money',
            'cancel_date' => 'Cancel Date',
            'convert_status' => 'Convert Status',
            'register_start' => 'Start register date',
            'register_end' => 'End register date',
            'cancel_start' => 'Start cancel date',
            'cancel_end' => 'End cancel date',
            'charging_start' => 'Start charging_start',
            'charging_end' => 'End charging_start',
            'endcharge_start' => 'Start charging_end',
            'endcharge_end' => 'End charging_end',
            'category_id' => 'Category',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public $shortcode, $registerDate, $cancelDate;

    public function search($pagination = array('pageSize' => 25)) {
	$export = Yii::app()->request->getParam('export');
        $service_ids = User::model()->findByPk(Yii::app()->user->id)->service;
        preg_match_all('/\[(.*?)\]/', $service_ids, $ids);

        $criteria = new CDbCriteria;
        $criteria->alias = 'a';
        $criteria->select = 'a.sub_type,a.id,b.shortcode,a.msisdn,a.start_charging_date,a.end_charging_date, 
            DATE_FORMAT(a.register_date,"%H:%i:%s %d/%m/%Y") as registerDate,a.service_id,
            DATE_FORMAT(a.cancel_date,"%H:%i:%s %d/%m/%Y") as cancelDate,a.end_free_date,a.status,a.total_money,b.service as serviceName';
        $criteria->join = 'INNER JOIN service as b ON b.id=a.service_id';

        $criteria->compare('a.id', $this->id);

        if ($this->service_id) {
            $criteria->compare('a.service_id', $this->service_id);
        } else {
            if ($this->category_id) {
                $sqlCate = '';
                $rsCate = Service::model()->findAllByAttributes(array('category_id' => $this->category_id));
                foreach ($rsCate as $k) {
                    $sqlCate .= 'a.service_id = ' . $k->id . ' || ';
                }
                $sqlFull = substr($sqlCate, 0, -3);
                if ($sqlFull != '') {
                    $criteria->condition = $sqlFull;
                }
            } else {
                for ($i = 0; $i < count($ids[1]); $i++) {
                    if ($i == 0)
                        $criteria->compare('a.service_id', $ids[1][$i]);
                    else
                        $criteria->addCondition('a.service_id=' . $ids[1][$i], 'OR');
                }
            }
        }

        $criteria->compare('a.status', $this->status);
        $criteria->compare('a.convert_status', $this->convert_status);

        if ($this->register_start)
            $criteria->compare('a.register_date', '>' . $this->register_start);
        if ($this->register_end)
            $criteria->compare('a.register_date', '<' . $this->register_end);

        if ($this->cancel_start)
            $criteria->compare('a.cancel_date', '>' . $this->cancel_start);
        if ($this->cancel_end) {
            $criteria->compare('a.cancel_date', '>' . date('Y-m-d H:i:s', 0));
            $criteria->compare('a.cancel_date', '<' . $this->cancel_end);
        }
        if ($this->charging_start)
            $criteria->compare('a.start_charging_date', '>' . $this->charging_start);
        if ($this->charging_end)
            $criteria->compare('a.start_charging_date', '<' . $this->charging_end);

        if ($this->endcharge_start)
            $criteria->compare('a.end_charging_date', '>' . $this->endcharge_start);
        if ($this->endcharge_end)
            $criteria->compare('a.end_charging_date', '>' . $this->endcharge_end);

        if ($this->sub_type != '') {
            if ($this->sub_type == 3) {
                $criteria->addCondition('(a.sub_type > 6 && a.sub_type < 9) || a.sub_type=0 || a.sub_type>9');
            } else if ($this->sub_type == 1) {
                $criteria->compare('a.sub_type', '>=1');
                $criteria->compare('a.sub_type', '<=6');
            } else if ($this->sub_type == 2) {
                $criteria->compare('a.sub_type', '9');
            }
        }

        if ($this->msisdn) {
            if ($this->option == 2)
                $criteria->join = 'INNER JOIN member_check ON (a.msisdn=member_check.msisdn AND a.msisdn LIKE "%' . $this->msisdn . '%") INNER JOIN service as b ON b.id=a.service_id';
            else if ($this->option == 3) {
                $criteria->addCondition('a.msisdn NOT IN (SELECT member_check.msisdn FROM member_check) AND a.msisdn LIKE "%' . $this->msisdn . '%"');
            } else
                $criteria->compare('a.msisdn', $this->msisdn, true);
        } else {
            if ($this->option == 2)
                $criteria->join = 'INNER JOIN member_check ON a.msisdn=member_check.msisdn INNER JOIN service as b ON b.id=a.service_id';
            else if ($this->option == 3) {
                $criteria->join = 'INNER JOIN service as b ON b.id=a.service_id';
                $criteria->addCondition('a.msisdn NOT IN (SELECT member_check.msisdn FROM member_check)');
            }
        }
        $criteria->order = 'id DESC';
	
	if($export){
           $arCrite = $criteria->toArray();
            $sql = " SELECT {$arCrite['select']} FROM ". $this->tableName() ." AS {$arCrite['alias']} {$arCrite['join']} WHERE {$arCrite['condition']} ORDER BY {$arCrite['order']}";
           if(isset($export))
                $status = 1;
                else $status = 0;
           $query = "insert into command_query (content,created_datetime,filename,status) values (:content,:created,:filename,:status)";
          $parameters = array(":content"=>$sql,":created" => date('Y-m-d H:i:s'),":filename"=>  $this->filename,":status"=>$status);
          Yii::app()->db->createCommand($query)->execute($parameters);
      }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => $pagination
        ));
    }

}
