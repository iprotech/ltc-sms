<?php
    require_once('lib/config.php');
    require_once('lib/functions.php');
    require_once('lib/sbMysqlPDO.class.php');
    try {
        $conn = new sbMysqlPDO($server, $user, $password, $db);
    } catch (Exception $e) {
        file_put_contents("log/connect_log.txt", "\nError when connect to database server|" . date("Y-m-d H:i:s", time()));
        sleep(5);
    }
    $month = date("m",  strtotime(date('Y-m-d')));
    if( $month == 2 && $month == 4 && $month == 6 && $month == 9 && $month == 12 ){
        $date_expire = date('Y-m-d H:m:s',time()-24*3600*30);
        $time =date('Y-m',time()-24*3600*30);
    }else{
        $date_expire = date('Y-m-d H:m:s',time()-24*3600*31);
        $time =date('Y-m',time()-24*3600*31);
    }
    $time =date('Y-m',time()-24*3600*30);
    $now = date("Y-m-d H:m:s",  time());
    
    $sqlSelectService = "SELECT DISTINCT(id) as service_id, service FROM service";
    $service = $conn->doSelect($sqlSelectService);
    
    foreach ($service as $item) {
        
        $sqlPreTime = "SELECT count(*) FROM charging_history WHERE sub_type != 9 and start_charging_date between '2014-03-01 00:00' and '2014-04-01 00:00' and service_id=" . $item['service_id'] ;
        $preTime = $conn->doSelectOne($sqlPreTime);
        
        $sqlPreAmount = "SELECT SUM(total_money) FROM charging_history WHERE sub_type != 9 and start_charging_date between '2014-03-01 00:00' and '2014-04-01 00:00' and service_id=" . $item['service_id'] ;
        $preAmount = $conn->doSelectOne($sqlPreAmount);
        
        $sqlPosTime = "SELECT count(*) FROM charging_history WHERE sub_type = 9 and start_charging_date between '2014-03-01 00:00' and '2014-04-01 00:00' and service_id=" . $item['service_id'] ;
        $posTime = $conn->doSelectOne($sqlPosTime);
        
        $sqlPosAmount = "SELECT SUM(total_money) FROM charging_history WHERE sub_type = 9 and start_charging_date between '2014-03-01 00:00' and '2014-04-01 00:00' and service_id=" . $item['service_id'] ;
        $posAmount = $conn->doSelectOne($sqlPosAmount);
        
        $sqlSub = "SELECT count(*) FROM charging_history WHERE start_charging_date between '2014-03-01 00:00' and '2014-04-01 00:00' and service_id=" . $item['service_id'] ;
        $Sub = $conn->doSelectOne($sqlSub);
        
        $sqlCount = "SELECT sum(total_vote) FROM charging_history WHERE start_charging_date between '2014-03-01 00:00' and '2014-04-01 00:00' and service_id=" . $item['service_id'] ;
        $Count = $conn->doSelectOne($sqlCount);
        
        $sqlInsert = "INSERT INTO report_month(service_id,service_name,pre_time,pre_amount,pos_time,pos_amount,time,sub,count) 
                        VALUES('{$item['service_id']}','{$item['service']}','{$preTime[0]}','{$preAmount[0]}','{$posTime[0]}','{$posAmount[0]}','2014-03','{$Sub[0]}','{$Count[0]}')";
        $conn->doUpdate($sqlInsert);
    }
?>

