<?php

/**
 * This is the model class for table "service".
 *
 * The followings are the available columns in table 'service':
 * @property integer $id
 * @property string $service
 * @property string $shortcode
 * @property string buffet
 * @property integer $price
 * @property integer $sms_second
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $type
 * @property integer $max_vote_time
 * @property integer $max_money
 * @property integer $has_postpad_vote 
 * @property integer $service_content_id 
 */
class Service extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Service the static model class
     */
    public $edit_command, $is_buffet, $buffetSelected,$contentSelected;
    public $voting_content, $keyword,$send_from;
    public $list_vote, $FileVote;
    public $filename = "Export_SERVICE";
    public $export_id;


    public function getListCategory() {
        $list = array();
        $cmd = Yii::app()->db->createCommand('SELECT * FROM category WHERE status=1');
        $data = $cmd->queryAll();
        $list[''] = 'Select';
        foreach ($data as $item) {
            $list[$item['id']] = $item['name'];
        }
        return $list;
    }
    
    public function afterFind() {
        $this->buffetSelected = explode(',', $this->buffet);
        $this->contentSelected = explode(',', $this->service_content_id);
        parent::afterFind();
    }

    public function getListService() {
        $sql = '';
        $service_ids = User::model()->findByPk(Yii::app()->user->id)->service;
		$groupId = User::model()->findByPk(Yii::app()->user->id)->group_id;
	 if ($groupId != 1) {
        if ($service_ids) {
            preg_match_all('/\[(.*?)\]/', $service_ids, $ids);
            for ($i = 0; $i < count($ids[1]); $i++) {
                $sql .= 'id=' . $ids[1][$i] . ' || ';
            }
            $sqlfull = '(' . substr($sql, 0, -3) . ')';
        }
	} else {
            $sqlfull = '1';
        }

        $list = array();
        $cmd = Yii::app()->db->createCommand('SELECT id,service FROM service WHERE ' . $sqlfull);
        $data = $cmd->queryAll();
        $list[''] = 'Select';
        foreach ($data as $item) {
            $list['[' . $item['id'] . ']'] = $item['service'];
        }
        return $list;
    }

    public function getShortcode() {
		$sql = '';
		$service_ids = User::model()->findByPk(Yii::app()->user->id)->service;
		$service_gp = User::model()->findByPk(Yii::app()->user->id)->group_id;
        if($service_gp !=1){
		if ($service_ids) {
			
            preg_match_all('/\[(.*?)\]/', $service_ids, $ids);
            for ($i = 0; $i < count($ids[1]); $i++) {
                $sql .= 's.id=' . $ids[1][$i] . ' || ';
            }
            $sqlfull = '(' . substr($sql, 0, -3) . ')';
			}
        } else {
            $sqlfull = '1';
        }
        $list = array();
        $cmd = Yii::app()->db->createCommand('SELECT sh.shortcode FROM shortcode sh, service s WHERE s.shortcode=sh.shortcode and '.$sqlfull);
        $data = $cmd->queryAll();
        $list[''] = 'Select';
        foreach ($data as $item) {
            $list[$item['shortcode']] = $item['shortcode'];
        }
        return $list;
    }
    

    public function getedit() {
        $str = '<a href="' . Yii::app()->createUrl('/commandCode/admin', array("CommandCode[service_id]" => $this->id)) . '">Edit Command</a>';
        return $str;
    }

    public function getServiceType(){
        switch($this->type){
            case '1':
                return "Daily";
            case "2":
                return "Weekly";
                break;
            case "3":
                return "Monthly";
                break;
            case "4":
                return "MO";
                break;
            case "5":
                return "MT";
                break;
            case "6":
                return "Charge Keyword";
                break;
            case "7":
                return "Vote";
                break;
        }    
    }

    public function beforeFind() {
        parent::beforeFind();
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'service';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('service, shortcode, category_id, price, type, sms_second', 'required'),
            array('price, type, category_id, sms_second', 'numerical', 'integerOnly' => true),
            array('service, shortcode', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, service, category_id, shortcode, price, type', 'safe', 'on' => 'search'),
             array('list_vote', 'file', 'allowEmpty' => true, 'types' => 'txt,csv')
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'service' => 'Service',
            'shortcode' => 'Shortcode',
            'category_id' => 'Category',
            'price' => 'Price',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'type' => 'Type',
            'buffet' => 'Buffet',
	     'sms_second' => 'SMS/1s',
            'service_content_id' => 'Import Content'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public $CateName;
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $export = Yii::app()->request->getParam('export');
        $service_ids = User::model()->findByPk(Yii::app()->user->id)->service;
        $groupId = User::model()->findByPk(Yii::app()->user->id)->group_id;
        preg_match_all('/\[(.*?)\]/', $service_ids, $ids);
        $criteria = new CDbCriteria;
        $criteria->alias = 'a';
        $criteria->select   = 'a.*,b.name as CateName';
        $criteria->join = 'LEFT JOIN category AS b ON a.category_id=b.id';
            if($groupId!=1){
                for ($i = 0; $i < count($ids[1]); $i++) {
                                if ($i == 0)
                        $criteria->condition = ('a.id ='. $ids[1][$i]);
                else
                        $criteria->addCondition('a.id=' . $ids[1][$i], 'OR');
                }
	}
        if($this->service)
        $criteria->addCondition("a.service like '%$this->service%'");
        if($this->shortcode)
        $criteria->addCondition('a.shortcode ='. $this->shortcode);
        if($this->price)
        $criteria->addCondition('a.price ='. $this->price);
        if($this->category_id)
        $criteria->addCondition('a.category_id ='. $this->category_id);
        if($this->type)
        $criteria->addCondition('a.type ='. $this->type);
        $criteria->order = 'id DESC';
        if($export){
            $arCrite = $criteria->toArray();
                
            if(empty($this->service) && empty($this->shortcode) && empty($this->price) && empty($this->category_id) && empty($this->type))
                $arCrite['condition'] = '1=1';
            else
                $arCrite['condition'] = $arCrite['condition'];
            $sql = " SELECT {$arCrite['select']} FROM ". $this->tableName() ." AS {$arCrite['alias']} {$arCrite['join']} WHERE {$arCrite['condition']} ORDER BY {$arCrite['order']}";
            if(isset($export))
                $status = 0;
            else $status = 1;
            $query = "insert into command_query (content,created_datetime,filename,status) values (:content,:created,:filename,:status)";
            $parameters = array(":content"=>$sql,":created" => date('Y-m-d H:i:s'),":filename"=>  $this->filename,":status"=>$status);
            $cmd = Yii::app()->db->createCommand($query)->execute($parameters);
            if(isset($cmd)){
                $this->export_id  = Yii::app()->db->getLastInsertID();
            }
        }
        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

}