<?php
/* @var $this EmailReportController */
/* @var $model EmailReport */

$this->breadcrumbs=array(
	'Email Reports'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Manage EmailReport', 'url'=>array('admin')),
);
?>

<h1>View EmailReport #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'email',
		'service',
		'type',
		'send_time',
	),
)); ?>
