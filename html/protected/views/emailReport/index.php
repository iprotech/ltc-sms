<?php
/* @var $this EmailReportController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Email Reports',
);

$this->menu=array(
	array('label'=>'Create EmailReport', 'url'=>array('create')),
	array('label'=>'Manage EmailReport', 'url'=>array('admin')),
);
?>

<h1>Email Reports</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
