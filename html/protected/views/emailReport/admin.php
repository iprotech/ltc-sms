<?php
/* @var $this EmailReportController */
/* @var $model EmailReport */

$this->breadcrumbs=array(
	'Email Reports'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create EmailReport', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#email-report-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Email Reports</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'email-report-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id',
		'email',
        //'service',
        array('name'=>'service','value'=>'$data->getListService()'),
        //array('name'=>'type','value'=>'$data->getType()'),
		'send_time',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
