<?php
/* @var $this EmailReportController */
/* @var $model EmailReport */
/* @var $form CActiveForm */
?>

<div class="form">

<?php
Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'email-report-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textArea($model,'email',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'serviceList'); ?>
        <?php echo $form->dropDownlist($model,'serviceList',  $model->getService(),array('style'=>'height: 150px!important;padding-top:8px;width: 200px;', 'multiple' => 'multiple')); ?>
		<?php echo $form->error($model,'serviceList'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
        <?php echo $form->dropDownList($model,'type',array(1=>'Daily',2=>'Weekly',3=>'Monthly')); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'send_time'); ?>
        <?php
        $this->widget('CJuiDateTimePicker',array(
            'model'=>$model, //Model object
            'attribute'=>'send_time', //attribute name
            'mode'=>'datetime', //use "time","date" or "datetime" (default)
            'options'=>array("dateFormat"=>'yy-mm-dd'), // jquery plugin options
            'language' => ''
        ));
        ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->