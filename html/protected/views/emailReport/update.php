<?php
/* @var $this EmailReportController */
/* @var $model EmailReport */

$this->breadcrumbs=array(
	'Email Reports'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Manage EmailReport', 'url'=>array('admin')),
);
?>

<h1>Update EmailReport <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>