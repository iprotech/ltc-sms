<?php
/* @var $this EmailReportController */
/* @var $model EmailReport */

$this->breadcrumbs=array(
	'Email Reports'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage EmailReport', 'url'=>array('admin')),
);
?>

<h1>Create EmailReport</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>