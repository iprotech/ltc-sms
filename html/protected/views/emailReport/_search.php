<?php
/* @var $this EmailReportController */
/* @var $model EmailReport */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'email'); ?>
		<?php echo $form->textArea($model,'email',array('rows'=>2, 'cols'=>50)); ?>
	</div>

    <!--<div class="row">
        <?php /*echo $form->labelEx($model,'serviceList'); */?>
        <?php /*echo $form->dropDownlist($model,'serviceList',  $model->getService(),array('style'=>'height: 20px!important;padding-top:8px;width: 200px;')); */?>
    </div>-->

	<div class="row">
		<?php echo $form->label($model,'type'); ?>
        <?php echo $form->dropDownList($model,'type',array(1=>'Daily',2=>'Weekly',3=>'Monthly'),array('empty'=>'Selected')); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->