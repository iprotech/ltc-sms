<?php
/* @var $this CommandCodeController */
/* @var $model CommandCode */

$this->breadcrumbs=array(
	'Command Codes'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage CommandCode', 'url'=>array('admin')),
);
?>

<h1>Create CommandCode</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>