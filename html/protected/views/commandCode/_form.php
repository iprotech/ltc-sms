<?php
/* @var $this CommandCodeController */
/* @var $model CommandCode */
/* @var $form CActiveForm */
?>

<div class="form">

<?php 
Yii::app()->clientScript->registerScript('any_unique_id', "
          $('#CommandCode_cmd_price').hide();
          $( '#CommandCode_type' ).change(function() {
            if($( this ).val() == 6){
                  $('#CommandCode_cmd_price').show();
            }
          });
          ");

$form=$this->beginWidget('CActiveForm', array(
	'id'=>'command-code-form',
	'enableAjaxValidation'=>false,
));

?>
    

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'service_id'); ?>
		<?php echo $form->dropDownList($model,'service_id',Member::model()->getListService()); ?>
		<?php echo $form->error($model,'service_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->dropDownList($model,'type',array(''=>'select',1=>'ON',2=>'OFF',5=>'HELP',6=>'MO'),array('options' => array(''=>array('selected'=>true)))); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>
        
        <div class ="row" id="CommandCode_cmd_price">
                <?php echo $form->labelEx($model,'cmd_price'); ?>
                <?php echo $form->textField($model,'cmd_price',array('size'=>20)); ?>
                <?php echo $form->error($model,'cmd_price'); ?>
        </div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'command_code'); ?> (Any command is available except "H")
		<?php echo $form->textField($model,'command_code',array('size'=>20,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'command_code'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'command_code_alias'); ?>
		<?php echo $form->textField($model,'command_code_alias',array('size'=>20,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'command_code_alias'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'reply_content'); ?>
		<?php echo $form->textArea($model,'reply_content',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'reply_content'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->