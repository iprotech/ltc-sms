<?php
/* @var $this CommandCodeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Command Codes',
);

$this->menu=array(
	array('label'=>'Create CommandCode', 'url'=>array('create')),
	array('label'=>'Manage CommandCode', 'url'=>array('admin')),
);
?>

<h1>Command Codes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
