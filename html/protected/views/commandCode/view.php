<?php
/* @var $this CommandCodeController */
/* @var $model CommandCode */

$this->breadcrumbs=array(
	'Command Codes'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Create CommandCode', 'url'=>array('create')),
	array('label'=>'Update CommandCode', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete CommandCode', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CommandCode', 'url'=>array('admin')),
);
?>

<h1>View CommandCode #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'service_id',
		'command_code',
		'command_code_alias',
		'type',
		'reply_content',
		'description',
		'created_datetime',
	),
)); ?>
