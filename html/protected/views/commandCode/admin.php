<?php
/* @var $this CommandCodeController */
/* @var $model CommandCode */

$this->breadcrumbs=array(
	'Command Codes'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create CommandCode', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('command-code-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Command Codes</h1>

Now listing all Command Code, <?php echo CHtml::link('Filter Options','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'command-code-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id',
                array('name' => 'service_id','value' => '$data->serviceName'),
		'command_code',
		'command_code_alias',
               array('name'=>'type','value'=>'$data->getCommandType()'),
		'reply_content',
		/*
		'description',
		'created_datetime',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
