<?php
/* @var $this CommandCodeController */
/* @var $model CommandCode */

$this->breadcrumbs=array(
	'Command Codes'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Create CommandCode', 'url'=>array('create')),
	array('label'=>'View CommandCode', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage CommandCode', 'url'=>array('admin')),
);
?>

<h1>Update CommandCode <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>