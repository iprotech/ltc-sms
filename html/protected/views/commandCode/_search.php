<?php
/* @var $this CommandCodeController */
/* @var $model CommandCode */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php 
    $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'service_id'); ?>
		<?php echo $form->dropDownList($model,'service_id',Member::model()->getListService()); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'command_code'); ?>
		<?php echo $form->textField($model,'command_code',array('size'=>20,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'command_code_alias'); ?>
		<?php echo $form->textField($model,'command_code_alias',array('size'=>20,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'type'); ?>
		<?php echo $form->dropDownList($model,'type',array(''=>'',1=>'ON',2=>'OFF',5=>'HELP',6=>'MO')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'reply_content'); ?>
		<?php echo $form->textArea($model,'reply_content',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->