<?php
/* @var $this ServiceController */
/* @var $model Service */
//var_dump($keys[0]->keyword);

$this->breadcrumbs=array(
	'Services'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Create Service', 'url'=>array('create')),
	array('label'=>'View Service', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Service', 'url'=>array('admin')),
);
?>

<h1>Update Service <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model,'keys' => $keys)); ?>