<?php
/* @var $this ServiceController */
/* @var $model Service */
/* @var $form CActiveForm */
?>

<div class="form">

<?php
    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $form=$this->beginWidget('CActiveForm', array(
	'id'=>'service-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'service'); ?>
		<?php echo $form->textField($model,'service',array('size'=>20,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'service'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'shortcode'); ?>
		<?php echo $form->dropDownList($model,'shortcode', $model->getShortcode()); ?>
		<?php echo $form->error($model,'shortcode'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'category_id'); ?>
		<?php echo $form->dropDownList($model,'category_id', $model->getListCategory()); ?>
		<?php echo $form->error($model,'category_id'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'send_from'); ?>
		<?php echo $form->textField($model,'send_from'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'price'); ?>
		<?php echo $form->textField($model,'price'); ?>
		<?php echo $form->error($model,'price'); ?>
	</div>
	
	<div class="row">
        <?php echo $form->labelEx($model,'sms_second'); ?>
        <?php echo $form->textField($model,'sms_second',array('size'=>20,'maxlength'=>12)); ?>
        <?php echo $form->error($model,'sms_second'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->dropDownList($model,'type',array(1=>'Daily', 2=>'Weekly',3=>'Monthly',4=>'MO',5=>'MT',6=>'Charge Keyword',7=>'Voting Keyword'),
                        array('id'=>'idtype','onchange'=>'showreply("idtype")')); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>
        
        <div id="voting" <?php if(isset($model->type) && $model->type=='7') { ?> style="display: block" <?php }else{ ?> style="display:none" <?php } ?>>
            <div class="row" style="width:30%; float: left;" id="start_time">
                    <?php echo $form->labelEx($model,'start_time'); ?>
                    <?php 
                        $this->widget('CJuiDateTimePicker',array(
                        'model'=>$model, //Model object
                        'attribute'=>'start_time', //attribute name
                        'mode'=>'datetime', //use "time","date" or "datetime" (default)
                        'options'=>array("dateFormat"=>'yy/mm/dd'), // jquery plugin options
                        'language' => ''
                    )); ?>
                    <?php echo $form->error($model,'start_time'); ?>
            </div>

            <div class="row" style="width:30%; float: left;" id="end_time">
                    <?php echo $form->labelEx($model,'end_time'); ?>
                    <?php 
                        $this->widget('CJuiDateTimePicker',array(
                        'model'=>$model, //Model object
                        'attribute'=>'end_time', //attribute name
                        'mode'=>'datetime', //use "time","date" or "datetime" (default)
                        'options'=>array("dateFormat"=>'yy/mm/dd'), // jquery plugin options
                        'language' => ''
                    )); ?>
                    <?php echo $form->error($model,'end_time'); ?>
            </div>
            <p style="color:red;clear:both; margin:12px 0;">Fill all information voting keyword</p>
            <?php
                if(isset($keys) && $keys) {
                    $i  = 0;
                    for($i;$i<count($keys);$i++) { ?>
            <div class="row" style="width: 23%; float: left;">
                    <label style="width:110px;">Name</label>
                    <input name="voting_name[<?php echo $keys[$i]->id; ?>]" value="<?php echo $keys[$i]->name; ?>" size="10" type="text" />
            </div>
            <div class="row" style="width: 23%; float: left;">
                    <label style="width:110px;">Keyword</label>
                    <input class="countKey" name="voting_key[<?php echo $keys[$i]->id; ?>]" value="<?php echo $keys[$i]->keyword; ?>" size="10" type="text" />
            </div>
            <div class="row" style="width: 54%; float: left;">
                    <label>Reply Content</label>
                    <input name="voting_content[<?php echo $keys[$i]->id; ?>]" value="<?php echo $keys[$i]->reply_content; ?>" size="50" type="text" />
            </div>
            <?php } } ?>
            <input type="hidden" id="maxID" value="<?php if(isset($keys) && $keys) echo $keys[count($keys)-1]->id; else echo 0; ?>" />
            <?php if(!isset($keys)): ?>
            <div class="row" style="width: 23%; float: left;">
                    <label style="width:110px;">Name</label>
                    <input name="voting_name[1]" value="" size="10" type="text" />
            </div>
            <div class="row" style="width: 23%; float: left;">
                    <label style="width:110px;">Keyword</label>
                    <input class="countKey" name="voting_key[1]" value="" size="10" type="text" />
            </div>
            <div class="row" style="width: 54%; float: left;">
                    <label>Reply Content</label>
                    <input name="voting_content[1]" value="" size="50" type="text" />
            </div>
            <div class="row" style="width: 23%; float: left;">
                    <label style="width:110px;">Name</label>
                    <input name="voting_name[2]" value="" size="10" type="text" />
            </div>
            <div class="row" style="width: 23%; float: left;">
                    <label style="width:110px;">Keyword</label>
                    <input class="countKey" name="voting_key[2]" value="" size="10" type="text" />
            </div>
            <div class="row" style="width: 54%; float: left;">
                    <label>Reply Content</label>
                    <input name="voting_content[2]" value="" size="50" type="text" />
            </div>
            <div class="row" style="width: 23%; float: left;">
                    <label style="width:110px;">Name</label>
                    <input name="voting_name[3]" value="" size="10" type="text" />
            </div>
            <div class="row" style="width: 23%; float: left;">
                    <label style="width:110px;">Keyword</label>
                    <input class="countKey" name="voting_key[3]" value="" size="10" type="text" />
            </div>
            <div class="row" style="width: 54%; float: left;">
                    <label>Reply Content</label>
                    <input name="voting_content[3]" value="" size="50" type="text" />
            </div>
            <div class="row" style="width: 23%; float: left;">
                    <label style="width:110px;">Name</label>
                    <input name="voting_name[4]" value="" size="10" type="text" />
            </div>
            <div class="row" style="width: 23%; float: left;">
                    <label style="width:110px;">Keyword</label>
                    <input class="countKey" name="voting_key[4]" value="" size="10" type="text" />
            </div>
            <div class="row" style="width: 54%; float: left;">
                    <label>Reply Content</label>
                    <input name="voting_content[4]" value="" size="50" type="text" />
            </div>
            <div class="row" style="width: 23%; float: left;">
                    <label style="width:110px;">Name</label>
                    <input name="voting_name[5]" value="" size="10" type="text" />
            </div>
            <div class="row" style="width: 23%; float: left;">
                    <label style="width:110px;">Keyword</label>
                    <input class="countKey" name="voting_key[5]" value="" size="10" type="text" />
            </div>
            <div class="row" style="width: 54%; float: left;">
                    <label>Reply Content</label>
                    <input name="voting_content[5]" value="" size="50" type="text" />
            </div>
            <?php endif; ?>
            <div id="add_key"></div>
            <input type="button" id="btn_add" value="ADD KEY" style="color:blue;"/>
        </div>
        
        <div class="row buffet" id="getbuffet" <?php if($model->type==7): ?> style="display: none" <?php endif; ?>>
            <a onclick="javascript:getshow('Is_Buffet');">
            <?php
                if(isset($model->buffet) && $model->buffet)
                    echo $form->checkBox($model, 'is_buffet', array('checked'=>'checked', 'name' => 'Is_Buffet'));
                else
                    echo $form->checkBox($model, 'is_buffet', array('name' => 'Is_Buffet'));
            ?>
            </a> Is Buffet?
        </div>
        
        <div class="row" id="buffet" <?php if(isset($model->buffet) && $model->buffet) { ?> style="display: block" <?php }else{ ?> style="display:none" <?php } ?>>
		<?php echo $form->labelEx($model,'buffet'); ?>
		<?php echo $form->dropDownList($model,'buffetSelected', $model->getListService(), array('multiple'=>'multiple','id'=>'buffet_value')); ?>
		<?php echo $form->error($model,'buffetSelected'); ?>
	</div>
        
        <div class="row" id="reply_content" <?php if($model->reply_content) { ?> style="display:block" <?php }else{ ?> style="display:none;" <?php } ?> >
                <?php echo $form->labelEx($model,'reply_content'); ?>
		<?php echo $form->textArea($model,'reply_content', array('cols'=>40, 'rows'=>5)); ?>
		<?php echo $form->error($model,'reply_content'); ?>
        </div>
	 <div class="row" id="invalid_content" <?php if($model->invalid_content) { ?> style="display:block" <?php }else{ ?> style="display:none;" <?php } ?> >
                <?php echo $form->labelEx($model,'invalid_content'); ?>
		<?php echo $form->textArea($model,'invalid_content', array('cols'=>40, 'rows'=>5)); ?>
		<?php echo $form->error($model,'invalid_content'); ?>
        </div>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
$(document).ready(function(){
  $("#btn_add").click(function(){
      var maxID = document.getElementById('maxID').value;
      var i = document.getElementsByTagName('input');
      i     = document.getElementsByClassName('countKey');
      var e = document.getElementsByTagName('input');
      e     = document.getElementsByClassName('tongthem');
      if(parseInt(maxID) > 0) {
            var j    = parseInt(maxID) + e.length + 1;
            var max   = j + 5;
      } else {
            var j     = i.length + 1;
            var max   = j+5;
      }
      for(j;j<max;j++) {
          $("#add_key").append('<div class="row" style="width: 23%; float: left;"><label style="width:110px;">Name</label><input class="tongthem" name="voting_name[' + j + ']" value="" size="10" type="text" /></div><div class="row" style="width: 23%; float: left;"><label style="width:110px;">Keyword</label><input class="countKey" name="voting_key[' + j + ']" value="" size="10" type="text" /></div><div class="row" style="width: 54%; float: left;"><label>Reply Content</label><input name="voting_content[' + j +']" value="" size="50" type="text" /></div>');
      }
  });
});
</script>
<script>
    function getshow(id) {
        if(document.getElementById(id).checked) {
            document.getElementById('buffet').style.display='block';
        } else {
            document.getElementById('buffet').style.display='none';
            document.getElementById('buffet_value').value   = '';
        }
    }
    
    function showreply(id) {
        var value = document.getElementById(id).value;
        if(value=='4'){
            document.getElementById('reply_content').style.display  = 'block';
            document.getElementById('invalid_content').style.display  = 'none';
            document.getElementById('voting').style.display  = 'none';
        }else if(value=='6'){
            document.getElementById('reply_content').style.display  = 'block';
	    document.getElementById('invalid_content').style.display  = 'none';
            document.getElementById('voting').style.display  = 'none';
	}else if(value=='7') {
            document.getElementById('voting').style.display  = 'block';
            document.getElementById('getbuffet').style.display  = 'none';
        }else{
            document.getElementById('reply_content').style.display  = 'none';
	    document.getElementById('invalid_content').style.display  = 'none';
            document.getElementById('voting').style.display  = 'none';
	}
    }
    
</script>