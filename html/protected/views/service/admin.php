<?php
/* @var $this ServiceController */
/* @var $model Service */

$this->breadcrumbs=array(
	'Services'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Service', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('service-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

Now listing all services, <?php echo CHtml::link('Filter Options','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'service-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id',
		'service',
                'send_from',
		'shortcode',
                array('name'=>'category_id','value'=>'$data->CateName'),
		'price',
                array('name' => 'type', 'value' => '$data->getServiceType()'),
                'created_datetime',
                array(
                    'name'=>'edit_command',
                    'value'=>'$data->getedit()',
                    'type'=>'raw',
                    ),
		'sms_second',
		/*
		'type',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
