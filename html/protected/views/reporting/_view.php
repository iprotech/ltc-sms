<?php
/* @var $this ReportingController */
/* @var $data Reporting */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('time')); ?>:</b>
	<?php echo CHtml::encode($data->time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('service_id')); ?>:</b>
	<?php echo CHtml::encode($data->service_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('new_register')); ?>:</b>
	<?php echo CHtml::encode($data->new_register); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cancel_by_user')); ?>:</b>
	<?php echo CHtml::encode($data->cancel_by_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cancel_by_system')); ?>:</b>
	<?php echo CHtml::encode($data->cancel_by_system); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('real_development')); ?>:</b>
	<?php echo CHtml::encode($data->real_development); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('current_user')); ?>:</b>
	<?php echo CHtml::encode($data->current_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('chargable_subs')); ?>:</b>
	<?php echo CHtml::encode($data->chargable_subs); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('monthly_fee_revenue')); ?>:</b>
	<?php echo CHtml::encode($data->monthly_fee_revenue); ?>
	<br />

	*/ ?>

</div>