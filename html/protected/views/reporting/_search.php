<?php
/* @var $this ReportingController */
/* @var $model Reporting */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php
    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>

    <div class="row" style="width:48%;float:left">
        <?php echo $form->label($model, 'service_id'); ?>
        <?php echo $form->dropDownList($model, 'service_id', Member::model()->getListService(), array('style' => 'width:140px;')); ?>
    </div>

    <div class="row" style="width:50%;float:left">
        <?php echo $form->label($model, 'category_id', array('style' => 'width:100px;')); ?>
        <?php echo $form->dropDownlist($model, 'category_id', Service::model()->getListCategory(), array('style' => 'width:140px;')); ?>
    </div>

    <div class="row" style="width:48%;float:left">
        <?php echo $form->label($model, 'time_start'); ?>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model, //Model object
            'attribute' => 'time_start', //attribute name
            'mode' => 'datetime', //use "time","date" or "datetime" (default)
            'options' => array("dateFormat" => 'yy-mm-dd'), // jquery plugin options
            'language' => ''
        ));
        ?>
    </div>

    <div class="row" style="width:50%;float:left;">
        <?php echo $form->label($model, 'time_end', array('style' => 'width:100px;')); ?>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model, //Model object
            'attribute' => 'time_end', //attribute name
            'mode' => 'datetime', //use "time","date" or "datetime" (default)
            'options' => array("dateFormat" => 'yy-mm-dd'), // jquery plugin options
            'language' => ''
        ));
        ?>
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
        <?php echo CHtml::button('Export', array('url' => Yii::app()->createUrl(''), 'id' => 'btn-export')); ?>
    </div>
    <div class="row" style="width: 48%;float: left">
         <?php echo $form->label($model, 'month'); ?>
        <?php echo $form->dropDownList($model, 'month', array(''=>'Select', '01'=>'1','02'=>'2','03'=>'3','04'=>'4','05'=>'5','06'=>'6','07'=>'7','08'=>'8','09'=>'9',10=>'10',11=>'11',12=>'12')); ?>
    </div>
    
    <div class="row" style="width: 50%;float: left">
         <?php echo $form->label($model, 'year',array('style'=>'width:100px')); ?>
        <?php echo $form->dropDownList($model, 'year', array(''=>'Select',2013=>'2013',2014=>'2014',2015=>'2015',2016=>'2016',2017=>'2017',2018=>'2018',2019=>'2019',2020=>'2020')); ?>
    </div>
    
    <div class="row buttons">
        <?php echo CHtml::button('Report', array('url' => Yii::app()->createUrl(''), 'id' => 'btn-report')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->
<script type="text/javascript">
    (function($, document, undefined) {
        
        $("#btn-report").on("click", function(e) {
            e.preventDefault();
            var month = $("#Reporting_month").val();
            var year = $("#Reporting_year").val();
            window.open("<?php echo $this->createUrl("excel"); ?>&month=" + month + "&year=" + year);
            //window.location.href = "<?php echo $this->createUrl("excel"); ?>&month=" + month + "&year=" + year;
        });
    })(jQuery, document);
</script>