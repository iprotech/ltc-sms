<?php
/* @var $this ReportingController */
/* @var $model Reporting */

$this->breadcrumbs=array(
	'Reportings'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Reporting', 'url'=>array('admin')),
);
?>

<h1>Create Reporting</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>