<?php
/* @var $this ReportingController */
/* @var $model Reporting */

$this->breadcrumbs=array(
	'Reportings'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Reporting', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('reporting-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'reporting-grid',
	'dataProvider'=>$model->search(),
	'ajaxUpdate' => false,
	'columns'=>array(
		'id',
		'time',
//		'service_id',
              array('name'=>'service_id','value'=>'$data->serviceName'),
              array('name' => 'Register', 'value' => '$data->new_register'),
              array('name' => 'Cancel', 'value' => '$data->cancel_by_user'),
              array('name' => 'System Cancel', 'value' => '$data->cancel_by_system'),
              array('name' => 'Development', 'value' => '$data->real_development'),
              array('name' => 'Member No', 'value' => '$data->current_user'),
              array('name' => 'Chargable No', 'value' => '$data->chargable_subs'),
              array('name' => 'Revenue', 'value' => '$data->monthly_fee_revenue'),
	),
)); ?>
