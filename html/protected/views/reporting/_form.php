<?php
/* @var $this ReportingController */
/* @var $model Reporting */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'reporting-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'time'); ?>
		<?php echo $form->textField($model,'time'); ?>
		<?php echo $form->error($model,'time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'service_id'); ?>
		<?php echo $form->textField($model,'service_id'); ?>
		<?php echo $form->error($model,'service_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'new_register'); ?>
		<?php echo $form->textField($model,'new_register'); ?>
		<?php echo $form->error($model,'new_register'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cancel_by_user'); ?>
		<?php echo $form->textField($model,'cancel_by_user'); ?>
		<?php echo $form->error($model,'cancel_by_user'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cancel_by_system'); ?>
		<?php echo $form->textField($model,'cancel_by_system'); ?>
		<?php echo $form->error($model,'cancel_by_system'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'real_development'); ?>
		<?php echo $form->textField($model,'real_development'); ?>
		<?php echo $form->error($model,'real_development'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'current_user'); ?>
		<?php echo $form->textField($model,'current_user'); ?>
		<?php echo $form->error($model,'current_user'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'chargable_subs'); ?>
		<?php echo $form->textField($model,'chargable_subs'); ?>
		<?php echo $form->error($model,'chargable_subs'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'monthly_fee_revenue'); ?>
		<?php echo $form->textField($model,'monthly_fee_revenue'); ?>
		<?php echo $form->error($model,'monthly_fee_revenue'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->