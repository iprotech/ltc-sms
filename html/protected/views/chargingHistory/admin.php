<?php
/* @var $this ChargingHistoryController */
/* @var $model chargingHistory */

$this->breadcrumbs = array(
    'Charging Histories' => array('admin'),
    'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('charging-history-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php // echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php $model->search(); ?>
<div class="thongke">
    <p>
        Total Revenue:
        <span><?php echo $model->totalmoney; ?></span>
    </p>
    <p>
        Total Vote:
        <span><?php echo $model->totalvote; ?></span>
    </p>
</div>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'charging-history-grid',
    'dataProvider' => $model->search(),
    'ajaxUpdate' => false,
    'columns' => array(
        'id',
        'msisdn',
        'start_charging_date',
        'end_charging_date',
        'total_money',
        'shortcode',
        'service_name',
        array('name'=>'sub_type','value'=>'$data->getType()'),
        'keyword',
        'total_vote',
        'old_balance',
        'new_balance',
        //  array('name'=>'service_name','value'=>'$data->sName'),
        /*
          'mo_id',
          'service_name',
          'charging_type',
          'convert_status',
         */
        array(
            'class' => 'CButtonColumn',
            'template' => '{view}'
        ),
    ),
));
?>
