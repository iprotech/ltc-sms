<?php
/* @var $this ChargingHistoryController */
/* @var $model chargingHistory */

$this->breadcrumbs=array(
	'Charging Histories'=>array('admin'),
	$model->id,
);

$this->menu=array(
	//array('label'=>'Update chargingHistory', 'url'=>array('update', 'id'=>$model->id)),
	//array('label'=>'Delete chargingHistory', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage chargingHistory', 'url'=>array('admin')),
);
?>

<h1>View chargingHistory #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'member_id',
		'start_charging_date',
		'end_charging_date',
		'total_money',
		'service_id',
		'mo_id',
		'service_name',
		'charging_type',
		'convert_status',
	),
)); ?>
