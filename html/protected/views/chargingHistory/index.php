<?php
/* @var $this ChargingHistoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Charging Histories',
);

$this->menu=array(
	array('label'=>'Manage chargingHistory', 'url'=>array('admin')),
);
?>

<h1>Charging Histories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
