<?php
/* @var $this ChargingHistoryController */
/* @var $model chargingHistory */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php
    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>

    <div class="row" style="width:25%;float:left">
        <?php echo $form->label($model, 'msisdn', array('style' => 'width:110px;')); ?>
        <?php echo $form->textField($model, 'msisdn'); ?>
    </div>
   
    <div class="row" style="width:25%;float:left;">
        <?php echo $form->label($model, 'service_name', array('style' => 'width:105px;')); ?>
        <?php echo $form->dropDownList($model, 'service_id', Member::model()->getListService(), array('style' => 'width:140px;')); ?>
    </div>

    <div class="row" style="width:25%;float:left">
        <?php echo $form->label($model, 'time_start', array('style' => 'width:110px;')); ?>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model, //Model object
            'attribute' => 'time_start', //attribute name
            'mode' => 'datetime', //use "time","date" or "datetime" (default)
            'options' => array("dateFormat" => 'yy/mm/dd'), // jquery plugin options
            'language' => ''
        ));
        ?>
    </div>

    <div class="row" style="width:25%;float:left;">
        <?php echo $form->label($model, 'time_end', array('style' => 'width:105px;')); ?>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model, //Model object
            'attribute' => 'time_end', //attribute name
            'mode' => 'datetime', //use "time","date" or "datetime" (default)
            'options' => array("dateFormat" => 'yy/mm/dd'), // jquery plugin options
            'language' => ''
        ));
        ?>
    </div>

    <div class="row" style="width:25%;float:left">
        <?php echo $form->label($model, 'category_id', array('style' => 'width:110px;')); ?>
        <?php echo $form->dropDownlist($model, 'category_id', Service::model()->getListCategory()); ?>
    </div>

    <div class="row" style="float:left; width: 25%;">
        <?php echo $form->label($model, 'sub_type', array('style' => 'width:105px;')); ?>
        <?php echo $form->dropDownList($model, 'sub_type', array('' => 'Select', 1 => 'Pre-paid', 2 => 'Post-paid', 3 => 'Unknown')); ?>
    </div>

    <div class="row " style="width:25%;float:left">
        <?php echo $form->label($model, 'option', array('style' => 'width:110px;')); ?>
        <?php echo $form->dropDownList($model, 'option', array(1 => 'Full', 2 => 'Member Test', 3 => 'Only Customer')); ?>
    </div>
	<div class="row" style="width:25%;float:left">
        <?php echo $form->label($model, 'shortcode', array('style' => 'width:110px;')); ?>
        <?php echo $form->dropDownList($model, 'shortcode', Service::model()->getShortcode(), array('style' => 'width:140px;')); ?>
    </div>
    <div class="clear"></div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
        <?php echo CHtml::button('Export', array('url' => Yii::app()->createUrl(''), 'id' => 'btn-export')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->