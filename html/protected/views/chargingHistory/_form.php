<?php
/* @var $this ChargingHistoryController */
/* @var $model chargingHistory */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'charging-history-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'member_id'); ?>
		<?php echo $form->textField($model,'member_id'); ?>
		<?php echo $form->error($model,'member_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'start_charging_date'); ?>
		<?php echo $form->textField($model,'start_charging_date',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'start_charging_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'end_charging_date'); ?>
		<?php echo $form->textField($model,'end_charging_date',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'end_charging_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'total_money'); ?>
		<?php echo $form->textField($model,'total_money'); ?>
		<?php echo $form->error($model,'total_money'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'service_id'); ?>
		<?php echo $form->textField($model,'service_id'); ?>
		<?php echo $form->error($model,'service_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mo_id'); ?>
		<?php echo $form->textField($model,'mo_id'); ?>
		<?php echo $form->error($model,'mo_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'service_name'); ?>
		<?php echo $form->textField($model,'service_name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'service_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'charging_type'); ?>
		<?php echo $form->textField($model,'charging_type'); ?>
		<?php echo $form->error($model,'charging_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'convert_status'); ?>
		<?php echo $form->textField($model,'convert_status'); ?>
		<?php echo $form->error($model,'convert_status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->