<?php
/* @var $this ChargingHistoryController */
/* @var $model chargingHistory */

$this->breadcrumbs=array(
	'Charging Histories'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'View chargingHistory', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage chargingHistory', 'url'=>array('admin')),
);
?>

<h1>Update chargingHistory <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>