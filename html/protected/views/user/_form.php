<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->textField($model,'password',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fullname'); ?>
		<?php echo $form->textField($model,'fullname',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'fullname'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'Module'); ?>
		<?php echo $form->checkBoxList($model, 'moduleSelected', $model->getListModule(), array('template' => '<li>{input}{label}</li>', 'container' => 'ul class="module-list"', 'separator' => '')); ?>
		<?php echo $form->error($model,'module'); ?>
	</div>

	 <div class="clear"></div>
    <div class="row">
        <?php echo $form->labelEx($model,'user_type'); ?>
        <?php echo $form->dropDownlist($model,'user_type',array(1=>'Lao Tel',2=>'Partner'),array('empty'=>'Selected...')); ?>
        <?php echo $form->error($model,'user_type'); ?>
    </div>

        <div class="clear"></div>
        <div class="row">
		<?php echo $form->labelEx($model,'group_id'); ?>
		<?php echo $form->dropDownlist($model,'group_id',array(''=>'Select...',0=>'Read Only',1=>'Administrator',2=>'Content provider'),array('onchange'=>'changeGroup("User_group_id","serSelected")')); ?>
		<?php echo $form->error($model,'group_id'); ?>
	</div>
        
        <div class="row" id="serSelected">
		<?php echo $form->labelEx($model,'servicesSelect'); ?>
                <?php echo $form->checkBoxList($model, 'servicesSelect', $model->getListService(), array('template' => '<li>{input}{label}</li>', 'container' => 'ul class="module-list"', 'separator' => '')); ?>
		<?php echo $form->error($model,'servicesSelect'); ?>
	</div>
        
        <div class="clear"></div>
        <div class="row" style="margin-bottom: 10px;" id="is_CateSelected">
		<?php echo $form->labelEx($model,'is_CateSelected',array('style'=>'color:red; font-weight:bold;')); ?>
		<?php echo $form->checkBox($model,'is_CateSelected',array('value'=>1,'style'=>'margin-top:6px;','onchange'=>'isCateSelected("User_is_CateSelected","cateSelected")')); ?>
		<?php echo $form->error($model,'is_CateSelected'); ?>
	</div>
        
        <div class="row" id="cateSelected" style="display: none;">
		<?php echo $form->labelEx($model,'CateSelected'); ?>
                <?php echo $form->checkBoxList($model, 'CateSelected', User::model()->getCateSelected(), array('template' => '<li>{input}{label}</li>', 'container' => 'ul class="module-list"', 'separator' => '')); ?>
		<?php echo $form->error($model,'CateSelected'); ?>
	</div>
        
        <div class="clear"></div>
        <div class="row" style="margin-bottom: 10px;">
		<?php echo $form->labelEx($model,'unlimit_sms'); ?>
		<?php echo $form->checkBox($model,'unlimit_sms',array('value'=>1,'style'=>'margin-top:6px;','onchange'=>'isFullSms("User_unlimit_sms","write_sms")')); ?>
		<?php echo $form->error($model,'unlimit_sms'); ?>
	</div>
        
        <div id="write_sms">
            <div class="row">
                    <?php echo $form->labelEx($model,'total_sms'); ?>
                    <?php echo $form->textField($model,'total_sms'); ?>
                    <?php echo $form->error($model,'total_sms'); ?>
            </div>

            <div class="row">
                    <?php echo $form->labelEx($model,'remain_sms'); ?>
                    <?php echo $form->textField($model,'remain_sms'); ?>
                    <?php echo $form->error($model,'remain_sms'); ?>
            </div>
        </div>
        
	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',array(1=>'Active',0=>'Locked')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    function isFullSms(idcheck,idshow) {
        var full    = document.getElementById(idcheck);
        var show    = document.getElementById(idshow);
        if(full.checked) {
            show.style.display  = 'none';
        } else {
            show.style.display  = 'block';
        }
    }
    
    function isCateSelected(idcheck,idshow) {
        var full    = document.getElementById(idcheck);
        var show    = document.getElementById(idshow);
        if(full.checked) {
            show.style.display  = 'block';
            document.getElementById('serSelected').style.display    = 'none';
        } else {
            show.style.display  = 'none';
            document.getElementById('serSelected').style.display    = 'block';
        }
    }
    
    function changeGroup(idcheck,idshow) {
        var group   = document.getElementById(idcheck);
        var ser     = document.getElementById(idshow);
        if(group.value == 1) {
            ser.style.display   = 'none';
            document.getElementById("is_CateSelected").style.display = 'none';
        }else{
            ser.style.display   = 'block';
            document.getElementById("is_CateSelected").style.display = 'block';
        }
    }
</script>