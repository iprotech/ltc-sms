<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create User', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('user-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Users</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id',
		'username',
		'fullname',
                array('name'=>'status','value'=>'$data->status = 1 ? "Active" : "Locked"'),
                array('name' => 'service', 'value' => '$data->getService()', 'type' => 'raw'),
                array('name'=>'group_id','value'=>'$data->getGroup()'),
	         array('name'=>'user_type','value'=>'$data->getUserType()'),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
