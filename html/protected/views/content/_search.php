<?php
/* @var $this ContentController */
/* @var $model Content */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php 
    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row" style="width:48%;float:left">
		<?php echo $form->label($model,'service_id'); ?>
		<?php echo $form->dropDownList($model,'service_id',  Member::model()->getListService()); ?>
	</div>

	<div class="row" style="width:50%;float:left;">
		<?php echo $form->label($model,'content',array('style'=>'width:100px;')); ?>
		<?php echo $form->textField($model,'content',array('size'=>20,'maxlength'=>140)); ?>
	</div>
    
        <div class="row" style="width:48%;float:left">
		<?php echo $form->label($model,'time_start'); ?>
		<?php 
                    $this->widget('CJuiDateTimePicker',array(
                    'model'=>$model, //Model object
                    'attribute'=>'time_start', //attribute name
                    'mode'=>'datetime', //use "time","date" or "datetime" (default)
                    'options'=>array("dateFormat"=>'yy/mm/dd'), // jquery plugin options
                    'language' => ''
            )); ?>
	</div>
    
        <div class="row" style="width:50%;float:left;">
		<?php echo $form->label($model,'time_end',array('style'=>'width:100px;')); ?>
		<?php 
                    $this->widget('CJuiDateTimePicker',array(
                    'model'=>$model, //Model object
                    'attribute'=>'time_end', //attribute name
                    'mode'=>'datetime', //use "time","date" or "datetime" (default)
                    'options'=>array("dateFormat"=>'yy/mm/dd'), // jquery plugin options
                    'language' => ''
            )); ?>
	</div>

	<div class="row" style="width:48%;float:left">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',array(''=>'',1=>'Processing',2=>'Complete')); ?>
	</div>
    
        <div class="row" style="width:50%;float:left">
		<?php echo $form->label($model,'category_id',array('style'=>'width:100px;')); ?>
		<?php echo $form->dropDownlist($model,'category_id',  Service::model()->getListCategory(),array('style'=>'width:140px;')); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->