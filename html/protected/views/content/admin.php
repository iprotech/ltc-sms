<?php
/* @var $this ContentController */
/* @var $model Content */

$this->breadcrumbs=array(
	'Contents'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Content', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('content-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

Now listing all contents, <?php echo CHtml::link('Filter Options','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php echo strtotime('1361780303'); ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'content-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
//		'service_id',
                'id',
                array('name' => 'service_id', 'value' => '$data -> serviceName'),
                'shortcode',
		'content',
                'created_datetime',
                array('name' => 'creater_id', 'value' => '$data -> creater'),
                array('name' => 'status', 'value' => '$data->getStatus()'),
                'send_datetime',
		/*
		'min_id',
		'max_id',
		'current_id',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
