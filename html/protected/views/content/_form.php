<?php
/* @var $this ContentController */
/* @var $model Content */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'content-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
    ));
    ?>
    <?php if (User::model()->findByPk(Yii::app()->user->id)->remain_sms > 0 || User::model()->findByPk(Yii::app()->user->id)->unlimit_sms == 1) { ?>

        <p class="note">Fields with <span class="required">*</span> are required.</p>

        <?php echo $form->errorSummary($model); ?>

        <div class="row">
            <?php echo $form->labelEx($model, 'service_id'); ?>
            <?php echo $form->dropDownList($model, 'service_id', Member::model()->getListService()); ?>
            <?php echo $form->error($model, 'service_id'); ?>
        </div>
        <p style="font-weight: bold; color: red; margin-left: 155px; margin-bottom: 6px;">Content: <span id="count">0</span> character</p>
        <div class="row">
            <?php echo $form->labelEx($model, 'content'); ?>
            <?php echo $form->textArea($model, 'content', array('rows' => 6, 'cols' => 50)); ?>
            <?php echo $form->error($model, 'content'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'fileContent'); ?>
            <?php echo $form->fileField($model, 'fileContent'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'send_datetime'); ?>
            <?php
            $this->widget('CJuiDateTimePicker', array(
                'model' => $model, //Model object
                'attribute' => 'send_datetime', //attribute name
                'mode' => 'datetime', //use "time","date" or "datetime" (default)
                'options' => array("dateFormat" => 'yy-mm-dd'), // jquery plugin options
                'language' => ''
            ));
            ?>
        </div>

        <div class="row">
            <label>Send sms status</label>
            <?php echo $form->dropDownList($model, 'send_status', array('' => 'Select...', 1 => 'SEND', 2 => 'STOP', 3 => 'DELETE')); ?>
            <?php echo $form->error($model, 'send_status'); ?>
        </div>
        <div class="row">
            <label>Language</label>
            <?php echo $form->dropDownList($model, 'language_id', array( 1 => 'English', 2 => 'Laos', 3 => 'Thailand')); ?>
            <?php echo $form->error($model, 'language_id'); ?>
        </div>
        <div class="row buttons">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
        </div>
    <?php } else { ?>
        <p style="font-weight: bold; color: red;">Your sms budget is over, please contact to administrator to increase that. Thanks.</p>
    <?php } ?>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript">
    (function($, document) {
        $(document).ready(function() {
            $("#Content_content").keyup(function(e) {
                var input = $(this).val();
                $("#count").html(input.length);
            });
        });
    })(jQuery, document);
</script>