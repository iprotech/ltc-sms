<?php
/* @var $this MemberController */
/* @var $model Member */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php 
    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
        'enableAjaxValidation' => FALSE,
	'method'=>'get',
)); ?>

	<div class="row" style="width:25%;float:left">
		<?php echo $form->label($model,'Phone Number',array('style'=>'width:110px;')); ?>
		<?php echo $form->textField($model,'msisdn',array('size'=>20,'maxlength'=>50)); ?>
	</div>
        
        
    
        <div class="row" style="width:25%;float:left">
		<?php echo $form->label($model,'category_id',array('style'=>'width:105px;')); ?>
		<?php echo $form->dropDownlist($model,'category_id',  Service::model()->getListCategory()); ?>
	</div>

	<div class="row" style="width:25%;float:left;">
		<?php echo $form->label($model,'service_id',array('style'=>'width:110px;')); ?>
		<?php echo $form->dropDownList($model,'service_id',$model->getListService(),array('style'=>'width:140px;')); ?>
	</div>
    
        <div class="row" style="width:25%;float:left;">
		<?php echo $form->label($model,'status',array('style'=>'width:105px;')); ?>
		<?php echo $form->dropDownList($model,'status',array('' => 'Select',1=>'Active',2=>'Suspend',3=>'Cancel',4=>'Cancel By System',5=>'Cancel by admin')); ?>
	</div>
    
        <div class="row" style="width:25%;float:left">
		<?php echo $form->label($model,'register_start',array('style'=>'width:110px;')); ?>
		<?php 
                    $this->widget('CJuiDateTimePicker',array(
                    'model'=>$model, //Model object
                    'attribute'=>'register_start', //attribute name
                    'mode'=>'datetime', //use "time","date" or "datetime" (default)
                    'options'=>array("dateFormat"=>'yy/mm/dd'), // jquery plugin options
                    'language' => ''
            )); ?>
	</div>
    
        <div class="row" style="width:25%;float:left;">
		<?php echo $form->label($model,'register_end',array('style'=>'width:105px;')); ?>
		<?php 
                    $this->widget('CJuiDateTimePicker',array(
                    'model'=>$model, //Model object
                    'attribute'=>'register_end', //attribute name
                    'mode'=>'datetime', //use "time","date" or "datetime" (default)
                    'options'=>array("dateFormat"=>'yy/mm/dd'), // jquery plugin options
                    'language' => ''
            )); ?>
	</div>
    
        <div class="row" style="width:25%;float:left">
		<?php echo $form->label($model,'cancel_start',array('style'=>'width:110px;')); ?>
		<?php 
                    $this->widget('CJuiDateTimePicker',array(
                    'model'=>$model, //Model object
                    'attribute'=>'cancel_start', //attribute name
                    'mode'=>'datetime', //use "time","date" or "datetime" (default)
                    'options'=>array("dateFormat"=>'yy/mm/dd'), // jquery plugin options
                    'language' => ''
            )); ?>
	</div>
    
        <div class="row" style="width:25%;float:left;">
		<?php echo $form->label($model,'cancel_end',array('style'=>'width:105px;')); ?>
		<?php 
                    $this->widget('CJuiDateTimePicker',array(
                    'model'=>$model, //Model object
                    'attribute'=>'cancel_end', //attribute name
                    'mode'=>'datetime', //use "time","date" or "datetime" (default)
                    'options'=>array("dateFormat"=>'yy/mm/dd'), // jquery plugin options
                    'language' => ''
            )); ?>
	</div>
    
        <div class="row" style="width:25%;float:left">
		<?php echo $form->label($model,'charging_start',array('style'=>'width:110px;')); ?>
		<?php 
                    $this->widget('CJuiDateTimePicker',array(
                    'model'=>$model, //Model object
                    'attribute'=>'charging_start', //attribute name
                    'mode'=>'datetime', //use "time","date" or "datetime" (default)
                    'options'=>array("dateFormat"=>'yy/mm/dd'), // jquery plugin options
                    'language' => ''
            )); ?>
	</div>
    
        <div class="row" style="width:25%;float:left;">
		<?php echo $form->label($model,'charging_end',array('style'=>'width:105px;')); ?>
		<?php 
                    $this->widget('CJuiDateTimePicker',array(
                    'model'=>$model, //Model object
                    'attribute'=>'charging_end', //attribute name
                    'mode'=>'datetime', //use "time","date" or "datetime" (default)
                    'options'=>array("dateFormat"=>'yy/mm/dd'), // jquery plugin options
                    'language' => ''
            )); ?>
	</div>
    
        <div class="row" style="width:25%;float:left">
		<?php echo $form->label($model,'endcharge_start',array('style'=>'width:110px;')); ?>
		<?php 
                    $this->widget('CJuiDateTimePicker',array(
                    'model'=>$model, //Model object
                    'attribute'=>'endcharge_start', //attribute name
                    'mode'=>'datetime', //use "time","date" or "datetime" (default)
                    'options'=>array("dateFormat"=>'yy/mm/dd'), // jquery plugin options
                    'language' => ''
            )); ?>
	</div>
    
        <div class="row" style="width:25%;float:left;">
		<?php echo $form->label($model,'endcharge_end',array('style'=>'width:105px;')); ?>
		<?php 
                    $this->widget('CJuiDateTimePicker',array(
                    'model'=>$model, //Model object
                    'attribute'=>'endcharge_end', //attribute name
                    'mode'=>'datetime', //use "time","date" or "datetime" (default)
                    'options'=>array("dateFormat"=>'yy/mm/dd'), // jquery plugin options
                    'language' => ''
            )); ?>
	</div>
    
        <div class="row" style="float:left; width: 25%;">
		<?php echo $form->label($model,'sub_type',array('style'=>'width:105px;')); ?>
		<?php echo $form->dropDownList($model,'sub_type',array(''=>'Select',1=>'Pre-paid',2=>'Post-paid',3=>'Unknown')); ?>
	</div>
        <div class="row" style="width:25%;float:left">
        <?php echo $form->label($model, 'shortcode', array('style' => 'width:110px;')); ?>
        <?php echo $form->dropDownList($model, 'shortcode', Service::model()->getShortcode(), array('style' => 'width:140px;')); ?>
    	</div>
        <div class="row" style="float: left; width:25%">
		<?php echo $form->label($model,'option',array('style'=>'width:105px;')); ?>
		<?php echo $form->dropDownList($model,'option',array(''=>'Select',1=>'Full',2=>'Member Test',3=>'Only Customer')); ?>
	</div>
    	<div class="clear"></div>
	<div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>&nbsp;&nbsp;&nbsp;&nbsp;
        <?php echo CHtml::button('Export', array('url' => Yii::app()->createUrl(''), 'id' => 'btn-export')); ?>
        <?php echo CHtml::link('Create Member', array('member/create'),array('class'=>'downfile','style'=>'margin-left:50px;'));?>
        <?php echo CHtml::link('Import Member', array('member/import'),array('class'=>'downfile','style'=>'margin-left:50px;'));?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->