<?php
/* @var $this MemberController */
/* @var $model Member */

$this->breadcrumbs = array(
    'Members' => array('admin'),
    'Manage',
);

$this->menu = array(
    array('label' => 'Create Member', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('member-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
$provider   = $model->search();
?>

<?php // echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<div class="exportcsv">
    <?php
    $totalmember = $provider->getTotalItemCount();
    echo '<span style="padding-left:20px;"><strong>Total member</strong>: '.$totalmember.'</span>';
    ?>
</div>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'member-grid',
    'dataProvider' => $provider,
    'ajaxUpdate'=>false,
    'columns' => array(
        'id',
        'msisdn',
        array('name' => 'service_id', 'value' => '$data->serviceName'),
        'shortcode',
        array('name' => 'register_date', 'value' => '$data->registerDate'),
        array('name' => 'cancel_date', 'value' => '$data->cancelDate'),
        array('name' => 'status', 'value' => '$data->getStatus()'),
        'total_money',
        array('name'=>'sub_type','value'=>'$data->getSubType()'),
        /*
          'start_charging_date',
          'end_charging_date',
          'status',
          'total_try_charging',
          'total_money',
          'cancel_date',
          'convert_status',
         */
        array(
            'class' => 'CButtonColumn',
            'template' => '{view}{update}'
        ),
    ),
));
?>