<?php
/* @var $this MemberController */
/* @var $model Member */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'member-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>
    <p class="note">Phone number is <span class="required">85620xxxxxxxx</span> </p>
    <div class="row">
        <?php echo $form->labelEx($model, 'msisdn'); ?>
        <?php echo $form->textField($model, 'msisdn', array('size' => 20, 'maxlength' => 50)); ?>
        <?php echo $form->error($model, 'msisdn'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'service_id'); ?>
        <?php echo $form->dropDownList($model, 'service_id', $model->getListService()); ?>
        <?php echo $form->error($model, 'service_id'); ?>
    </div>

    <?php if (!$model->isNewRecord): ?>
        <div class="row">
            <?php echo $form->labelEx($model, 'status'); ?>
            <?php echo $form->dropDownList($model, 'status', $model->getMemberStatus()); ?>
            <?php echo $form->error($model, 'status'); ?>
        </div>
    <?php endif; ?>

    <div class="row">
        <label>Charging money</label>
        <?php if ($this->isAdmin()) { ?>
            <?php echo $form->dropDownList($model, 'charging_status', array('' => 'Select...', 0 => 'Charge', 1 => 'Not Charge')); ?>
        <?php } else { ?>
            <span style="line-height: 26px;"><?php echo $model->charging_status == 0 ? "Charge" : "Not Charge"; ?></span>
        <?php } ?>
        <?php echo $form->error($model, 'charging_status'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->