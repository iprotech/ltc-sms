<?php
/* @var $this MemberController */
/* @var $model Member */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'member-form',
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'service_id'); ?>
		<?php echo $form->dropDownList($model,'service_id',$model->getListService()); ?>
		<?php echo $form->error($model,'service_id'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'list_member'); ?>
		<?php echo $form->fileField($model,'list_member'); ?>
		<?php echo $form->error($model,'list_member'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Import'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->