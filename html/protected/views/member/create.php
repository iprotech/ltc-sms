<?php
/* @var $this MemberController */
/* @var $model Member */

$this->breadcrumbs=array(
	'Members'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Member', 'url'=>array('admin')),
);
?>

<h1>Create Member</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>