<?php
/* @var $this MemberController */
/* @var $data Member */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('msisdn')); ?>:</b>
	<?php echo CHtml::encode($data->msisdn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('service_id')); ?>:</b>
	<?php echo CHtml::encode($data->service_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('register_date')); ?>:</b>
	<?php echo CHtml::encode($data->register_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('start_free_date')); ?>:</b>
	<?php echo CHtml::encode($data->start_free_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('end_free_date')); ?>:</b>
	<?php echo CHtml::encode($data->end_free_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('start_charging_date')); ?>:</b>
	<?php echo CHtml::encode($data->start_charging_date); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('end_charging_date')); ?>:</b>
	<?php echo CHtml::encode($data->end_charging_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_try_charging')); ?>:</b>
	<?php echo CHtml::encode($data->total_try_charging); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_money')); ?>:</b>
	<?php echo CHtml::encode($data->total_money); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cancel_date')); ?>:</b>
	<?php echo CHtml::encode($data->cancel_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('convert_status')); ?>:</b>
	<?php echo CHtml::encode($data->convert_status); ?>
	<br />

	*/ ?>

</div>