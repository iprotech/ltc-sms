<?php
/* @var $this MemberCheckController */
/* @var $model MemberCheck */

$this->breadcrumbs=array(
	'Member Checks'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Create MemberCheck', 'url'=>array('create')),
	array('label'=>'View MemberCheck', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MemberCheck', 'url'=>array('admin')),
);
?>

<h1>Update MemberCheck <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>