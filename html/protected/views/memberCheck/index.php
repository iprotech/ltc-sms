<?php
/* @var $this MemberCheckController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Member Checks',
);

$this->menu=array(
	array('label'=>'Create MemberCheck', 'url'=>array('create')),
	array('label'=>'Manage MemberCheck', 'url'=>array('admin')),
);
?>

<h1>Member Checks</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
