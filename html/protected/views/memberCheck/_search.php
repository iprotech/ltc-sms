<?php
/* @var $this MemberCheckController */
/* @var $model MemberCheck */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'msisdn'); ?>
		<?php echo $form->textField($model,'msisdn',array('size'=>20,'maxlength'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
                <?php echo CHtml::link('Delete All & Actions', array('memberCheck/delallaction'),array('class'=>'downfile'));?>
                <?php echo CHtml::link('Delete Actions', array('memberCheck/delaction'),array('class'=>'downfile'));?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->