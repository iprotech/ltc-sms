<?php
/* @var $this MemberCheckController */
/* @var $model MemberCheck */

$this->breadcrumbs=array(
	'Member Checks'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Create MemberCheck', 'url'=>array('create')),
	array('label'=>'Update MemberCheck', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MemberCheck', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MemberCheck', 'url'=>array('admin')),
);
?>

<h1>View MemberCheck #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'msisdn',
	),
)); ?>
