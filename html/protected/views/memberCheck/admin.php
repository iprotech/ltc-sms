<?php
/* @var $this MemberCheckController */
/* @var $model MemberCheck */

$this->breadcrumbs=array(
	'Member Checks'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create MemberCheck', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('member-check-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Member Checks</h1>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'member-check-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id',
		'msisdn',
		/*
		'start_charging_date',
		'end_charging_date',
		'status',
		'total_try_charging',
		'total_money',
		'convert_status',
		'charging_status',
		'register_type',
		*/
		array(
			'class'=>'CButtonColumn',
                        'buttons' => array(
                            'delete' =>
                            array(
                                'label' => 'Delete',
                                'imageUrl' => FALSE,
                                'url' => 'Yii::app()->createUrl("memberCheck/delete", array("id"=>$data->id))',
                            ),
                            'update' =>
                            array(
                                'label' => 'Update',
                                'imageUrl' => FALSE,
                                'url' => 'Yii::app()->createUrl("memberCheck/update", array("id"=>$data->id))',
                            ),
                            'deleteAll' =>
                            array(
                                'label' => 'Delete All',
                                'imageUrl' => FALSE,
                                'url' => 'Yii::app()->createUrl("memberCheck/delAllinOnlyTest", array("id"=>$data->id))',
                            ),
                        ),
                        'template'=>'{deleteAll} | {update} | {delete}',
                        'htmlOptions' => array('style'=>'width:150px;')
		),
	),
)); ?>
