<?php
/* @var $this MemberCheckController */
/* @var $model MemberCheck */

$this->breadcrumbs=array(
	'Member Checks'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage MemberCheck', 'url'=>array('admin')),
);
?>

<h1>Create MemberCheck</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>