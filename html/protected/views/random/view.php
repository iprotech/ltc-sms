<?php
/* @var $this RandomController */
/* @var $model Random */

$this->breadcrumbs=array(
	'Randoms'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Random', 'url'=>array('index')),
	array('label'=>'Create Random', 'url'=>array('create')),
	array('label'=>'Update Random', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Random', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Random', 'url'=>array('admin')),
);
?>

<h1>View Random #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'start_time',
		'end_time',
		'is_current',
		'current_random',
	),
)); ?>
