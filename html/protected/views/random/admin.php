<?php
/* @var $this RandomController */
/* @var $model Random */

$this->breadcrumbs = array(
    'Randoms' => array('index'),
    'History',
);

$this->menu = array(
    array('label' => 'List Random', 'url' => array('admin'))
);
?>

<h1>Manage History</h1>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'random-grid',
    'dataProvider' => $model->search(),
    //'filter' => $model,
    'columns' => array(
        'id',
        'start_time',
        'end_time',
        'is_current',
        'current_random',
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>
