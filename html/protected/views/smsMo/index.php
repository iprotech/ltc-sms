<?php
/* @var $this SmsMoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Sms Mos',
);

$this->menu=array(
	array('label'=>'Create SmsMo', 'url'=>array('create')),
	array('label'=>'Manage SmsMo', 'url'=>array('admin')),
);
?>

<h1>Sms Mos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
