<?php
/* @var $this SmsMoController */
/* @var $data SmsMo */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sender')); ?>:</b>
	<?php echo CHtml::encode($data->sender); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('service')); ?>:</b>
	<?php echo CHtml::encode($data->service); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('command_code')); ?>:</b>
	<?php echo CHtml::encode($data->command_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('content')); ?>:</b>
	<?php echo CHtml::encode($data->content); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_datetime')); ?>:</b>
	<?php echo CHtml::encode($data->created_datetime); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_datetime')); ?>:</b>
	<?php echo CHtml::encode($data->updated_datetime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('error_description')); ?>:</b>
	<?php echo CHtml::encode($data->error_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('process_time')); ?>:</b>
	<?php echo CHtml::encode($data->process_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('respond_status')); ?>:</b>
	<?php echo CHtml::encode($data->respond_status); ?>
	<br />

	*/ ?>

</div>