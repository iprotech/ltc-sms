<?php
/* @var $this SmsMoController */
/* @var $model SmsMo */

$this->breadcrumbs=array(
	'Sms Mos'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage SmsMo', 'url'=>array('admin')),
);
?>

<h1>Create SmsMo</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>