<?php
/* @var $this SmsMoController */
/* @var $model SmsMo */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'sms-mo-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'sender'); ?>
		<?php echo $form->textField($model,'sender',array('size'=>16,'maxlength'=>16)); ?>
		<?php echo $form->error($model,'sender'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'service'); ?>
		<?php echo $form->textField($model,'service',array('size'=>16,'maxlength'=>16)); ?>
		<?php echo $form->error($model,'service'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'command_code'); ?>
		<?php echo $form->textField($model,'command_code',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'command_code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'content'); ?>
		<?php echo $form->textField($model,'content',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'content'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_datetime'); ?>
		<?php echo $form->textField($model,'created_datetime'); ?>
		<?php echo $form->error($model,'created_datetime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_datetime'); ?>
		<?php echo $form->textField($model,'updated_datetime'); ?>
		<?php echo $form->error($model,'updated_datetime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'error_description'); ?>
		<?php echo $form->textField($model,'error_description',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'error_description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'process_time'); ?>
		<?php echo $form->textField($model,'process_time'); ?>
		<?php echo $form->error($model,'process_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'respond_status'); ?>
		<?php echo $form->textField($model,'respond_status'); ?>
		<?php echo $form->error($model,'respond_status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->