<?php
/* @var $this SmsMoController */
/* @var $model SmsMo */

$this->breadcrumbs=array(
	'Sms Mos'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Create SmsMo', 'url'=>array('create')),
	array('label'=>'View SmsMo', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SmsMo', 'url'=>array('admin')),
);
?>

<h1>Update SmsMo <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>