<?php
/* @var $this SmsMoController */
/* @var $model SmsMo */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php
    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>

    <div class="row" style="width:25%;float:left">
        <?php echo $form->label($model, 'sender', array('style' => 'width:110px;')); ?>
        <?php echo $form->textField($model, 'sender', array('size' => 20, 'maxlength' => 16)); ?>
    </div>

    <div class="row" style="width:25%;float:left;">
        <?php echo $form->label($model, 'content', array('style' => 'width:105px;')); ?>
        <?php echo $form->textField($model, 'content', array('size' => 20, 'maxlength' => 16)); ?>
    </div>

    <div class="row" style="width:25%;float:left">
        <?php echo $form->label($model, 'start_date', array('style' => 'width:110px;')); ?>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model, //Model object
            'attribute' => 'start_date', //attribute name
            'mode' => 'datetime', //use "time","date" or "datetime" (default)
            'options' => array("dateFormat" => 'yy/mm/dd'), // jquery plugin options
            'language' => ''
        ));
        ?>
    </div>

    <div class="row" style="width:25%;float:left;">
        <?php echo $form->label($model, 'end_date', array('style' => 'width:105px;')); ?>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model, //Model object
            'attribute' => 'end_date', //attribute name
            'mode' => 'datetime', //use "time","date" or "datetime" (default)
            'options' => array("dateFormat" => 'yy/mm/dd'), // jquery plugin options
            'language' => ''
        ));
        ?>
    </div>

    <div class="row" style="width:25%;float:left">
        <?php echo $form->label($model, 'shortcode', array('style' => 'width:110px;')); ?>
        <?php echo $form->dropDownList($model, 'service', Service::model()->getShortcode(), array('style' => 'width:140px;')); ?>
    </div>

    <div class="row" style="width:25%;float:left;">
        <?php echo $form->label($model, 'service_id', array('style' => 'width:105px;')); ?>
        <?php echo $form->dropDownList($model, 'service_id', Member::model()->getListService()); ?>
    </div>

    <div class="row" style="width:25%;float:left">
        <?php echo $form->label($model, 'charging_status', array('style' => 'width:110px;')); ?>
        <?php echo $form->dropDownList($model, 'charging_status', array('' => 'Select', 0 => 'Pending', 1 => 'Successful'), array('style' => 'width:140px;')); ?>
    </div>

    <div class="row" style="width:25%;float:left;">
        <?php echo $form->label($model, 'status', array('style' => 'width:105px;')); ?>
        <?php echo $form->dropDownList($model, 'status', array('' => 'Select', 0 => 'Pending', 1 => 'Complete', '>1' => 'Error'), array('style' => 'width:140px;')); ?>
    </div>

    <div class="row" style="width:25%;float:left">
        <?php echo $form->label($model, 'category_id', array('style' => 'width:110px;')); ?>
        <?php echo $form->dropDownlist($model, 'category_id', Service::model()->getListCategory(), array('style' => 'width:140px;')); ?>
    </div>

    <div class="row" style="width:75%;float:left">
        <?php echo $form->label($model, 'option', array('style' => 'width:105px;')); ?>
        <?php echo $form->dropDownList($model, 'option', array('' => 'Select', 1 => 'Full', 2 => 'Member Test', 3 => 'Only Customer')); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
        <?php echo CHtml::button('Export', array('url' => Yii::app()->createUrl(''), 'id' => 'btn-export')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->