<?php
/* @var $this SmsMoController */
/* @var $model SmsMo */

$this->breadcrumbs=array(
	'Sms Mos'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create SmsMo', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('sms-mo-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'sms-mo-grid',
	'dataProvider'=>$model->search(),
       'ajaxUpdate' => false,
	'columns'=>array(
		'id',
		'sender',
                array('name'=>'shortcode','value'=>'$data->service'),
                array('name'=>'serviceName','value'=>'$data->serviceName'),
		//'service',
		'command_code',
		'content',
		'created_datetime',
                array('name'=>'status','value'=>'$data->getStatus()'),
                array('name'=>'charging_status','value'=>'$data->getChargingStatus()'),
                'total_money',
		array(
			'class'=>'CButtonColumn',
                        'template' => '{view}'
		),
	),
)); ?>
