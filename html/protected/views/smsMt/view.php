<?php
/* @var $this SmsMtController */
/* @var $model SmsMt */

$this->breadcrumbs=array(
	'Sms Mts'=>array('admin'),
	$model->id,
);

$this->menu=array(
	//array('label'=>'Create SmsMt', 'url'=>array('create')),
	//array('label'=>'Update SmsMt', 'url'=>array('update', 'id'=>$model->id)),
	//array('label'=>'Delete SmsMt', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SmsMt', 'url'=>array('admin')),
);
?>

<h1>View SmsMt #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'mo_id',
		'sender',
		'service',
                'service_id',
		'receiver',
		'command_code',
		'content_type',
		'status',
		'content',
		'send_datetime',
		'receive_datetime',
		'send_type',
	),
)); ?>
