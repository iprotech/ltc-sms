<?php
/* @var $this SmsMtController */
/* @var $model SmsMt */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'sms-mt-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'mo_id'); ?>
		<?php echo $form->textField($model,'mo_id'); ?>
		<?php echo $form->error($model,'mo_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sender'); ?>
		<?php echo $form->textField($model,'sender',array('size'=>16,'maxlength'=>16)); ?>
		<?php echo $form->error($model,'sender'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'service'); ?>
		<?php echo $form->textField($model,'service',array('size'=>16,'maxlength'=>16)); ?>
		<?php echo $form->error($model,'service'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'receiver'); ?>
		<?php echo $form->textField($model,'receiver',array('size'=>16,'maxlength'=>16)); ?>
		<?php echo $form->error($model,'receiver'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'command_code'); ?>
		<?php echo $form->textField($model,'command_code',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'command_code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'content_type'); ?>
		<?php echo $form->textField($model,'content_type'); ?>
		<?php echo $form->error($model,'content_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'content'); ?>
		<?php echo $form->textField($model,'content',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'content'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'send_datetime'); ?>
		<?php echo $form->textField($model,'send_datetime'); ?>
		<?php echo $form->error($model,'send_datetime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'receive_datetime'); ?>
		<?php echo $form->textField($model,'receive_datetime'); ?>
		<?php echo $form->error($model,'receive_datetime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'send_type'); ?>
		<?php echo $form->textField($model,'send_type'); ?>
		<?php echo $form->error($model,'send_type'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->