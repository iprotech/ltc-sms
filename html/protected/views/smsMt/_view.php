<?php
/* @var $this SmsMtController */
/* @var $data SmsMt */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mo_id')); ?>:</b>
	<?php echo CHtml::encode($data->mo_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sender')); ?>:</b>
	<?php echo CHtml::encode($data->sender); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('service')); ?>:</b>
	<?php echo CHtml::encode($data->service); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('receiver')); ?>:</b>
	<?php echo CHtml::encode($data->receiver); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('command_code')); ?>:</b>
	<?php echo CHtml::encode($data->command_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('content_type')); ?>:</b>
	<?php echo CHtml::encode($data->content_type); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('content')); ?>:</b>
	<?php echo CHtml::encode($data->content); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('send_datetime')); ?>:</b>
	<?php echo CHtml::encode($data->send_datetime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('receive_datetime')); ?>:</b>
	<?php echo CHtml::encode($data->receive_datetime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('send_type')); ?>:</b>
	<?php echo CHtml::encode($data->send_type); ?>
	<br />

	*/ ?>

</div>