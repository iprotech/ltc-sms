<?php
/* @var $this ShortcodeController */
/* @var $model Shortcode */

$this->breadcrumbs=array(
	'Shortcodes'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Shortcode', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('shortcode-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'shortcode-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id',
		'shortcode',
		'help_content',
		'invalid_content',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>