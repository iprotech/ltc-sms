<?php
/* @var $this ShortcodeController */
/* @var $model Shortcode */

$this->breadcrumbs=array(
	'Shortcodes'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Create Shortcode', 'url'=>array('create')),
	array('label'=>'Update Shortcode', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Shortcode', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Shortcode', 'url'=>array('admin')),
);
?>

<h1>View Shortcode #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'shortcode',
		'help_content',
		'invalid_content',
	),
)); ?>
