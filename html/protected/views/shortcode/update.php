<?php
/* @var $this ShortcodeController */
/* @var $model Shortcode */

$this->breadcrumbs=array(
	'Shortcodes'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Create Shortcode', 'url'=>array('create')),
	array('label'=>'View Shortcode', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Shortcode', 'url'=>array('admin')),
);
?>

<h1>Update Shortcode <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>