<?php
/* @var $this ShortcodeController */
/* @var $model Shortcode */

$this->breadcrumbs=array(
	'Shortcodes'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Shortcode', 'url'=>array('admin')),
);
?>

<h1>Create Shortcode</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>