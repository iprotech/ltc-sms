<?php
/* @var $this ShortcodeController */
/* @var $data Shortcode */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shortcode')); ?>:</b>
	<?php echo CHtml::encode($data->shortcode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('help_content')); ?>:</b>
	<?php echo CHtml::encode($data->help_content); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('invalid_content')); ?>:</b>
	<?php echo CHtml::encode($data->invalid_content); ?>
	<br />


</div>