<?php
/* @var $this ShortcodeController */
/* @var $model Shortcode */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'shortcode-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'shortcode'); ?>
		<?php echo $form->textField($model,'shortcode',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'shortcode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'help_content'); ?>
		<?php echo $form->textArea($model,'help_content',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'help_content'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'invalid_content'); ?>
		<?php echo $form->textArea($model,'invalid_content',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'invalid_content'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->