<?php
/* @var $this MonitorController */
?>
<style type="text/css">
    .selected {
        text-decoration: underline;
    }

    .btn-action {
        text-align: center; 
        padding: 10px 0;
    }

    .progess {
        border: 1px solid #999999;
        padding: 10px;
    }

    .progess > ul {
        padding: 0;
        margin: 0;
        list-style: none;
        height: 400px;
        overflow-y: scroll;
        overflow-x: hidden;
    }

    .progess > ul > li {
        padding: 3px 0;
        margin-bottom: 1px;
    }
    
    .progess > ul > li.error {
        background-color: rgba(255, 0, 0, 0.33);
    }
    
    .progess > ul > li.warring {
        background-color: rgba(245, 222, 179, 0.65);
    }
    
    .progess > ul > li.complete {
        background-color: rgba(118, 195, 118, 0.5);
    }

    .progess > ul > li span {
        display: inline-block;
    }

    .progess > .header {
        background-color: #A20000;
        color: #ffffff;
        padding: 10px 0;
        width: 100%;
    }

    .progess > .header span {
        display: inline-block;
    }
</style>
<h1>
    <a class="selected" href="<?php echo $this->createUrl("charging"); ?>">Charging Monitor</a>
    <span>|</span>
    <a href="<?php echo $this->createUrl("charging"); ?>">Sms Monitor</a>
</h1>
<div class="btn-action">
    <button type="button" disabled="disabled">Start</button>
    <button type="button">Stop</button>
</div>
<div class="progess" id="process" url-read="<?php echo $this->createUrl("chargingData"); ?>" time="">
    <div class="header">
        <span style="width: 15%">Time</span>
        <span style="width: 10%">Action</span>
        <span style="width: 10%">Member ID</span>
        <span style="width: 10%">MSISDN</span>
        <span style="width: 10%">Status</span>
        <span style="width: 35%">Info</span>
    </div>
    <ul>
        
    </ul>
</div>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/process.js"></script>