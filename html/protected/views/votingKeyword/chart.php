<style>
    .loader {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('img/page-loader.gif') 50% 50% no-repeat rgb(249,249,249);
    }
</style>
<script type="text/JavaScript">
    setTimeout("location.reload(true);",60000);
    $(window).load(function() {
        $(".loader").fadeOut("slow");
    })
</script>
<?php
$provider = $model->search()->getData();
$maxWidth   = 70*count($provider) + 30;
foreach ($provider as $i) {
    $list_key[] = array(intval($i->total_vote), $i->keyword);
}

$title  = '';
if ($model->service_id)
    $title  =  Service::model()->findByPk($model->service_id)->service;

$this->widget('jqBarGraph', array('values' => $list_key,
    'type' => 'simple',
    'width' => $maxWidth,
    'space' => 5,
    'title' => $title));
?>