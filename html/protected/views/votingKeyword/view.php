<?php
/* @var $this VotingKeywordController */
/* @var $model VotingKeyword */

$this->breadcrumbs=array(
	'Voting Keywords'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List VotingKeyword', 'url'=>array('index')),
	array('label'=>'Create VotingKeyword', 'url'=>array('create')),
	array('label'=>'Update VotingKeyword', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete VotingKeyword', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage VotingKeyword', 'url'=>array('admin')),
);
?>

<h1>View VotingKeyword #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'keyword',
		'reply_content',
		'total_vote',
		'service_id',
	),
)); ?>
