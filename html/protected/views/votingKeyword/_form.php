<?php
/* @var $this VotingKeywordController */
/* @var $model VotingKeyword */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'voting-keyword-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'keyword'); ?>
		<?php echo $form->textField($model,'keyword',array('size'=>60,'maxlength'=>1024)); ?>
		<?php echo $form->error($model,'keyword'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'reply_content'); ?>
		<?php echo $form->textField($model,'reply_content',array('size'=>60,'maxlength'=>1024)); ?>
		<?php echo $form->error($model,'reply_content'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'total_vote'); ?>
		<?php echo $form->textField($model,'total_vote'); ?>
		<?php echo $form->error($model,'total_vote'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'service_id'); ?>
		<?php echo $form->textField($model,'service_id'); ?>
		<?php echo $form->error($model,'service_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->