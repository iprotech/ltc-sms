<?php
/* @var $this VotingKeywordController */
/* @var $model VotingKeyword */

$this->breadcrumbs=array(
	'Voting Keywords'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List VotingKeyword', 'url'=>array('index')),
	array('label'=>'Manage VotingKeyword', 'url'=>array('admin')),
);
?>

<h1>Create VotingKeyword</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>