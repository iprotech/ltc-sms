<?php
/* @var $this VotingKeywordController */
/* @var $model VotingKeyword */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php
    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>

    <div class="row">
        <?php echo $form->label($model, 'service_id'); ?>
        <?php echo $form->dropDownlist($model, 'service_id', VotingKeyword::model()->getService()); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'time_start'); ?>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model, //Model object
            'attribute' => 'time_start', //attribute name
            'mode' => 'datetime', //use "time","date" or "datetime" (default)
            'options' => array("dateFormat" => 'yy-mm-dd'), // jquery plugin options
            'language' => ''
        ));
        ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'time_end'); ?>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model, //Model object
            'attribute' => 'time_end', //attribute name
            'mode' => 'datetime', //use "time","date" or "datetime" (default)
            'options' => array("dateFormat" => 'yy-mm-dd'), // jquery plugin options
            'language' => ''
        ));
        ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
        <a href="javascript:void(0);" onclick="Popup = window.open('http://115.84.105.172/index.php?r=votingKeyword%2Fdraw&VotingKeyword%5Bservice_id%5D=' + document.getElementById('VotingKeyword_service_id').value, 'Popup', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=no, width=400,height=550,left=430,top=23');
                return false;">
            <input type="submit" value="Chart" />
        </a>
        <?php echo CHtml::button('Export', array('url' => Yii::app()->createUrl(''), 'id' => 'btn-export')); ?>
    </div>


    <?php $this->endWidget(); ?>

</div><!-- search-form -->