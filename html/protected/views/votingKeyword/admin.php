<?php
$this->breadcrumbs = array(
    'Voting Keywords' => array('admin'),
    'Chart',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('voting-keyword-grid', {
		data: $(this).serialize()
	});
	return false;
});
");


$provider = $model->search()->getData();
//var_dump($provider);
?>

<div class="search-form">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'voting-keyword-grid',
    'dataProvider' => $model->search(),
    'ajaxUpdate' => FALSE,
    'columns' => array(
        'id',
        'name',
        'keyword',
        'reply_content',
        'total_vote',
        array('name'=>'service_id','value'=>'$data->serName'),
    ),
));
?>
<!--
<div id="chart-content">
    <div class="keyService" style="float:left; width:2px; position: relative; right:72px; font-weight: bold;">
<?php //if($model->service_id) echo Service::model()->findByPk($model->service_id)->service;  ?>
    </div>
<?php //for($i=0;$i<count($provider);$i++) {  ?>
    <div class="key1">
        <div style="height:<?php //echo $provider[$i]->setHeight1;  ?>px;width:30px;"></div>
        <div class="chung mau<?php //echo $i;  ?>" style="height:<?php //echo $provider[$i]->setHeight2; ?>px;vertical-align: top; position: relative;"><span style="position: absolute; top: -16px; left: 0; right: 0; text-align: center;"><?php //echo $provider[$i]->total_vote; ?></span></div>
    </div>
<?php //}  ?>
</div>
<div id="key-content">
<?php //for($i=0;$i<count($provider);$i++) { ?>
    <div class="key1">
<?php //echo $provider[$i]->keyword;  ?>
    </div>
<?php //}  ?>
</div>
-->