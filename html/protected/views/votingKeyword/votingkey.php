<style>
    .loader {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('img/page-loader.gif') 50% 50% no-repeat rgb(249,249,249);
    }
</style>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/JavaScript">
    setTimeout("location.reload(true);",1000000);
    $(window).load(function() {
        $(".loader").fadeOut("slow");
    })
</script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" media="screen, projection" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/switch.css" media="print" />

<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/theme.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/custom.css" />
<?php
$provider = $model->search()->getData();
//var_dump($provider);
$maxWidth   = 70*count($provider) + 30;
?>

<div class="loader"></div>

<div id="chart-content" style="width:<?php echo $maxWidth; ?>px; margin-left:5px;">
    <div class="keyService" style="float:left; width:2px; font-weight: bold; padding-top:30px; font-size: 16px;">
        <?php if ($model->service_id) echo Service::model()->findByPk($model->service_id)->service; ?>
    </div>
    <?php for ($i = 0; $i < count($provider); $i++) { ?>
        <div class="key1">
            <div style="height:<?php echo $provider[$i]->setHeight1; ?>px;width:30px;"></div>
            <div class="chung mau<?php echo $i; ?>" style="height:<?php echo $provider[$i]->setHeight2; ?>px;vertical-align: top; position: relative;">
                <span style="position: absolute; top: -16px; left: 0; right: 0; text-align: center;">
                    <?php echo $provider[$i]->total_vote; ?>
                </span>
                <span style="position: absolute;top:10px;left:0;right:0;text-align: center;">
                    <?php //if($provider[$i]->percent>0) echo round($provider[$i]->percent).'%'; ?>
                </span>
            </div>
        </div>
    <?php } ?>
</div>
<div id="key-content" style="margin-left:5px; width:<?php echo $maxWidth; ?>px;">
    <div style="float:left; width: 10px;height: 5px;"></div>
    <?php for ($i = 0; $i < count($provider); $i++) { ?>
        <div class="key2">
            <?php echo $provider[$i]->keyword; ?>
        </div>
    <?php } ?>
    <div class="clear"></div>
</div>
<div id="name-content" style="margin-left:5px; width:<?php echo $maxWidth; ?>px;">
    <div style="float:left; width: 10px;height: 5px;"></div>
    <?php for ($i = 0; $i < count($provider); $i++) { ?>
        <div class="key3">
            <?php
                $nv     = explode(' ',$provider[$i]->name);
                if(count($nv)>1)
                    echo $nv[0];
                else
                    echo $provider[$i]->name;
            ?>
        </div>
    <?php } ?>
</div>

<div style="clear:both; width:auto; margin:20px 0 20px 160px; font-weight: bold; font-size: 13px;">
    <span>Total <?php echo count($provider); ?></span>
    <span>Total Vote <?php echo $model->getTotal(''); ?></span>
</div>