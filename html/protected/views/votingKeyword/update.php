<?php
/* @var $this VotingKeywordController */
/* @var $model VotingKeyword */

$this->breadcrumbs=array(
	'Voting Keywords'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List VotingKeyword', 'url'=>array('index')),
	array('label'=>'Create VotingKeyword', 'url'=>array('create')),
	array('label'=>'View VotingKeyword', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage VotingKeyword', 'url'=>array('admin')),
);
?>

<h1>Update VotingKeyword <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>