<?php
/* @var $this VotingKeywordController */
/* @var $data VotingKeyword */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keyword')); ?>:</b>
	<?php echo CHtml::encode($data->keyword); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reply_content')); ?>:</b>
	<?php echo CHtml::encode($data->reply_content); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_vote')); ?>:</b>
	<?php echo CHtml::encode($data->total_vote); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('service_id')); ?>:</b>
	<?php echo CHtml::encode($data->service_id); ?>
	<br />


</div>