<?php
/* @var $this VotingKeywordController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Voting Keywords',
);

$this->menu=array(
	array('label'=>'Create VotingKeyword', 'url'=>array('create')),
	array('label'=>'Manage VotingKeyword', 'url'=>array('admin')),
);
?>

<h1>Voting Keywords</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
