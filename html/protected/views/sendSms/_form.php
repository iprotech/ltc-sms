<?php
/* @var $this SmsMtController */
/* @var $model SmsMt */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'send-sms-form',
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

<!--	<div class="row">
		<?php echo $form->labelEx($model,'shortcode'); ?>
		<?php echo $form->dropDownlist($model,'sender', Member::model()->getShortcode(),array('style'=>'width:140px;','onchange'=>'getSender("SendSms_sender","sendby")')); ?>
                <div id="sendby" style="display: inline;">
                    <span style="margin-left: 20px;">Send By</span>
                    <?php echo $form->textField($model,'sendby'); ?>
                </div>
		<?php echo $form->error($model,'shortcode'); ?>
	</div>-->
        
        <div class="row">
		<?php echo $form->labelEx($model,'service_id'); ?>
		<?php echo $form->dropDownlist($model,'service_id', Member::model()->getListService(),array('style'=>'width:140px;')); ?>
		<?php echo $form->error($model,'service_id'); ?>
	</div>
        
        <div class="row">
            <?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker'); ?>
            <?php echo $form->labelEx($model, 'send_datetime'); ?>
            <?php
            $this->widget('CJuiDateTimePicker', array(
                'model' => $model, //Model object
                'attribute' => 'send_datetime', //attribute name
                'mode' => 'datetime', //use "time","date" or "datetime" (default)
                'options' => array("dateFormat" => 'yy-mm-dd'), // jquery plugin options
                'language' => ''
            ));
            ?>
        </div>
        <div class="row">
            <label>Language</label>
            <?php echo $form->dropDownList($model, 'language_id', array( 1 => 'English', 2 => 'Laos', 3 => 'Thailand')); ?>
            <?php echo $form->error($model, 'language_id'); ?>
        </div>
        <div class="row">
		<?php echo $form->labelEx($model,'is_billalert',array('style'=>'color:red; font-weight:bold;')); ?>
		<?php echo $form->checkBox($model,'is_billalert',array('value'=>1,'style'=>'margin-top:6px;','onchange'=>'isBillAlert("SendSms_is_billalert","yeah_billalert","not_billalert")')); ?>
		<?php echo $form->error($model,'is_billalert'); ?>
	</div>
        
        <div class="row" id="yeah_billalert" style="display: none;">
            <p style="font-weight: bold; color: red; margin-left: 150px;"><span style="color: #333;">The format of upload file below:</span><br/>
            20xxxxxxxx|Content>><br/>
            20xxxxxxxx|Content>><br/>
            ..</p>
		<?php echo $form->labelEx($model,'list_billalert'); ?>
		<?php echo $form->fileField($model,'list_billalert'); ?>
		<?php echo $form->error($model,'list_billalert'); ?>
	</div>
        
        <div id="not_billalert">            
            <div class="row">
                    <?php echo $form->labelEx($model,'list_receiver'); ?>
                    <?php echo $form->fileField($model,'list_receiver'); ?>
                    <?php echo $form->error($model,'list_receiver'); ?>
            </div>

            <div class="row">
                    <?php echo $form->labelEx($model,'list_handle'); ?>
                    <?php echo $form->textArea($model,'list_handle',array('cols'=>50,'rows'=>5)); ?>
                    <?php echo $form->error($model,'list_handle'); ?>
		      <p style="font-weight: bold; color: red; margin-left: 150px;"><span style="color: #333;">The format:</span> 20xxxxxxxx,20xxxxxxxx</p>
            </div>

            <div class="row">
                    <?php echo $form->labelEx($model,'content'); ?>
                    <?php echo $form->textArea($model,'content',array('cols'=>50,'rows'=>5)); ?>
                    <?php echo $form->error($model,'content'); ?>
            </div>
        </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    function getSender(id1,id2) {
        var s1 = document.getElementById(id1);
        var s2 = document.getElementById(id2);
        if(s1.value != '')
            s2.style.display    = 'none';
        else
            s2.style.display    = 'inline';
    }
    
    function isBillAlert(id1,id2,id3) {
        var t1  = document.getElementById(id1);
        var t2  = document.getElementById(id2);
        var t3  = document.getElementById(id3);
        if(t1.checked) {
            t2.style.display    = 'block';
            t3.style.display    = 'none';
        } else {
            t2.style.display    = 'none';
            t3.style.display    = 'block';
        }
    }
</script>