<?php
/* @var $this SmsMtController */
/* @var $model SmsMt */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'send-sms-form',
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'shortcode'); ?>
		<?php echo $form->dropDownlist($model,'sender', Member::model()->getShortcode(),array('style'=>'width:140px;','onchange'=>'getSender("SendSms_sender","sendby")')); ?>
                <div id="sendby" style="display: inline;">
                    <span style="margin-left: 20px;">Send By</span>
                    <?php echo $form->textField($model,'sendby'); ?>
                </div>
		<?php echo $form->error($model,'shortcode'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'service_id'); ?>
		<?php echo $form->dropDownlist($model,'service_id', Member::model()->getListService(),array('style'=>'width:140px;')); ?>
		<?php echo $form->error($model,'service_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'list_receiver'); ?>
		<?php echo $form->fileField($model,'list_receiver'); ?>
		<?php echo $form->error($model,'list_receiver'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'list_handle'); ?>
		<?php echo $form->textArea($model,'list_handle',array('cols'=>50,'rows'=>5)); ?>
		<?php echo $form->error($model,'list_handle'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'content'); ?>
		<?php echo $form->textArea($model,'content',array('cols'=>50,'rows'=>5)); ?>
		<?php echo $form->error($model,'content'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    function getSender(id1,id2) {
        var s1 = document.getElementById(id1);
        var s2 = document.getElementById(id2);
        if(s1.value != '')
            s2.style.display    = 'none';
        else
            s2.style.display    = 'inline';
    }
</script>