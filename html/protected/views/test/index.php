<?php
/* @var $this Controller */
/* @var $form CActiveForm */
?>

<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Demo Server Push Long-Pull</title>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">
            <form class="row" method="<?php echo $form->method; ?>" action="<?php echo $form->action; ?>">
                <label class="col-xs-2">Name</label>
                <div class="col-xs-2">
                    <input type="text" name="username" />
                </div>
                <label class="col-xs-2">Chat</label>
                <div class="col-xs-2">
                    <input type="text" name="msg" id="chat_input" />
                </div>
                <div class="col-xs-2">
                    <button type="submit">Send</button>
                </div>
            </form>
            <ul id="chat-msg" class="list-unstyled" url="<?php echo $this->createUrl("test/chat"); ?>">

            </ul>
        </div>
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="http://malsup.github.com/min/jquery.form.min.js"></script>
        <script type="text/javascript">
            (function($) {
                $.commet = function(options) {
                    $.fun = {
                        version: "1.1",
                        author: "uybv",
                        url: "",
                        time: <?php echo time(); ?>,
                        id: 0,
                        init: function() {
                            var obj = this;
                            obj.url = $("#chat-msg").attr("url");

                            $("form").submit(function() {
                                $(this).ajaxSubmit({
                                    async: false,
                                    cache: false,
                                    success: function(response) {
                                        $("#chat_input").val("")
                                                .focus();
                                    },
                                    error: function() {
                                    }
                                });
                                return false;
                            });

                            $("button").click(function(e) {
                                e.preventDefault();
                                $("form").submit();
                            });
                            obj.connect();
                            return obj;
                        },
                        connect: function() {
                            var obj = this;
                            $.ajax({
                                type: "POST",
                                url: obj.url,
                                async: true,
                                cache: false,
                                data: {
                                    time: obj.time
                                },
                                dataType: "json",
                                success: function(response) {
                                    if (response.status === true && response.data) {
                                        obj.time = response.time;
                                        $(response.data).each(function(key, val) {
                                            var li = $("<li>");
                                            var div = $("<div>").addClass("row");
                                            var lable = $("<label>").addClass("col-xs-1").html(this.name + ":");
                                            var p = $("<p>").addClass("col-xs-10").html(this.msg);

                                            div.append(lable).append(p)
                                                    .appendTo(li)
                                                    .prependTo("ul");
                                        });
                                    } else {

                                    }
                                    setTimeout(function() {
                                        obj.connect.apply(obj);
                                    }, 0);
                                },
                                error: function(XMLHttpRequest, textStatus, errorThrown) {
                                    //alert("error: " + textStatus + "  " + errorThrown);
                                    setTimeout(function() {
                                        obj.connect.apply(obj);
                                    }, 5000);
                                }
                            });
                        },
                        disconnect: function() {

                        }
                    };

                    return $.fun.init();
                };
            })(jQuery);

            $(function() {
                $.commet();
            });
        </script>
    </body>
</html>
