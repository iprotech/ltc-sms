<?php
$this->breadcrumbs=array(
	'Charging Histories'=>array('index'),
	$model->_id,
);

?>

<h1>View ChargingHistory #<?php echo $model->_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'_id',
		'msisdn',
		'member_id',
		'start_charging_date',
		'end_charging_date',
		'total_money',
		'service_id',
		'mo_id',
		'mt_id',
		'service_name',
		'charging_type',
		'convert_status',
		'delete_status',
		'sub_type',
		'cdr_status',
		'shortcode',
		'keyword',
		'old_balance',
		'new_balance',
		'total_vote',
		
	),
)); ?>