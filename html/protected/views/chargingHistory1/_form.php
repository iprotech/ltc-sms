<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'charging-history1-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'_id'); ?>
		<?php echo $form->textField($model,'_id'); ?>
		<?php echo $form->error($model,'_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'msisdn'); ?>
		<?php echo $form->textField($model,'msisdn'); ?>
		<?php echo $form->error($model,'msisdn'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'member_id'); ?>
		<?php echo $form->textField($model,'member_id'); ?>
		<?php echo $form->error($model,'member_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'start_charging_date'); ?>
		<?php echo $form->textField($model,'start_charging_date'); ?>
		<?php echo $form->error($model,'start_charging_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'end_charging_date'); ?>
		<?php echo $form->textField($model,'end_charging_date'); ?>
		<?php echo $form->error($model,'end_charging_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'total_money'); ?>
		<?php echo $form->textField($model,'total_money'); ?>
		<?php echo $form->error($model,'total_money'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'service_id'); ?>
		<?php echo $form->textField($model,'service_id'); ?>
		<?php echo $form->error($model,'service_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mo_id'); ?>
		<?php echo $form->textField($model,'mo_id'); ?>
		<?php echo $form->error($model,'mo_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mt_id'); ?>
		<?php echo $form->textField($model,'mt_id'); ?>
		<?php echo $form->error($model,'mt_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'service_name'); ?>
		<?php echo $form->textField($model,'service_name'); ?>
		<?php echo $form->error($model,'service_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'charging_type'); ?>
		<?php echo $form->textField($model,'charging_type'); ?>
		<?php echo $form->error($model,'charging_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'convert_status'); ?>
		<?php echo $form->textField($model,'convert_status'); ?>
		<?php echo $form->error($model,'convert_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'delete_status'); ?>
		<?php echo $form->textField($model,'delete_status'); ?>
		<?php echo $form->error($model,'delete_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sub_type'); ?>
		<?php echo $form->textField($model,'sub_type'); ?>
		<?php echo $form->error($model,'sub_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cdr_status'); ?>
		<?php echo $form->textField($model,'cdr_status'); ?>
		<?php echo $form->error($model,'cdr_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'shortcode'); ?>
		<?php echo $form->textField($model,'shortcode'); ?>
		<?php echo $form->error($model,'shortcode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'keyword'); ?>
		<?php echo $form->textField($model,'keyword'); ?>
		<?php echo $form->error($model,'keyword'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'old_balance'); ?>
		<?php echo $form->textField($model,'old_balance'); ?>
		<?php echo $form->error($model,'old_balance'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'new_balance'); ?>
		<?php echo $form->textField($model,'new_balance'); ?>
		<?php echo $form->error($model,'new_balance'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'total_vote'); ?>
		<?php echo $form->textField($model,'total_vote'); ?>
		<?php echo $form->error($model,'total_vote'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->