<?php
$this->breadcrumbs=array(
	'Charging Histories'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ChargingHistory', 'url'=>array('index')),
	array('label'=>'Create ChargingHistory', 'url'=>array('create')),
	array('label'=>'View ChargingHistory', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ChargingHistory', 'url'=>array('admin')),
);
?>

<h1>Update ChargingHistory <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>