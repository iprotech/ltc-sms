<?php
$this->breadcrumbs=array(
	'Charging Histories'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('charging-history2-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

?>


<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php $model->search(); ?>
<div class="thongke">
    <p>
        Total Revenue:
        <span><?php echo $model->totalmoney;?></span>
    </p>
    <p>
        Total Vote:
        <span><?php echo $model->totalvote; ?></span>
    </p>
</div>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'charging-history2-grid',
	'dataProvider'=>$model->search(), 
        'ajaxUpdate'=>false,
	'columns'=>array(
                array(
                    'name' => 'ID',          
                    'type' => 'raw',
                    'value' => 'CHtml::encode($data->id)'
                ),
                array(
                    'name' => 'Msisdn',          
                    'type' => 'raw',
                    'value' => 'CHtml::encode($data->msisdn)'
                ),
                array(
                    'name' => 'Start Charging Date',          
                    'type' => 'raw',
                    'value' => 'CHtml::encode($data->start_charging_date)'
                ),
                array(
                    'name' => 'End Charging Date',          
                    'type' => 'raw',
                    'value' => 'CHtml::encode($data->end_charging_date)'
                ),
                array(
                    'name' => 'Total Money',          
                    'type' => 'raw',
                    'value' => 'CHtml::encode($data->total_money)'
                ),
                array(
                    'name' => 'Shortcode',          
                    'type' => 'raw',
                    'value' => 'CHtml::encode($data->shortcode)'
                ),
                array(
                    'name' => 'Service Name',          
                    'type' => 'raw',
                    'value' => 'CHtml::encode($data->service_name)'
                ),
                array(
                    'name' => 'Sub Type',          
                    'type' => 'raw',
                    'value' => 'CHtml::encode($data->getType())'
                ),
                array(
                    'name' => 'Keyword',          
                    'type' => 'raw',
                    'value' => 'CHtml::encode($data->keyword)'
                ),
                array(
                    'name' => 'Vote Time',          
                    'type' => 'raw',
                    'value' => 'CHtml::encode($data->total_vote)'
                ),
                array(
                    'name' => 'Old Balance',          
                    'type' => 'raw',
                    'value' => 'CHtml::encode($data->old_balance)'
                ),
                array(
                    'name' => 'New Balance',          
                    'type' => 'raw',
                    'value' => 'CHtml::encode($data->new_balance)'
                ),
		array(
                    'class' => 'CButtonColumn',
                    'template' => '{view}'
                ),
	),
)); ?>