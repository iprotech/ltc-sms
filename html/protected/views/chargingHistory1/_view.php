<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('msisdn')); ?>:</b>
	<?php echo CHtml::encode($data->msisdn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('member_id')); ?>:</b>
	<?php echo CHtml::encode($data->member_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('start_charging_date')); ?>:</b>
	<?php echo CHtml::encode($data->start_charging_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('end_charging_date')); ?>:</b>
	<?php echo CHtml::encode($data->end_charging_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_money')); ?>:</b>
	<?php echo CHtml::encode($data->total_money); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('service_id')); ?>:</b>
	<?php echo CHtml::encode($data->service_id); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('mo_id')); ?>:</b>
	<?php echo CHtml::encode($data->mo_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mt_id')); ?>:</b>
	<?php echo CHtml::encode($data->mt_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('service_name')); ?>:</b>
	<?php echo CHtml::encode($data->service_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('charging_type')); ?>:</b>
	<?php echo CHtml::encode($data->charging_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('convert_status')); ?>:</b>
	<?php echo CHtml::encode($data->convert_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('delete_status')); ?>:</b>
	<?php echo CHtml::encode($data->delete_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sub_type')); ?>:</b>
	<?php echo CHtml::encode($data->sub_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cdr_status')); ?>:</b>
	<?php echo CHtml::encode($data->cdr_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shortcode')); ?>:</b>
	<?php echo CHtml::encode($data->shortcode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keyword')); ?>:</b>
	<?php echo CHtml::encode($data->keyword); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('old_balance')); ?>:</b>
	<?php echo CHtml::encode($data->old_balance); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('new_balance')); ?>:</b>
	<?php echo CHtml::encode($data->new_balance); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_vote')); ?>:</b>
	<?php echo CHtml::encode($data->total_vote); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('_id')); ?>:</b>
	<?php echo CHtml::encode($data->_id); ?>
	<br />

	*/ ?>

</div>