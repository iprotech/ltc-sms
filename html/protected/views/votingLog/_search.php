<?php
/* @var $this VotingLogController */
/* @var $model VotingLog */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php
    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl("votingLog/getWinner"),
        'method' => 'get',
        'htmlOptions' => array('name' => 'myForm')
    ));
    ?>

    <div class="row" style="width: 26%;float: left">
        <?php echo $form->label($model, 'serviceId', array('style' => 'width:110px;')); ?>
        <?php echo $form->dropDownlist($model, 'serviceId', VotingKeyword::model()->getService(), array('style' => 'width:140px;')); ?>
    </div>
    <div class="row" style="width: 70%;float: left">
        <?php echo $form->label($model, 'sub_type', array('style' => 'width:110px;')); ?>
        <?php echo $form->dropDownlist($model, 'sub_type', $model->getSubTypeList(), array('style' => 'width:140px;', 'prompt' => 'Select subtype')); ?>
    </div>

    <div class="clear"></div>

    <div class="row" style="width: 26%;float: left">
        <?php echo $form->label($model, 'memberType', array('style' => 'width:110px;')); ?>
        <?php echo $form->dropDownlist($model, 'memberType', array(3 => "All member", 2 => "Member Test", 1 => "Non-Member Test"), array('style' => 'width:140px;', 'prompt' => 'Member type')); ?>
    </div>

    <div class="clear"></div>

    <div class="row" style="width:26%;float:left;">
        <?php echo $form->label($model, 'time_start', array('style' => 'width:110px;')); ?>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model, //Model object
            'attribute' => 'time_start', //attribute name
            'mode' => 'datetime', //use "time","date" or "datetime" (default)
            'options' => array("dateFormat" => 'yy/mm/dd'), // jquery plugin options
            'language' => ''
        ));
        ?>
    </div>

    <div class="row" style="width:70%;float:left;">
        <?php echo $form->label($model, 'time_end', array('style' => 'width:105px;')); ?>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model, //Model object
            'attribute' => 'time_end', //attribute name
            'mode' => 'datetime', //use "time","date" or "datetime" (default)
            'options' => array("dateFormat" => 'yy/mm/dd'), // jquery plugin options
            'language' => ''
        ));
        ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'option', array('style' => 'width:110px;')); ?>
        <?php
        echo $form->radioButtonList($model, 'option', $model->getListOption(), array('template' => '<li>{input}{label}</li>', 'container' => 'ul class="module-list" style="width: auto;"',
            'separator' => '', 'onClick' => 'javascript:selectOnlyThis(this);'));
        ?>
    </div>
    <div class="clear"></div>
    <div class="row" style="width: 26%;float: left">
        <label style="width:110px;">Is Matching</label>
        <?php echo $form->checkBox($model, 'is_match', array('value' => 4, 'style' => 'margin-top: 7px;')); ?>
    </div>
    <div class="row" style="width: 70%;float: left">
        <?php echo $form->label($model, 'matching', array('style' => 'width:110px;')); ?>
        <?php echo $form->textField($model, 'matching'); ?>
    </div>
    <div class="clear"></div>
    <div class="row" style="width: 26%;float: left">
        <?php echo $form->label($model, 'limit', array('style' => 'width:110px;')); ?>
        <?php echo $form->textField($model, 'limit', array('maxlength' => 150)); ?>
    </div>
    <div class="row" style="width: 70%;float: left">
        <label style="width: 110px;">Total refresh: </label>
        <input type="number" id="total-refresh" />
    </div>
    <div class="clear"></div>
    <div class="row">
        <?php echo $form->label($model, 'prize', array('style' => 'width:110px;')); ?>
        <?php echo $form->textField($model, 'prize'); ?>
    </div>
    <div class="row">
        <label style="width: 110px;">Time: </label>
        <input type="number" id="total-time" /><span>s</span>
    </div>
    <div class="row buttons">
        <?php //echo CHtml::submitButton('Pick Up'); ?>
        <?php echo CHtml::button("Random", array("id" => "btnScript")); ?>
        <?php echo CHtml::button("Clear", array("id" => "btnClear")); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->
<script type="text/javascript">
    (function($, document, undefined) {
        $(document).ready(function() {
            /*
             $("#yw0").on("submit", function(e) {
             var total = $("#total-refresh").val();
             total = parseInt(total);
             if (total !== NaN && total > 0) {
             total--;
             $("#total-refresh").attr("disabled", "disabled");
             $("#total-refresh").val(total);
             if (total > 0) {
             setTimeout(function() {
             $("#yw0").find("[type='submit']").click();
             }, 5000);
             } else {
             $("#total-refresh").removeAttr("disabled");
             }
             }
             });
             */

            $("#btnClear").on("click", function(e) {
                e.preventDefault();
                if (confirm("are you sure you want to clear current history?")) {
                    $.ajax({
                        url: "index.php?r=votingLog/clear",
                        type: "GET",
                        data: {},
                        dataType: "json",
                        success: function(response) {
                            alert(response.msg);
                        }
                    });
                }
            });

            $("#btnScript").on("click", function(e) {
                e.preventDefault();

                $("#yw0").ajaxSubmit({
                    //url: "index.php?r=votingLog/getWinner",
                    dataType: "json",
                    success: function(response) {
                        if (response.status) {
                            if (response.result.length === 0) {
                                alert("No data");
                            } else {
                                var prize = $("#VotingLog_prize").val();
                                var time = $("#total-time").val();
                                $("<form method='post' action='index.php?r=votingLog/winner' target='_blank'>")
                                        .append($("<input name='prize' value='' />").val(prize))
                                        .append($("<input name='time' value='' />").val(time))
                                        .append($("<input name='result' value='' />").val(response.result.join()))
                                        .submit();
                            }
                        } else {
                            alert(response.msg);
                            $("#VotingLog_limit").focus();
                        }
                    }
                });
                /*
                 var result = [];
                 var prize = $("#VotingLog_prize").val();
                 var time = $("#total-time").val();
                 $("#voting-log-grid table > tbody > tr").each(function(k, v) {
                 var msisdn = $(v).children("td").first().html();
                 result.push(msisdn);
                 });
                 
                 $("<form method='post' action='index.php?r=votingLog/winner'>")
                 .append($("<input name='prize' value='' />").val(prize))
                 .append($("<input name='time' value='' />").val(time))
                 .append($("<input name='result' value='' />").val(result.join()))
                 .submit();
                 */
            });

        });
    })(jQuery, document);
</script>