<?php
/* @var $this VotingLogController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Voting Logs',
);

$this->menu=array(
	array('label'=>'Create VotingLog', 'url'=>array('create')),
	array('label'=>'Manage VotingLog', 'url'=>array('admin')),
);
?>

<h1>Voting Logs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
