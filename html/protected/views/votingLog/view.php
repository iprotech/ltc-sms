<?php
/* @var $this VotingLogController */
/* @var $model VotingLog */

$this->breadcrumbs=array(
	'Voting Logs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List VotingLog', 'url'=>array('index')),
	array('label'=>'Create VotingLog', 'url'=>array('create')),
	array('label'=>'Update VotingLog', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete VotingLog', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage VotingLog', 'url'=>array('admin')),
);
?>

<h1>View VotingLog #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'msisdn',
		'keyword',
		'keyword_id',
		'created_datetime',
		'mo_id',
	),
)); ?>
