<?php
/* @var $this VotingLogController */
/* @var $model VotingLog */

$this->breadcrumbs=array(
	'Voting Logs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List VotingLog', 'url'=>array('index')),
	array('label'=>'Create VotingLog', 'url'=>array('create')),
	array('label'=>'View VotingLog', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage VotingLog', 'url'=>array('admin')),
);
?>

<h1>Update VotingLog <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>