<?php
/* @var $this VotingLogController */
/* @var $model VotingLog */

$this->breadcrumbs = array(
    'Voting Logs' => array('admin'),
    'Manage',
);

/*
Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('voting-log-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
 * 
 */
?>

<h1>Manage Voting Logs</h1>

<div class="search-form">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
/*
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'voting-log-grid',
    'dataProvider' => $model->search(),
    'columns' => array(
        //'id',
        'msisdn',
        'created_datetime',
        'keyword',
        'total_vote'
    ),
    'htmlOptions' => array(
        'style' => 'display:none;'
    )
));
*/