<?php
/* @var $this VotingLogController */
/* @var $model VotingLog */

$this->breadcrumbs=array(
	'Voting Logs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List VotingLog', 'url'=>array('index')),
	array('label'=>'Manage VotingLog', 'url'=>array('admin')),
);
?>

<h1>Create VotingLog</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>