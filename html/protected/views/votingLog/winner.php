<html>
    <head>
        <title>Winner</title>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/winner.css" />
        <?php Yii::app()->clientScript->scriptMap = array('jquery.js' => false); ?>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/winner.js"></script>
    </head>
    <body>
        <div id="wrapper">
            <div id="wrapper-phone">
                <ul id="list-number">
                    <li class="top5">0000000000</li>
                    <li class="top4">0000000000</li>
                    <li class="top3">0000000000</li>
                    <li class="top2">0000000000</li>
                    <li class="top1">0000000000</li>
                    <li class="top2">0000000000</li>
                    <li class="top3">0000000000</li>
                    <li class="top4">0000000000</li>
                    <li class="top5">0000000000</li>
                </ul>
            </div>
            <div id="wrapper-prize">
                <?php echo $prize; ?>
            </div>
            <div id="wrapper-winner">
                <i></i>
                <ul>
                </ul>
            </div>
        </div>
        <input type="hidden" value="<?php echo $result; ?>" id="txtResult" />
        <input type="hidden" value="<?php echo $totalTime; ?>" id="txtTotalTime" />
    </body>
</html>