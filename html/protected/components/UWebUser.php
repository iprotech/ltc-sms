<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CWebUser
 *
 */
class UWebUser extends CWebUser {

    private $account;
    public $returnUrl = 'index.php?r=service/admin';

    public function getMainMenu() {
        $menu = array();
        $account = $this->getAccount();

        if ($account) {
            $moduleList = Yii::app()->params['module'];
            $accountModules = explode(',', $account->module);
            foreach ($accountModules as $item) {
                if (isset($moduleList[$item]))
                    $menu[] = array('label' => $moduleList[$item]['name'], 'url' => $moduleList[$item]['url']);
            }
        }
        if ($this->isGuest) {
            $menu[] = array('label' => 'Login', 'url' => array('/site/login'));
        } else {
            $menu[] = array('label' => 'Change Password', 'url' => array('/myaccount/changepassword'));
            $menu[] = array('label' => 'Logout (' . $this->name . ')', 'url' => array('/site/logout'));
        }
        return $menu;
    }

    public function getGroup1() {
        $account = $this->getAccount();
        $menu = array();
        if ($account) {
            if (strlen(strstr($account->module, 'shortcode')) > 0)
                $menu[] = array('label' => 'Shortcode', 'url' => Yii::app()->createUrl('/shortcode/admin'));
            if (strlen(strstr($account->module, 'category')) > 0)
                $menu[] = array('label' => 'Category', 'url' => Yii::app()->createUrl('/category/admin'));
            if (strlen(strstr($account->module, 'service')) > 0)
                $menu[] = array('label' => 'Services', 'url' => Yii::app()->createUrl('/service/admin'));
            if (strlen(strstr($account->module, 'content')) > 0)
                $menu[] = array('label' => 'Content', 'url' => Yii::app()->createUrl('/content/admin'));
            if (strlen(strstr($account->module, 'sendSms')) > 0)
                $menu[] = array('label' => 'Send Sms', 'url' => Yii::app()->createUrl('/sendSms/create'));
        }

        return $menu;
    }

    public function getGroup2() {
        
        $currentDate = date("Y/m/d 00:00", time());
        
        $account = $this->getAccount();
        $menu = array();
        if ($account) {
            if (strlen(strstr($account->module, 'member')) > 0)
                $menu[] = array('label' => 'Member', 'url' => Yii::app()->createUrl('/member/admin'));
            if (strlen(strstr($account->module, 'chargingHistory')) > 0) {
                $menu[] = array('label' => 'Revenue', 'url' => Yii::app()->createUrl('/chargingHistory/admin', array(
                    "chargingHistory[time_start]" => $currentDate
                )));
            }
            if (strlen(strstr($account->module, 'smsMo')) > 0) {
                $menu[] = array('label' => 'MO', 'url' => Yii::app()->createUrl('/smsMo/admin', array(
                    "SmsMo[start_date]" => $currentDate
                )));
            }
            if (strlen(strstr($account->module, 'smsMt')) > 0) {
                $menu[] = array('label' => 'MT', 'url' => Yii::app()->createUrl('/smsMt/admin', array(
                    "SmsMt[start_date]" => $currentDate
                )));
            }
            if (strlen(strstr($account->module, 'reporting')) > 0)
                $menu[] = array('label' => 'Reports', 'url' => Yii::app()->createUrl('/reporting/admin'));
            if (strlen(strstr($account->module, 'emailReport')) > 0)
                $menu[] = array('label' => 'Email', 'url' => Yii::app()->createUrl('/emailReport/admin'));
            if (strlen(strstr($account->module, 'votingKeyword')) > 0)
                $menu[] = array('label' => 'Voting', 'url' => Yii::app()->createUrl('/votingKeyword/admin'));
	     if (strlen(strstr($account->module, 'votingLog')) > 0)
                $menu[] = array('label' => 'Random', 'url' => Yii::app()->createUrl('/votingLog/admin'));
        }

        return $menu;
    }

    public function getGroup3() {
        $account = $this->getAccount();
        $menu = array();
        if ($account) {
            if (strlen(strstr($account->module, 'memberCheck')) > 0)
                $menu[] = array('label' => 'Test', 'url' => Yii::app()->createUrl('/memberCheck/admin'));
            if (strlen(strstr($account->module, 'user')) > 0)
                $menu[] = array('label' => 'Admin & Accounts', 'url' => Yii::app()->createUrl('/user/admin'));
            if (strlen(strstr($account->module, 'monitor')) > 0)
                $menu[] = array('label' => 'Monitor', 'url' => Yii::app()->createUrl('/monitor/charging'));
             
        }
        return $menu;
    }

    public function authenticate($module) {
        $account = $this->getAccount();
        if ($account) {
            $accountModules = explode(',', $account->module);
            //return array_key_exists($moduleId, $accountModules);
            return in_array($module, $accountModules);
        }
        return FALSE;
    }

    public function getAccount() {
        if (!$this->account)
            $this->account = User::model()->findByAttributes(array('username' => $this->name, 'status' => 1));
        return $this->account;
    }

}

?>