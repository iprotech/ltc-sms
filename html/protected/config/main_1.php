<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'LTC CMS MANAGEMENT',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
    ),
    'modules' => array(
        // uncomment the following to enable the Gii tool

        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => '12345',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
    ),
    // application components
    'components' => array(
        'urlManager' => array(
            'rules' => array(
                'member/admin/<export>' => 'member/admin',
            ),
        ),
        'user' => array(
            // enable cookie-based authentication
            'class' => 'UWebUser'
        ),
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=cms_ltc',
            'emulatePrepare' => true,
            'username' => 'cms',
            'password' => 'cms',
            'charset' => 'utf8',
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'trace,log',
                    'categories' => 'system.db.CDbCommand',
                    'logFile' => 'db.log',
                )
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'webmaster@example.com',
        'path' => array(
            'member' => 'member/export/',
            'revenue' => 'revenue/export/',
            'smsmo' => 'data/export/',
            'smsmt' => 'data/export/',
            'report' => 'data/export/',
        ),
        'module' => array(
            'user' => array(
                'name' => 'User',
                'url' => array('/user/admin')
            ),
            'category' => array(
                'name' => 'Category',
                'url' => array('/category/admin')
            ),
            'member' => array(
                'name' => 'Member',
                'url' => array('/member/admin')
            ),
            'memberCheck' => array(
                'name' => 'Test Member',
                'url' => array('/memberCheck/admin')
            ),
            'content' => array(
                'name' => 'Content',
                'url' => array('/content/admin')
            ),
            'chargingHistory' => array(
                'name' => 'Revenue Statistic',
                'url' => array('/chargingHistory/admin')
            ),
            'reporting' => array(
                'name' => 'Reporting',
                'url' => array('/reporting/admin')
            ),
            'smsMo' => array(
                'name' => 'Sms_Mo',
                'url' => array('/smsMo/admin')
            ),
            'smsMt' => array(
                'name' => 'Sms_Mt',
                'url' => array('/smsMt/admin')
            ),
            'service' => array(
                'name' => 'Service',
                'url' => array('/service/admin')
            ),
            'commandCode' => array(
                'name' => 'Service Command',
                'url' => array('/commandCode/admin')
            ),
            'shortcode' => array(
                'name' => 'Shortcode',
                'url' => array('/shortcode/admin')
            ),
            'sendSms' => array(
                'name' => 'Send Sms',
                'url' => array('/sendSms/admin')
            ),
            'emailReport' => array(
                'name' => 'Email Report',
                'url' => array('/emailReport/admin')
            ),
            'votingKeyword' => array(
                'name' => 'Voting Chart',
                'url' => array('/votingKeyword/admin')
            ),
        )
    ),
);
