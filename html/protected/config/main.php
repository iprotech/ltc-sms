<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'LTC CMS MANAGEMENT',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.vendors.phpexcel.PHPExcel',
        'ext.yiireport.*',
        'application.models.*',
        'application.components.*',
        'ext.YiiMongoDbSuite.*',
    ),
    'modules' => array(
        // uncomment the following to enable the Gii tool

        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'thiendv@ltc',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'generatorPaths'=>array(
            'ext.YiiMongoDbSuite.gii'
        ),
			
			
        ),
    ),
    // application components
    'components' => array(
        'urlManager' => array(
            'rules' => array(
                'member/admin/<export>' => 'member/admin',
            ),
        ),
        'user' => array(
            // enable cookie-based authentication
            'class' => 'UWebUser'
        ),
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=smsproject',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ),
        'behaviors' => array(
            'edms' => array(
            'class'=>'EDMSBehavior',
            'connectionId' => 'mongodb',
            'debug'=>true //for extended logging
            )
       ),
        'mongodb' => array(
            'class'=> 'EMongoDB',
            'connectionString' => 'mongodb://localhost',
            'dbName'=> 'ltc_sms',
            'fsyncFlag'=> true,
            'safeFlag'=> true,
            'useCursor'=> false,
       ),
       'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
       ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'trace,log',
                    'categories' => 'system.db.CDbCommand',
                    'logFile' => 'db.log',
                ),
            ),
        ),
        'cache' => array(
            'class' => 'CMemCache',
            'servers' => array(
                array(
                    'host' => 'localhost',
                    'port' => 11211,
                    'weight' => 100,
                )
            )
        )
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'webmaster@example.com',
        'path' => array(
            'member' => 'member/export/',
            'revenue' => 'revenue/export/',
            'smsmo' => 'data/export/',
            'smsmt' => 'data/export/',
            'report' => 'data/export/',
        ),
        'module' => array(
            'user' => array(
                'name' => 'User',
                'url' => array('/user/admin')
            ),
            'category' => array(
                'name' => 'Category',
                'url' => array('/category/admin')
            ),
            'member' => array(
                'name' => 'Member',
                'url' => array('/member/admin')
            ),
            'memberCheck' => array(
                'name' => 'Test Member',
                'url' => array('/memberCheck/admin')
            ),
            'content' => array(
                'name' => 'Content',
                'url' => array('/content/admin')
            ),
            
            'chargingHistory' => array(
                'name' => 'Revenue Statistic',
                'url' => array('/chargingHistory/admin')
            ),
            
            'reporting' => array(
                'name' => 'Reporting',
                'url' => array('/reporting/admin')
            ),
            'smsMo' => array(
                'name' => 'Sms_Mo',
                'url' => array('/smsMo/admin')
            ),
            'smsMt' => array(
                'name' => 'Sms_Mt',
                'url' => array('/smsMt/admin')
            ),
            'service' => array(
                'name' => 'Service',
                'url' => array('/service/admin')
            ),
            'commandCode' => array(
                'name' => 'Service Command',
                'url' => array('/commandCode/admin')
            ),
            'shortcode' => array(
                'name' => 'Shortcode',
                'url' => array('/shortcode/admin')
            ),
            'sendSms' => array(
                'name' => 'Send Sms',
                'url' => array('/sendSms/admin')
            ),
            'emailReport' => array(
                'name' => 'Email Report',
                'url' => array('/emailReport/admin')
            ),
            'votingKeyword' => array(
                'name' => 'Voting Chart',
                'url' => array('/votingKeyword/admin')
            ),
            'votingLog' => array(
                'name' => 'Voting Log',
                'url' => array('/votingLog/admin')
            ),
            'monitor' => array(
                'name' => 'Monitor',
                'url' => array('/monitor/charging')
            )
        )
    ),
);
