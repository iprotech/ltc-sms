<?php

class ChargingHistory1Controller extends AccessController
{
	public $layout = '//layouts/column1';
        public $module = 'chargingHistory1';
        public $filename = 'Report_Revenue';
        
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	
	public function actionAdmin()
	{
		$model = new ChargingHistory1('search');
		$model->unsetAttributes();

		if(isset($_GET['ChargingHistory1']))
			$model->setAttributes($_GET['ChargingHistory1']);
                if (Yii::app()->request->getParam('export')) {
                    $this->exportData($model, array('msisdn', 'total_money', 'start_charging_date', 'service_name', 'sub_type_name', 'shortcode','keyword','total_vote','old_balance','new_balance'), $this->filename);
                    Yii::app()->end();
                }
		$this->render('admin', array(
			'model'=>$model
		));
	}

	public function loadModel($id)
	{
		$model=ChargingHistory1::model()->findByPk(new MongoId($id));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='charging-history1-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
