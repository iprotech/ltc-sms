<?php

class EmailReportController extends AccessController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $module = 'content';

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new EmailReport;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['EmailReport'])) {
            $model->attributes = $_POST['EmailReport'];
            if (isset($_POST['EmailReport']['serviceList']) && $_POST['EmailReport']['serviceList']) {
                $model->service = implode(',', $_POST['EmailReport']['serviceList']);
            }
            if (isset($_POST['EmailReport']['send_time']) && $_POST['EmailReport']['send_time']) {
                $model->send_time = $_POST['EmailReport']['send_time'];
            } else {
                $model->send_time = date('Y-m-d H:i:s');
            }
            if ($model->save())
                $this->redirect(array('admin'));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['EmailReport'])) {
            $model->attributes = $_POST['EmailReport'];
            if (isset($_POST['EmailReport']['serviceList']) && $_POST['EmailReport']['serviceList']) {
                $model->service = implode(',', $_POST['EmailReport']['serviceList']);
            }
            if (isset($_POST['EmailReport']['send_time']) && $_POST['EmailReport']['send_time']) {
                $model->send_time = $_POST['EmailReport']['send_time'];
            } else {
                $model->send_time = date('Y-m-d H:i:s');
            }
            if ($model->save())
                $this->redirect(array('admin'));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('EmailReport');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new EmailReport('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['EmailReport']))
            $model->attributes = $_GET['EmailReport'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return EmailReport the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = EmailReport::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param EmailReport $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'email-report-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
