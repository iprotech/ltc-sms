<?php

class VotingLogController extends AccessController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $module = 'votingLog';

    public function accessRules() {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('view', 'create', 'admin', 'export', 'draw', 'delallaction', 'delaction', 'delAllinOnlyTest', 'import', 'winner', 'getWinner', 'clear'),
                'users' => array('@'),
                'expression' => (string) $this->authenticate($this->module)
            ),
            array('allow',
                'actions' => array('delete', 'update'),
                'users' => array('@'),
                'expression' => (string) $this->full()
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new VotingLog('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['VotingLog']))
            $model->attributes = $_GET['VotingLog'];
        
        
        
        //if ($model->serviceId = )

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = VotingLog::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'voting-log-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionGetWinner() {
        $model = new VotingLog('search');
        $model->unsetAttributes();
        if (isset($_GET['VotingLog']))
            $model->attributes = $_GET['VotingLog'];

        if (!$model->limit) {
            echo json_encode(array(
                "status" => false,
                "msg" => "Please enter quality..."
            ));
            Yii::app()->end();
        }
        $result = array();
        if($model->serviceId == 19 || $model->serviceId == 21 || $model->serviceId == 31 || $model->serviceId == 82 || $model->serviceId == 84){
        $data = $model->findAll($model->getCriteria());
        if ($data) {
            $time = date("Y-m-d H:i:s", time());
            
            $random = Random::model()->findByAttributes(array("is_current" => "1"));
            $randomId = 0;
            $current_random = 1;
            if (!$random) {
                $randomItem = new Random();
                $randomItem->unsetAttributes();
                $randomItem->start_time = $time;
                $randomItem->is_current = 1;
                $randomItem->current_random = $current_random;
                $randomItem->insert();
                $randomId = $randomItem->id;
            } else {
                $randomId = $random->id;
                $current_random = $random->current_random + 1;
            }

            $i = 1;
            foreach ($data as $item) {
                $result[] = $item->msisdn;
                $randomHistory = new RandomHistory();
                $randomHistory->unsetAttributes();
                $randomHistory->msisdn = $item->msisdn;
                $randomHistory->position = $i;
                $randomHistory->random_id = $randomId;
                $randomHistory->random_rank = $current_random;
                $randomHistory->insert();
                $i++;
            }

            if (count($result) > 0 && $random) {
                $random->current_random = $current_random;
                $random->update();
            }
        }

        }
        else{
            $randomNotIn = Random::getAllMsisdnInCurrentRandom();
            $ran = '';
             if ($randomNotIn && count($randomNotIn) > 0) {
                for ($i = 0; $i < count($randomNotIn); $i++){
                    if(strlen($randomNotIn[$i]) > 0){
                        $ran .= $randomNotIn[$i] . ',';
                    }
                }
            $phone = substr($ran,0,strlen($ran) - 1);
            if($model->serviceId){
                $condition =" and service_id = {$model->serviceId}";
            }
            if($model->sub_type){
                $condition = " and sub_type = {$model->sub_type} and service_id = {$model->serviceId}";
            }
            if ($model->time_start) {
                $condition = " and created_datetime >= '{$model->time_start}' and service_id = {$model->serviceId}";
            }
            if ($model->time_end) {
                $condition = " and created_datetime <= '{$model->time_end}' and service_id = {$model->serviceId}";
            }
            if($model->is_match && $model->matching){
                $condition = " and content like '{$model->matching}%' and service_id = {$model->serviceId}";
            }
            if ($model->memberType == 1 || $model->memberType == 2) {
                $memberTest = array(0);
                $tempDt = MemberCheck::model()->findAll();
                if ($tempDt) {
                    foreach ($tempDt as $tempItem) {
                        $memberTest[] = $tempItem->msisdn;
                    }
                }
                for ($i = 0; $i < count($memberTest); $i++){
                    if(strlen($memberTest[$i]) > 0){
                        $mem .= $memberTest[$i] . ',';
                    }
                }
                $test = substr($mem,0,strlen($mem) - 1);
                if ($model->memberType == 1) {
                    $condition = " and sender not in ({$test}) and service_id = {$model->serviceId}";
                } else if ($model->memberType == 2) {
                    $condition = " and sender in ({$test}) and service_id = {$model->serviceId}";
                }
            }
                $sql = "select distinct service_id, sender from sms_mo where charging_status = 1 {$condition} and sender not in ($phone) order by rand() limit $model->limit";
            }
           
            $data = Yii::app()->db->createCommand($sql)->queryAll();
            if ($data) {
            $time = date("Y-m-d H:i:s", time());
            
            $random = Random::model()->findByAttributes(array("is_current" => "1"));
            $randomId = 0;
            $current_random = 1;
            if (!$random) {
                $randomItem = new Random();
                $randomItem->unsetAttributes();
                $randomItem->start_time = $time;
                $randomItem->is_current = 1;
                $randomItem->current_random = $current_random;
                $randomItem->insert();
                $randomId = $randomItem->id;
            } else {
                $randomId = $random->id;
                $current_random = $random->current_random + 1;
            }

            $i = 1;
            foreach ($data as $item) {
                $result[] = $item['sender'];
                $randomHistory = new RandomHistory();
                $randomHistory->unsetAttributes();
                $randomHistory->msisdn = $item['sender'];
                $randomHistory->position = $i;
                $randomHistory->random_id = $randomId;
                $randomHistory->random_rank = $current_random;
                $randomHistory->insert();
                $i++;
            }

            if (count($result) > 0 && $random) {
                $random->current_random = $current_random;
                $random->update();
            }
        }
        

        }
        echo json_encode(array(
            "status" => true,
            "result" => $result
        ));
        Yii::app()->end();
    }

    public function actionWinner() {
        $this->layout = FALSE;
        $prize = Yii::app()->request->getPost("prize");
        $data = Yii::app()->request->getPost("result");
        $time = Yii::app()->request->getPost("time");
        
        if ($time) {
            $time = intval($time) * 1000;
        } else {
            $time = 20000;
        }

        $result = array();

        if ($data) {
            $datas = explode(",", $data);
            foreach ($datas as $item) {
                $result[] = str_replace("856", "", $item);
            }
        }
        if (!$result) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        $this->render("winner", array(
            "result" => implode(",", array_reverse($result)),
            "prize" => $prize,
            "totalTime" => $time
        ));
    }

    public function actionClear() {
        $random = Random::model()->findByAttributes(array("is_current" => "1"));
        if ($random) {
            $random->is_current = 0;
            $random->update();
        }

        echo json_encode(array(
            "status" => true,
            "msg" => "Cleared!"
        ));
    }

}
