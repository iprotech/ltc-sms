<?php
    class ServiceController extends AccessController {

        /**
         * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
         * using two-column layout. See 'protected/views/layouts/column2.php'.
         */
        public $layout = '//layouts/column2';
        public $module = 'service';
		public $filename = 'export_service';
        /**
         * Displays a particular model.
         * @param integer $id the ID of the model to be displayed
         */
        public function actionView($id) {
            $this->render('view', array(
                'model' => $this->loadModel($id),
            ));
        }

        /**
         * Creates a new model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         */
        public function actionCreate() {
            $model = new Service;

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if (isset($_POST['Service'])) {
                $model->created_datetime = date('Y-m-d H:i:s', time());
                $model->updated_datetime = date('Y-m-d H:i:s', time());
                $model->attributes = $_POST['Service'];
                $service_id = $model->id;
                if (isset($_POST['Service']['service_content_id']))
                    $model->service_content_id = $_POST['Service']['service_content_id'];
                else
                    $model->service_content_id = '';
                if (isset($_POST['Service']['buffetSelected']))
                    $model->buffet = implode(',', $_POST['Service']['buffetSelected']);
                else
                    $model->buffet = '';
                if (isset($_POST['Service']['reply_content']))
                    $model->reply_content = $_POST['Service']['reply_content'];
                else
                    $model->reply_content = NULL;

                if(isset($_POST['Service']['shortcode_charging']))
                    $model->shortcode_charging  = $_POST['Service']['shortcode_charging'];

                if (isset($_POST['Service']['invalid_content']))
                    $model->invalid_content = $_POST['Service']['invalid_content'];
                else
                    $model->invalid_content = NULL;

                if (isset($_POST['Service']['start_time']))
                    $model->start_time = $_POST['Service']['start_time'];
                if (isset($_POST['Service']['end_time']))
                    $model->end_time = $_POST['Service']['end_time'];

                if (strlen($_POST['Service']['send_from']) > 1)
                    $model->send_from = $_POST['Service']['send_from'];
                else
                    $model->send_from = $_POST['Service']['shortcode'];

                $sql_after = '';
                if (isset($_POST['voting_name']))
                    $name = $_POST['voting_name'];
                if (isset($_POST['voting_key']))
                    $keywords = $_POST['voting_key'];
                if (isset($_POST['voting_content']))
                    $reply = $_POST['voting_content'];

                if (isset($_POST['Service']['max_vote_time'])) {
                    $model->max_vote_time = intval($_POST['Service']['max_vote_time']);
                }

                if (isset($_POST['Service']['max_money'])) {
                    $model->max_money = intval($_POST['Service']['max_money']);
                }

                if (isset($_POST['Service']['has_postpad_vote'])) {
                    $model->has_postpad_vote = intval($_POST['Service']['has_postpad_vote']);
                }
                $FileVote = CUploadedFile::getInstanceByName('Service[list_vote]');
                if($FileVote) $model->list_vote = $FileVote;
                if ($model->save()){
                    if(isset($FileVote) && $FileVote){
                        $fileName = preg_replace('/\s+/', '', $model->list_vote->getName());
                        $fileName = str_replace(' ', '', $fileName);
                        $fileTemp = explode('.', $fileName);
                        if( $model->list_vote->saveAs(dirname(dirname(dirname(__FILE__))).'/data/voting/' . $fileName)){
                            $sql = '';
                            $sql_before = 'INSERT INTO voting_keyword(name,keyword,reply_content,service_id) VALUES ';
                            $ListVote = file('data/voting/' . $fileName);

                            if (file('data/voting/' . $fileName)) {
                                $nv = $ListVote;
                                if (count($nv) > 0) {
                                    for ($j = 0; $j < count($nv); $j++) {
                                        $arFull = explode(',', $nv[$j]);
                                        if (isset($arFull[0]))
                                            $name = trim($arFull[0]);
                                        if (isset($arFull[1]))
                                            $key = trim($arFull[1]);
                                        if (isset($arFull[2]))
                                            $content = trim($arFull[2]);

                                        if (isset($name) && isset($key) && isset($content) && $name && $key && $content)
                                            $rs_vote[] = '("' . $name . '","' . $key . '","' . $content . '","' . $model->id. '")';
                                        unset($arFull);
                                        unset($content);
                                        unset($key);
                                        unset($name);
                                    }
                                }

                                if (isset($rs_vote) && count($rs_vote) > 0) {
                                    $cmd = Yii::app()->db->createCommand($sql_before . implode(',', $rs_vote));
                                    $cmd->execute();

                                }
                            }
                        }
                    }
                $rsUser = array();
                $sqlUser = 'SELECT id,service FROM account WHERE group_id=1';
                $cmdUser = Yii::app()->db->createCommand($sqlUser);
                $rsUser = $cmdUser->queryAll();
                foreach ($rsUser as $item) {
                    $cmdUser->update('account', array('service' => $item['service'] . '[' . $model->id . ']'), 'group_id=:group_id AND id=:id', array(':group_id' => 1, ':id' => $item['id']));
                }

                $this->redirect(array('view', 'id' => $model->id));
            }
        }
            $this->render('create', array(
                'model' => $model,
            ));
        }

        /**
         * Updates a particular model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id the ID of the model to be updated
         */
        public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $cri = new CDbCriteria;
        $cri->order = 'id';
        $keys = VotingKeyword::model()->findAllByAttributes(array('service_id' => $id,'status'=>1), $cri);
        $sql_after = '';

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Service'])) {
            $model->updated_datetime = date('Y-m-d H:i:s', time());
            $model->attributes = $_POST['Service'];
            if (isset($_POST['Service']['service_content_id']))
                $model->service_content_id = $_POST['Service']['service_content_id'];
            else
                $model->service_content_id = '';
            if (isset($_POST['Service']['buffetSelected']))
                $model->buffet = implode(',', $_POST['Service']['buffetSelected']);
            else
                $model->buffet = '';
            if (isset($_POST['Service']['reply_content']))
                $model->reply_content = $_POST['Service']['reply_content'];
            else
                $model->reply_content = NULL;
            
            if(isset($_POST['Service']['shortcode_charging']))
                $model->shortcode_charging  = $_POST['Service']['shortcode_charging'];

            if (isset($_POST['Service']['invalid_content']))
                $model->invalid_content = $_POST['Service']['invalid_content'];
            else
                $model->invalid_content = NULL;

            if (isset($_POST['Service']['start_time']))
                $model->start_time = $_POST['Service']['start_time'];
            if (isset($_POST['Service']['end_time']))
                $model->end_time = $_POST['Service']['end_time'];

            $model->updated_datetime = date('Y-m-d H:i:s', time());

            if (isset($_POST['voting_name']))
                $name = $_POST['voting_name'];
            if (isset($_POST['voting_key']))
                $keywords = $_POST['voting_key'];
            if (isset($_POST['voting_content']))
                $reply = $_POST['voting_content'];

            if (strlen($_POST['Service']['send_from']) > 1)
                $model->send_from = $_POST['Service']['send_from'];
            else
                $model->send_from = $_POST['Service']['shortcode'];
            
            if (isset($_POST['Service']['max_vote_time'])) {
                $model->max_vote_time = intval($_POST['Service']['max_vote_time']);
            }
            
            if (isset($_POST['Service']['max_money'])) {
                $model->max_money = intval($_POST['Service']['max_money']);
            }
            
            if (isset($_POST['Service']['has_postpad_vote'])) {
                $model->has_postpad_vote = intval($_POST['Service']['has_postpad_vote']);
            }

            $rsMax = Yii::app()->db->createCommand('SELECT max(id) as max FROM voting_keyword WHERE service_id=' . $id)->queryRow();
            
            $FileVote = CUploadedFile::getInstanceByName('Service[list_vote]');
            if($FileVote) $model->list_vote = $FileVote;
            
            if ($model->save()) {
                if (isset($keywords)) {
                    $keyarr = array_keys($keywords);
                    $minID = $keyarr[0];
                    $maxID = $keyarr[count($keywords) - 1];
                    $min = $rsMax['max'] + 1;
                    $minUpdate = $rsMax['max'];
//                    echo 'min: ' . $min . '; minID=' . $minID . '; maxID=' . $maxID;
                    $sql_before = 'INSERT INTO voting_keyword(name,keyword,reply_content,service_id,status) VALUES ';
                }
            

                if (isset($keywords) && isset($reply)) {
                    if ($min < $maxID) {
                        for ($min; $min < $maxID; $min++) {
                            if ($name[$min] != '' && $keywords[$min] != '' && $reply[$min] != '')
                                $sql_after .= '("' . $name[$min] . '","' . $keywords[$min] . '","' . $reply[$min] . '","' . $model->id . '","1"), ';
                        }
                        if ($sql_after != '') {
                            $sql_full = $sql_before . substr($sql_after, 0, -2);
                            $cmd = Yii::app()->db->createCommand($sql_full);
                            $cmd->execute();
                        }
                    }

                    for ($minID; $minID <= $minUpdate; $minID++) {
                        if (in_array($minID, $keyarr)) {
                            $sqlUpdate = 'UPDATE voting_keyword SET 
                                                name="' . $name[$minID] . '",
                                                keyword="' . $keywords[$minID] . '",
                                                reply_content="' . $reply[$minID] . '" WHERE id=' . $minID;
                            Yii::app()->db->createCommand($sqlUpdate)->execute();
                        }
                    }
                }
                if(isset($FileVote) && $FileVote){
                    $sqlMax = "Select max(id) from voting_keyword where service_id = '{$model->id}'";
                    $max = Yii::app()->db->createCommand($sqlMax)->queryScalar();
                    $sqlUpdate = "UPDATE voting_keyword SET 
                                  status = '2'  where service_id = '{$model->id}' and id <= {$max}  ";
                    Yii::app()->db->createCommand($sqlUpdate)->execute();
                    
                    $fileName = preg_replace('/\s+/', '', $model->list_vote->getName());
                    $fileName = str_replace(' ', '', $fileName);
                    $fileTemp = explode('.', $fileName);
                    if( $model->list_vote->saveAs(dirname(dirname(dirname(__FILE__))).'/data/voting/' . $fileName)){
                        $sql = '';
                        $sql_before = 'INSERT INTO voting_keyword(name,keyword,reply_content,service_id,status) VALUES ';
                        $ListVote = file('data/voting/' . $fileName);
                        if (file('data/voting/' . $fileName)) {
                            $nv = $ListVote;
                            if (count($nv) > 0) {
                                for ($j = 0; $j < count($nv); $j++) {
                                    $arFull = explode(',', $nv[$j]);
                                    if (isset($arFull[0]))
                                        $name = trim($arFull[0]);
                                    if (isset($arFull[1]))
                                        $key = trim($arFull[1]);
                                    if (isset($arFull[2]))
                                        $content = trim($arFull[2]);

                                    if (isset($name) && isset($key) && isset($content) && $name && $key && $content)
                                        $rs_vote[] = '("' . $name . '","' . $key . '","' . $content . '","' . $model->id. '","1")';
                                    unset($arFull);
                                    unset($content);
                                    unset($key);
                                    unset($name);
                                }
                            }

                            if (isset($rs_vote) && count($rs_vote) > 0) {
                                $cmd = Yii::app()->db->createCommand($sql_before . implode(',', $rs_vote));
                                $cmd->execute();
                            }
                            
                        }
                    }
                }

                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model, 'keys' => $keys
        ));
    }
        /**
         * Deletes a particular model.
         * If deletion is successful, the browser will be redirected to the 'admin' page.
         * @param integer $id the ID of the model to be deleted
         */
        public function actionDelete($id) {
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }

        /**
         * Lists all models.
         */
        public function actionIndex() {
            $dataProvider = new CActiveDataProvider('Service');
            $this->render('index', array(
                'dataProvider' => $dataProvider,
            ));
        }

        /**
         * Manages all models.
         */
        public function actionAdmin() {
            $model = new Service('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['Service']))
                $model->attributes = $_GET['Service'];
			if(Yii::app()->request->getParam('export')) {
            $model->search();
            while(1){
               $sql = "select status from command_query where  id = $model->export_id ";
                $cmd = Yii::app()->db->createCommand($sql)->queryScalar();
                $file = "export/Export_SERVICE"."_".$model->export_id."."."csv";
                if($cmd == 2){
                    $this->redirect($file);
                }else{
                    sleep(5);
                }
                //if($i++>3) break;
            }
            exit();

        }
            $this->render('admin', array(
                'model' => $model,
            ));
        }

        /**
         * Returns the data model based on the primary key given in the GET variable.
         * If the data model is not found, an HTTP exception will be raised.
         * @param integer the ID of the model to be loaded
         */
        public function loadModel($id) {
            $model = Service::model()->findByPk($id);
            if ($model === null)
                throw new CHttpException(404, 'The requested page does not exist.');
            return $model;
        }

        /**
         * Performs the AJAX validation.
         * @param CModel the model to be validated
         */
        protected function performAjaxValidation($model) {
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'service-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
        }

    }
?>