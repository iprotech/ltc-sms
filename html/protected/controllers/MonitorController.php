<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MonitorController
 *
 * @author BUI VAN UY
 */
class MonitorController extends AccessController {

    //put your code here
    public $layout = '//layouts/column1';
    public $module = 'monitor';

    public function accessRules() {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('charging', 'ChargingData', "*", "test"),
                'users' => array('@'),
                'expression' => (string) $this->authenticate($this->module)
            ),
            array('allow',
                'actions' => array('delete', 'update'),
                'users' => array('@'),
                'expression' => (string) $this->full()
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionCharging() {
        $this->render('charging');
    }

    private function getStatus($status) {
        if ($status == 1) {
            return "Successful";
        } else if ($status == 0) {
            return "Unsuccessful";
        } else if ($status == 99) {
            return "Error";
        }
        return "Undefined";
    }

    public function actionChargingData($time, $lastLine) {
        $this->layout = FALSE;
        $data = array();

        $path = "/var/www/html/data/log/charging_" . date("Y_m_d") . ".txt";
        $lastTime = filemtime($path);
        $endLine = "";

        if (intval($lastTime) > intval($time)) {
            $f = new ReverseFile($path);

            $i = 0;
            foreach ($f as $line) {
                $dt = explode("|", $line);
                if (count($dt) > 1) {
                    $i++;

                    if ($line == $lastLine || $i >= 20) {
                        break;
                    }

                    if (count($dt) === 5) {
                        $data[] = array(
                            "time" => $dt[4],
                            "action" => $dt[0],
                            "memberId" => $dt[1],
                            "msisdn" => $dt[2],
                            "status" => $dt[3],
                            "statusName" => $this->getStatus(intval($dt[3])),
                            "info" => ""
                        );
                    } else if (count($dt) === 7) {
                        $data[] = array(
                            "time" => $dt[6],
                            "action" => $dt[0],
                            "memberId" => $dt[1],
                            "msisdn" => $dt[2],
                            "status" => $dt[3],
                            "statusName" => $this->getStatus(intval($dt[3])),
                            "info" => "Service = " . $dt[5] . "; Money = " . $dt[4]
                        );
                    }

                    if ($endLine === "") {
                        $endLine = $line;
                    }
                }
            }
        }

        echo json_encode(array(
            "status" => true,
            "data" => array_reverse($data),
            "time" => $lastTime,
            "lastLine" => $endLine
        ));
    }

    public function actionTest() {
        $pid = exec('su - root -p ltc@2014 -c "php /app/mo/xtest_schedule.php"', $output, $var_result);
        echo $pid;
        echo "<br/>";
        print_r($output);
        echo "<br/>";
        print_r($var_result);
    }

    private function startProcess() {
        //query the database for process id
        $query = "SELECT value, id FROM `settings` WHERE `name` = 'process' LIMIT 1";
        $result = mysql_query($query);
        $pid = mysql_result($result, 0, 'value');
        $rowId = mysql_result($result, 0, 'id');
        //check if the process is running
        exec("ps $pid", $pState);
        if (!(count($pState) >= 2) || empty($pid)) {
            //start the process
            $command = " php script.php ";
            $pid = exec("nohup $command > /dev/null 2>&1 & echo $!");
            //check if the process has start
            if ($pid) {
                //process started, updating process id in the database
                $query = saveRow(array('id' => $rowId, 'name' => 'process', 'value' => $pid), 'settings');
            } else {
                //process not started!
            }
        }
    }

    private function stopProcess() {
        //query the database for process id
        $query = "SELECT value, id FROM `settings` WHERE `name` = 'process' LIMIT 1";
        $result = mysql_query($query);
        $pid = mysql_result($result, 0, 'value');
        $rowId = mysql_result($result, 0, 'id');
        //if no process is found in the database then skip everything
        if (!empty($pid)) {
            //check if the process from the database is running
            exec("ps $pid", $pState);
            if ((count($pState) >= 2)) {
                //if the process is running, kill it
                exec("kill $pid");
                //update database row with an empty process id
                saveRow(array('id' => $rowId, 'name' => 'process', 'value' => ''), 'settings');
            } else {
                //process is not running
            }
        } else {
            //no process found in the database
        }
    }

}
