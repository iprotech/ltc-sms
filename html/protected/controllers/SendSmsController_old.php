<?php

class SendSmsController extends AccessController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $module = 'sendSms';

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new SendSms;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['SendSms'])) {
            $model->attributes = $_POST['SendSms'];
            $model->receiveFile = CUploadedFile::getInstance($model, 'list_receiver');
            if ($model->validate()) {
                $Time_Now = date('Y-m-d H:i:s', time());
                $service_id = $model->service_id;
                $content = $model->content;
                
                if(isset($_POST['SendSms']['sender']) && $_POST['SendSms']['sender']) {
                    $sender = $model->sender;
                    $model->sendby  = Service::model()->findByPk($_POST['SendSms']['service_id'])->send_from;
                }else{
                    $sender = Service::model()->findByPk($model->service_id)->shortcode;
                    $model->sendby  = $_POST['SendSms']['sendby'];
                }
                
                $status = 0;
                if (isset($model->receiveFile) && $model->receiveFile) {
                    if ($model->receiveFile->saveAs('data/import/' . $model->receiveFile->getName())) {
                        $sql = '';
                        $sql_before = 'INSERT INTO sms_mt (receiver,sender,service,service_id,content,send_datetime,status,creater_id) VALUES ';
                        $ListMember = file('/var/www/html/data/import/' . $model->receiveFile->getName());
			if(file_get_contents('/var/www/html/data/import/' . $model->receiveFile->getName())){
                        var_dump($ListMember);
                        if (User::model()->findByPk(Yii::app()->user->id)->remain_sms < count($ListMember)&&!User::model()->findByPk(Yii::app()->user->id)->unlimit_sms)
                            $maxCount = User::model()->findByPk(Yii::app()->user->id)->remain_sms;
                        else
                            $maxCount = count($ListMember);

                        $re = '/^(20|30)[0-9]{7,8}$/';
                        for ($i = 0; $i < $maxCount; $i++) {
                            echo $ListMember[$i];
                            if (preg_match($re, trim($ListMember[$i])))
                                $sql .= '("856' . trim($ListMember[$i]) . '","' . $model->sendby . '","' . $sender . '","' . $service_id . '","' . $content . '","' . $Time_Now . '","' . $status . '","' . Yii::app()->user->id . '"), ';
                        }
                        
                        if ($sql != '') {
                            $sql_after = substr($sql, 0, -2);

                            $cmd = Yii::app()->db->createCommand($sql_before . $sql_after);
                            $cmd->execute();

                            if (User::model()->findByPk(Yii::app()->user->id)->unlimit_sms != '1') {
                                $sqlUpdate = 'UPDATE account SET remain_sms = remain_sms - ' . $maxCount . ' WHERE id=' . Yii::app()->user->id;
                                $upl = Yii::app()->db->createCommand($sqlUpdate)->execute();
                            }
                        }
			   }
                    }
                }

                if (isset($_POST['SendSms']['list_handle'])) {
                    $sql1 = '';
                    $sql_before1 = 'INSERT INTO sms_mt (receiver,sender,service,service_id,content,send_datetime,status,creater_id) VALUES ';
                    $arHandle = explode(',', $_POST['SendSms']['list_handle']);
                    if (User::model()->findByPk(Yii::app()->user->id)->remain_sms < count($arHandle)&&!User::model()->findByPk(Yii::app()->user->id)->unlimit_sms)
                            $maxCount = User::model()->findByPk(Yii::app()->user->id)->remain_sms;
                    else
                            $maxCount = count($arHandle);
                    
                    
                    $re1 = '/^(20|30)[0-9]{7,8}$/';
                    for ($i = 0; $i < $maxCount; $i++) {
                        if (preg_match($re1, trim($arHandle[$i])))
                            $sql1 .= '("856' . trim($arHandle[$i]) . '","' . $model->sendby . '","' . $sender . '","' . $service_id . '","' . $content . '","' . $Time_Now . '","' . $status . '","' . Yii::app()->user->id . '"), ';
                    }

                    if ($sql1 != '') {
                        $sql_after1 = substr($sql1, 0, -2);
                        $cmd1 = Yii::app()->db->createCommand($sql_before1 . $sql_after1);
                        $cmd1->execute();

                        if (User::model()->findByPk(Yii::app()->user->id)->unlimit_sms != '1') {
                            $sqlUpdate = 'UPDATE account SET remain_sms = remain_sms - ' . $maxCount . ' WHERE id=' . Yii::app()->user->id;
                            $upl = Yii::app()->db->createCommand($sqlUpdate)->execute();
                        }
                    }
                }

                $this->redirect(Yii::app()->createUrl('/smsMt/admin'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function loadModel($id) {
        $model = SendSms::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'send-sms-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
