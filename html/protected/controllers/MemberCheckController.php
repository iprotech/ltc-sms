<?php

class MemberCheckController extends AccessController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $module  = 'memberCheck';

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }
    
    public function actionDelallaction() {
        $sqlMember  = 'DELETE FROM member WHERE msisdn IN (SELECT msisdn FROM member_check)';
        $sqlCharge  = 'DELETE FROM charging_history WHERE msisdn IN (SELECT msisdn FROM member_check)';
        $sqlSmsMO   = 'DELETE FROM sms_mo WHERE sender IN (SELECT msisdn FROM member_check)';
        $sqlSmsMT   = 'DELETE FROM sms_mt WHERE receiver IN (SELECT msisdn FROM member_check)';
        Yii::app()->db->createCommand($sqlMember)->execute();
        Yii::app()->db->createCommand($sqlCharge)->execute();
        Yii::app()->db->createCommand($sqlSmsMO)->execute();
        Yii::app()->db->createCommand($sqlSmsMT)->execute();
        Yii::app()->db->createCommand('DELETE FROM member_check WHERE 1')->execute();
        
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }
    
    public function actionDelaction() {
        $sqlMember  = 'DELETE FROM member WHERE msisdn IN (SELECT msisdn FROM member_check)';
        $sqlCharge  = 'DELETE FROM charging_history WHERE msisdn IN (SELECT msisdn FROM member_check)';
        $sqlSmsMO   = 'DELETE FROM sms_mo WHERE sender IN (SELECT msisdn FROM member_check)';
        $sqlSmsMT   = 'DELETE FROM sms_mt WHERE receiver IN (SELECT msisdn FROM member_check)';
        Yii::app()->db->createCommand($sqlMember)->execute();
        Yii::app()->db->createCommand($sqlCharge)->execute();
        Yii::app()->db->createCommand($sqlSmsMO)->execute();
        Yii::app()->db->createCommand($sqlSmsMT)->execute();
        
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }
    
    public function actionDelAllinOnlyTest($id) {
        Yii::app()->db->createCommand('DELETE FROM member WHERE msisdn="'.$this->loadModel($id)->msisdn.'"')->execute();
        Yii::app()->db->createCommand('DELETE FROM charging_history WHERE msisdn="'.$this->loadModel($id)->msisdn.'"')->execute();
        Yii::app()->db->createCommand('DELETE FROM sms_mo WHERE sender="'.$this->loadModel($id)->msisdn.'"')->execute();
        Yii::app()->db->createCommand('DELETE FROM sms_mt WHERE receiver="'.$this->loadModel($id)->msisdn.'"')->execute();
        
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new MemberCheck;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['MemberCheck'])) {
            $model->attributes = $_POST['MemberCheck'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['MemberCheck'])) {
            $model->attributes = $_POST['MemberCheck'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        Yii::app()->db->createCommand('DELETE FROM member WHERE msisdn="'.$this->loadModel($id)->msisdn.'"')->execute();
        Yii::app()->db->createCommand('DELETE FROM charging_history WHERE msisdn="'.$this->loadModel($id)->msisdn.'"')->execute();
        Yii::app()->db->createCommand('DELETE FROM sms_mo WHERE sender="'.$this->loadModel($id)->msisdn.'"')->execute();
        Yii::app()->db->createCommand('DELETE FROM sms_mt WHERE receiver="'.$this->loadModel($id)->msisdn.'"')->execute();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new MemberCheck('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['MemberCheck']))
            $model->attributes = $_GET['MemberCheck'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = MemberCheck::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'member-check-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
