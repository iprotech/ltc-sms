<?php

class SendSmsController extends AccessController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $module = 'sendSms';

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new SendSms;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['SendSms'])) {
            $model->attributes = $_POST['SendSms'];
            //var_dump($_POST);
            //var_dump($model);
            $model->receiveFile = CUploadedFile::getInstance($model, 'list_receiver');
            $model->BillFile = CUploadedFile::getInstance($model, 'list_billalert');
            if ($model->validate()) {
                $Time_Now = date('Y-m-d H:i:s', time());
                if(@$_POST['SendSms']['send_datetime']){
                    $Time_Now = $_POST['SendSms']['send_datetime'];
                }
                $service_id = $model->service_id;
                $content = $model->content;
                if (isset($_POST['SendSms']['service_id']) && $_POST['SendSms']['service_id']) {
                    $sender = Service::model()->findByPk($model->service_id)->shortcode;
                    $model->sendby = Service::model()->findByPk($_POST['SendSms']['service_id'])->send_from;
                } else {
                    $sender = Service::model()->findByPk($model->service_id)->shortcode;
                    $model->sendby = $_POST['SendSms']['sendby'];
                }
                if (isset($_POST['SendSms']['language_id']) && $_POST['SendSms']['language_id'])
                $model->language_id = $_POST['SendSms']['language_id'];
                $status = 0;
                if (isset($model->receiveFile) && $model->receiveFile) {
                    if ($model->receiveFile->saveAs('data/import/' . $model->receiveFile->getName())) {
                        $sql = '';
                        $sql_before = 'INSERT INTO sms_mt (receiver,sender,service,service_id,content,send_datetime,status,creater_id,language_id) VALUES ';
                        $ListMember = file('data/import/' . $model->receiveFile->getName());
                        if (file_get_contents('data/import/' . $model->receiveFile->getName())) {
//                            var_dump($ListMember);
                            if (User::model()->findByPk(Yii::app()->user->id)->remain_sms < count($ListMember) && !User::model()->findByPk(Yii::app()->user->id)->unlimit_sms)
                                $maxCount = User::model()->findByPk(Yii::app()->user->id)->remain_sms;
                            else
                                $maxCount = count($ListMember);

                            $re = '/^(20|30)[0-9]{7,8}$/';
                            for ($i = 0; $i < $maxCount; $i++) {
                                echo $ListMember[$i];
                                if (preg_match($re, trim($ListMember[$i])))
                                    $sql .= '("856' . trim($ListMember[$i]) . '","' . $model->sendby . '","' . $sender . '","' . $service_id . '","' . $content . '","' . $Time_Now . '","' . $status . '","' . Yii::app()->user->id . '","'.$model->language_id.'"), ';
                            }

                            if ($sql != '') {
                                $sql_after = substr($sql, 0, -2);

                                $cmd = Yii::app()->db->createCommand($sql_before . $sql_after);
                                $cmd->execute();

                                if (User::model()->findByPk(Yii::app()->user->id)->unlimit_sms != '1') {
                                    $sqlUpdate = 'UPDATE account SET remain_sms = remain_sms - ' . $maxCount . ' WHERE id=' . Yii::app()->user->id;
                                    $upl = Yii::app()->db->createCommand($sqlUpdate)->execute();
                                }
                            }
                        }
                    }
                }

                if (isset($_POST['SendSms']['list_handle'])) {
                    $sql1 = '';
                    $sql_before1 = 'INSERT INTO sms_mt (receiver,sender,service,service_id,content,send_datetime,status,creater_id,language_id) VALUES ';
                    $arHandle = explode(',', $_POST['SendSms']['list_handle']);
                    if (User::model()->findByPk(Yii::app()->user->id)->remain_sms < count($arHandle) && !User::model()->findByPk(Yii::app()->user->id)->unlimit_sms)
                        $maxCount = User::model()->findByPk(Yii::app()->user->id)->remain_sms;
                    else
                        $maxCount = count($arHandle);


                    $re1 = '/^(20|30)[0-9]{7,8}$/';
                    for ($i = 0; $i < $maxCount; $i++) {
                        if (preg_match($re1, trim($arHandle[$i])))
                            $sql1 .= '("856' . trim($arHandle[$i]) . '","' . $model->sendby . '","' . $sender . '","' . $service_id . '","' . $content . '","' . $Time_Now . '","' . $status . '","' . Yii::app()->user->id . '","'.$model->language_id.'"), ';
                    }

                    if ($sql1 != '') {
                        $sql_after1 = substr($sql1, 0, -2);
                        $cmd1 = Yii::app()->db->createCommand($sql_before1 . $sql_after1);
                        $cmd1->execute();

                        if (User::model()->findByPk(Yii::app()->user->id)->unlimit_sms != '1') {
                            $sqlUpdate = 'UPDATE account SET remain_sms = remain_sms - ' . $maxCount . ' WHERE id=' . Yii::app()->user->id;
                            $upl = Yii::app()->db->createCommand($sqlUpdate)->execute();
                        }
                    }
                }

                if (isset($model->BillFile) && $model->BillFile) {
                    if ($model->BillFile->saveAs('data/billalert/' . $model->BillFile->getName())) {
                        $sql = '';
                        $sql_before = 'INSERT INTO sms_mt (receiver,sender,service,service_id,content,send_datetime,status,creater_id,language_id) VALUES ';
                        $ListBill = file_get_contents('data/billalert/' . $model->BillFile->getName());
                        if (file_get_contents('data/billalert/' . $model->BillFile->getName())) {
                            $nv = explode('>>', $ListBill);
                            
                            if (User::model()->findByPk(Yii::app()->user->id)->remain_sms < count($nv) && !User::model()->findByPk(Yii::app()->user->id)->unlimit_sms)
                                $maxCount = User::model()->findByPk(Yii::app()->user->id)->remain_sms;
                            else
                                $maxCount = count($nv);

                            if (count($nv) > 0) {
                                for ($j = 0; $j < $maxCount; $j++) {
                                    $arFull = explode('|', $nv[$j]);
                                    $re = '/^(20|30)[0-9]{7,8}$/';
                                    if (preg_match($re, trim($arFull[0])))
                                        $msisdn = trim($arFull[0]);
                                    if (isset($arFull[1]))
                                        $ct_bill = trim($arFull[1]);

                                    if (isset($msisdn) && isset($ct_bill) && $msisdn && $ct_bill)
                                        $rs_bill[] = '("856' . trim($msisdn) . '","' . $model->sendby . '","' . $sender . '","' . $service_id . '","' . $ct_bill . '","' . $Time_Now . '","' . $status . '","' . Yii::app()->user->id . '","'.$model->language_id.'")';
                                    unset($arFull);
                                    unset($ct_bill);
                                    unset($msisdn);
                                }
                            }

                            if (isset($rs_bill) && count($rs_bill) > 0) {
                                $cmd = Yii::app()->db->createCommand($sql_before . implode(',', $rs_bill));
                                $cmd->execute();

                                if (User::model()->findByPk(Yii::app()->user->id)->unlimit_sms != '1') {
                                    $sqlUpdate = 'UPDATE account SET remain_sms = remain_sms - ' . $maxCount . ' WHERE id=' . Yii::app()->user->id;
                                    $upl = Yii::app()->db->createCommand($sqlUpdate)->execute();
                                }
                            }
                        }
                    }
                }

                $this->redirect(Yii::app()->createUrl('/smsMt/admin'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function loadModel($id) {
        $model = SendSms::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'send-sms-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
