<?php

class ReportingController extends AccessController {

    public $layout = '//layouts/column2';
    public $module = 'reporting';
    public $filename = 'List_Reporting';

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionCreate() {
        $model = new Reporting;

        if (isset($_POST['Reporting'])) {
            $model->attributes = $_POST['Reporting'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        if (isset($_POST['Reporting'])) {
            $model->attributes = $_POST['Reporting'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        $this->loadModel($id)->delete();
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Reporting');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionAdmin() {
        $model = new Reporting('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Reporting']))
            $model->attributes = $_GET['Reporting'];
        
        if(Yii::app()->request->getParam('export')) {
            $this->exportData($model, array('time','serviceName','new_register','cancel_by_user','cancel_by_system','real_development','current_user','chargable_subs','monthly_fee_revenue'), $this->filename);
            Yii::app()->end();
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id) {
        $model = Reporting::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'reporting-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    public function actionExcel() {
        
        Yii::import('ext.yiiexcel.YiiExcel', true);
        Yii::registerAutoloader(array('YiiExcel', 'autoload'), true);
        PHPExcel_Shared_ZipStreamWrapper::register();
        if (ini_get('mbstring.func_overload') & 2) {
            throw new Exception('Multibyte function overloading in PHP must be disabled for string functions (2).');
        }
        PHPExcel_Shared_String::buildCharacterSets();
        $r = new YiiReport(array('template' => 'report.xlsx'));
        
        $month = Yii::app()->request->getParam("month");
        $year = Yii::app()->request->getParam("year");
        
	$services = Service::model()->findAll();
        $categories = Category::model()->findAll();
	$i = 1; $i4 = 4; $i3 = 3; $i2 = 2; $i5 = 5; $i6 = 6;
        $data = array();$item = array();
        $data[] = array(
            "stt" => $i,
            "name" => "New",
            "code" => "",
            "pz" => " price/unit",
            "count" => "Vas charge/time(kip)",
            "sub" => "",
            "total_vas" => "",
            "pre_time" => "",
            "pre_amount" => "",
            "pos_time" => "",
            "pos_amount" => "",
            "inhouse" => "",
            "total" => ""); $j = 1;
	$data2 = array();$item2 = array();
        $data2[] = array(
            "stt" => $i4,
            "name" => "Entertainment",
            "code" => "",
            "pz" => " ",
            "count" => "",
            "sub" => "",
            "total_vas" => "",
            "pre_time" => "",
            "pre_amount" => "",
            "pos_time" => "",
            "pos_amount" => "",
            "inhouse" => "",
            "total" => ""); $j4=1;
	$data3 = array();$item3 = array();
        $data3[] = array(
            "stt" => $i3,
            "name" => "Opinion",
            "code" => "",
            "pz" => " ",
            "count" => "",
            "sub" => "",
            "total_vas" => "",
            "pre_time" => "",
            "pre_amount" => "",
            "pos_time" => "",
            "pos_amount" => "",
            "inhouse" => "",
            "total" => ""); $j3 =1;
	$data4 = array();$item4 = array();
        $data4[] = array(
            "stt" => $i2,
            "name" => "Vote",
            "code" => "",
            "pz" => "",
            "count" => "",
            "sub" => "",
            "total_vas" => "",
            "pre_time" => "",
            "pre_amount" => "",
            "pos_time" => "",
            "pos_amount" => "",
            "inhouse" => "",
            "total" => ""); $j2=1;
	$data5 = array();$item5 = array();
        $data5[] = array(
            "stt" => $i5,
            "name" => "Bulk SMS",
            "code" => "",
            "pz" => " ",
            "count" => "",
            "sub" => "",
            "total_vas" => "",
            "pre_time" => "",
            "pre_amount" => "",
            "pos_time" => "",
            "pos_amount" => "",
            "inhouse" => "",
            "total" => ""); $j5=1;
	$data6 = array();$item6 = array();
        $data6[] = array(
            "stt" => $i6,
            "name" => "Enquiry",
            "code" => "",
            "pz" => " ",
            "count" => "",
            "sub" => "",
            "total_vas" => "",
            "pre_time" => "",
            "pre_amount" => "",
            "pos_time" => "",
            "pos_amount" => "",
            "inhouse" => "",
            "total" => "");$j6=1;
        
       foreach ($services as $service) {
            $date  = $year.'-'.$month;
            /* Tính count */
            $sqlCount = "select count from report_month as a, service as s where a.service_id = $service->id and a.time ='{$date}'";
            $count = Yii::app()->db->createCommand($sqlCount)->queryScalar();
            /* Tính sub */
            $sqlSub = "select sub from report_month as a, service as s where a.service_id = $service->id and a.time ='{$date}'";
            $sub = Yii::app()->db->createCommand($sqlSub)->queryScalar();
            /* Tính pre_time */
            $sqlPreT = "select pre_time from report_month as a, service as s where a.service_id = $service->id and a.time ='{$date}'";
            $preTime = Yii::app()->db->createCommand($sqlPreT)->queryScalar();
            /* Tính pre_amount */
            $sqlPreA = "select pre_amount from report_month as a, service as s where a.service_id = $service->id  and a.time ='{$date}'";
            $preAmount = Yii::app()->db->createCommand($sqlPreA)->queryScalar();
            /* Tính pos_time */
            $sqlPosT = "select pos_time from report_month as a, service as s where a.service_id = $service->id  and a.time ='{$date}'";
            $posTime = Yii::app()->db->createCommand($sqlPosT)->queryScalar();
            /* Tính pos_amount */
            $sqlPosA = "select pos_amount from report_month as a, service as s where a.service_id = $service->id  and a.time ='{$date}'";
            $posAmount = Yii::app()->db->createCommand($sqlPosA)->queryScalar();
				
            if ($service->category_id == 1) {
                $item = array(
                "stt" => $i . "." . $j,
                "name" => $service->service,
                "code" => $service->shortcode,
                "pz" => "",
                "count" => "",
                "sub" => "",
                "total_vas" => "",
                "pre_time" => "",
                "pre_amount" => "",
                "pos_time" => "",
                "pos_amount" => "",
                "inhouse" => "",
                "total" => "");
                $j++;
                if ($service->type == 1 || $service->type == 2 || $service->type == 3) {
                    $item["count"] = $service->price;
                    $item["sub"] = number_format($sub);
                    $item['pz'] = number_format($item['count'] * 30);
                    $item["pre_time"] = number_format($preTime);
                    $item["pre_amount"] = number_format($preAmount);
                    $item["pos_time"] = number_format($posTime);
                    $item["pos_amount"] = number_format($posAmount);
                    $item["total_vas"] = number_format($sub * $service->price );
                    $item["total"] = number_format($preAmount + $posAmount);
                } 
                $data[] = $item;
            }
            if ($service->category_id == 4) {
                $item4 = array(
                "stt" => $i2 . "." . $j2,
                "name" => $service->service,
                "code" => $service->shortcode,
                "pz" => $service->price,
                "count" => "",
                "sub" => "",
                "total_vas" => "",
                "pre_time" => "",
                "pre_amount" => "",
                "pos_time" => "",
                "pos_amount" => "",
                "inhouse" => "",
                "total" => "");
                $j2++;
                $item4["sub"] = number_format($sub);
                $item4["count"] = number_format($count);
                $item4["pre_time"] = number_format($preTime);
                $item4["pre_amount"] = number_format($preAmount);
                $item4["pos_time"] = number_format($posTime);
                $item4["pos_amount"] = number_format($posAmount);
                $item4["total_vas"] = number_format($service->price * $count);
                $item4["total"] = number_format($preAmount + $posAmount);
                $data4[] = $item4;
            }
            if ($service->category_id == 3) {
                $item3 = array(
                "stt" => $i3 . "." . $j3,
                "name" => $service->service,
                "code" => $service->shortcode,
                "pz" => $service->price,
                "count" => "",
                "sub" => "",
                "total_vas" => "",
                "pre_time" => "",
                "pre_amount" => "",
                "pos_time" => "",
                "pos_amount" => "",
                "inhouse" => "",
                "total" => "");
                $j3++;
                $item3["sub"] = number_format($sub);
                $item3["count"] = number_format($count);
                $item3["pre_time"] = number_format($preTime);
                $item3["pre_amount"] = number_format($preAmount);
                $item3["pos_time"] = number_format($posTime);
                $item3["pos_amount"] = number_format($posAmount);
                $item3["total_vas"] = number_format($service->price * $count);
                $item3["total"] = number_format($preAmount + $posAmount);
                $data3[] = $item3;
            }
            if ($service->category_id == 2) {
                $item2 = array(
                "stt" => $i4 . "." . $j4,
                "name" => $service->service,
                "code" => $service->shortcode,
                "pz" => $service->price,
                "count" => "",
                "sub" => "",
                "total_vas" => "",
                "pre_time" => "",
                "pre_amount" => "",
                "pos_time" => "",
                "pos_amount" => "",
                "inhouse" => "",
                "total" => "");
                $j4++;
                $item2["sub"] = number_format($sub);
                $item2["count"] = number_format($count);
                $item2["pre_time"] = number_format($preTime);
                $item2["pre_amount"] = number_format($preAmount);
                $item2["pos_time"] = number_format($posTime);
                $item2["pos_amount"] = number_format($posAmount);
                $item2["total_vas"] = number_format($service->price * $count);
                $item2["total"] = number_format($preAmount + $posAmount);
                $data2[] = $item2;
            }
            if ($service->category_id == 5) {
                $item5 = array(
                "stt" => $i5 . "." . $j5,
                "name" => $service->service,
                "code" => $service->shortcode,
                "pz" => $service->price,
                "count" => "",
                "sub" => "",
                "total_vas" => "",
                "pre_time" => "",
                "pre_amount" => "",
                "pos_time" => "",
                "pos_amount" => "",
                "inhouse" => "",
                "total" => "");
                $j5++;
               
                $item5["sub"] = number_format($sub);
                $item5["count"] = number_format($count);
                $item5["pre_time"] = number_format($preTime);
                $item5["pre_amount"] = number_format($preAmount);
                $item5["pos_time"] = number_format($posTime);
                $item5["pos_amount"] = number_format($posAmount);
                $item5["total_vas"] = number_format($service->price * $count);
                $item5["total"] = number_format($preAmount + $posAmount);
                $data5[] = $item5;
            }
            if ($service->category_id == 6) {
                $item6 = array(
                "stt" => $i6 . "." . $j6,
                "name" => $service->service,
                "code" => $service->shortcode,
                "pz" => $service->price,
                "count" => "",
                "sub" => "",
                "total_vas" => "",
                "pre_time" => "",
                "pre_amount" => "",
                "pos_time" => "",
                "pos_amount" => "",
                "inhouse" => "",
                "total" => "");
                $j6++;
                $item6["sub"] = number_format($sub);
                $item6["count"] = number_format($count);
                $item6["pre_time"] = number_format($preTime);
                $item6["pre_amount"] = number_format($preAmount);
                $item6["pos_time"] = number_format($posTime);
                $item6["pos_amount"] = number_format($posAmount);
                $item6["total_vas"] = number_format($service->price * $count);
                $item6["total"] = number_format($preAmount + $posAmount);
                $data6[] = $item6;
            }
        }
        $i++;$i2++;$i3++;$i4++;$i5++;$i6++;
        $datat1 = array();$itemt1 = array();
        $datat2 = array();$itemt2 = array();
        $datat3 = array();$itemt3 = array();
        $datat4 = array();$itemt4 = array();
        $datat5 = array();$itemt5 = array();
        $datat6 = array();$itemt6 = array();
        foreach($categories as $category){
            /* Tinh total pre time */
            $sqlTotalPreT = "select sum(pre_time) from report_month as a, service as s where s.category_id = $category->id and  s.id = a.service_id and a.time ='{$date}'";
            $totalPreT = Yii::app()->db->createCommand($sqlTotalPreT)->queryScalar();
            /* Tinh total pre amount */
            $sqlTotalPreA = "select sum(pre_amount) from report_month as a, service as s where s.category_id = $category->id and s.id = a.service_id and a.time ='{$date}'";
            $totalPreA = Yii::app()->db->createCommand($sqlTotalPreA)->queryScalar();
            /* Tinh total pos time */
            $sqlTotalPosT = "select sum(pos_time) from report_month as a, service as s where s.category_id = $category->id and s.id = a.service_id and a.time ='{$date}'";
            $totalPosT = Yii::app()->db->createCommand($sqlTotalPosT)->queryScalar();
            /* Tinh total pos amount */
            $sqlTotalPosA = "select sum(pos_amount) from report_month as a, service as s where s.category_id =$category->id and s.id = a.service_id and a.time ='{$date}'";
            $totalPosA = Yii::app()->db->createCommand($sqlTotalPosA)->queryScalar();
            /* Tính total*/
            if($category->id == 1){
                $itemt1["total_pre_time"] = number_format($totalPreT);
                $itemt1["total_pre_amount"] = number_format($totalPreA);
                $itemt1["total_pos_time"] = number_format($totalPosT);
                $itemt1["total_pos_amount"] = number_format($totalPosA);
                $datat1[] = $itemt1;
            }
            if($category->id == 2){
                $itemt2["total_pre_time"] = number_format($totalPreT);
                $itemt2["total_pre_amount"] = number_format($totalPreA);
                $itemt2["total_pos_time"] = number_format($totalPosT);
                $itemt2["total_pos_amount"] = number_format($totalPosA);
                $datat2[] = $itemt2;
            }
            if($category->id == 3){
                $itemt3["total_pre_time"] = number_format($totalPreT);
                $itemt3["total_pre_amount"] = number_format($totalPreA);
                $itemt3["total_pos_time"] = number_format($totalPosT);
                $itemt3["total_pos_amount"] = number_format($totalPosA);
                $datat3[] = $itemt3;
            }
            if($category->id == 4){
                $itemt4["total_pre_time"] = number_format($totalPreT);
                $itemt4["total_pre_amount"] = number_format($totalPreA);
                $itemt4["total_pos_time"] = number_format($totalPosT);
                $itemt4["total_pos_amount"] = number_format($totalPosA);
                $datat4[] = $itemt4;
            }
            if($category->id == 5){
                $itemt5["total_pre_time"] = number_format($totalPreT);
                $itemt5["total_pre_amount"] = number_format($totalPreA);
                $itemt5["total_pos_time"] = number_format($totalPosT);
                $itemt5["total_pos_amount"] = number_format($totalPosA);
                $datat5[] = $itemt5;   
            }
            if($category->id == 6){
                $itemt6["total_pre_time"] = number_format($totalPreT);
                $itemt6["total_pre_amount"] = number_format($totalPreA);
                $itemt6["total_pos_time"] = number_format($totalPosT);
                $itemt6["total_pos_amount"] = number_format($totalPosA);
                $datat6[] = $itemt6;            
            }
        }
        $r->load(
                array(
                    array(
                        'id' => 'main',
                        'data' => array(
                            'name' => $month.' - '.$year
                        )
                    ),
                    array(
                        'id' => 'data',
                        'repeat' => true,
                        'data' => $data
                    ),
                    array(
                        'id' => 'data4',
                        'repeat' => true,
                        'data' => $data4
                    ),
                    array(
                        'id' => 'data3',
                        'repeat' => true,
                        'data' => $data3
                    ),
                    array(
                        'id' => 'data2',
                        'repeat' => true,
                        'data' => $data2
                    ),
                    array(
                        'id' => 'data5',
                        'repeat' => true,
                        'data' => $data5
                    ),
                    array(
                        'id' => 'data6',
                        'repeat' => true,
                        'data' => $data6
                    ),
                    array(
                        'id' => 'datat1',
                        'repeat' => true,
                        'data' => $datat1
                    ),
                    array(
                        'id' => 'datat2',
                        'repeat' => true,
                        'data' => $datat2
                    ),
                    array(
                        'id' => 'datat3',
                        'repeat' => true,
                        'data' => $datat3
                    ),
                    array(
                        'id' => 'datat4',
                        'repeat' => true,
                        'data' => $datat4
                    ),
                    array(
                        'id' => 'datat5',
                        'repeat' => true,
                        'data' => $datat5
                    ),
                    array(
                        'id' => 'datat6',
                        'repeat' => true,
                        'data' => $datat6
                    ),
                    
                  )  
        );
        echo $r->render('excel2007', 'report');
    }
}
