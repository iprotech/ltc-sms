<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TestController
 *
 * @author BUI VAN UY
 */
class TestController extends CController {

    //put your code here

    public $layout = FALSE;

    public function actionIndex() {
        $form = new CActiveForm();
        $form->method = "POST";
        $form->action = $this->createUrl("test/post");
        $this->render("index", array("form" => $form));
    }

    public function actionPost() {
        if (isset($_POST["username"])) {
            Yii::app()->cache->delete("chat");
            Yii::app()->cache->add("chat", array(
                "name" => $_POST["username"],
                "msg" => $_POST["msg"],
                "time" => time()
            ));
            echo json_encode(array("status" => TRUE));
            return;
        }
        echo json_encode(array("status" => FALSE));
    }

    public function actionChat() {
        $time = intval($_REQUEST["time"]);

        $response = array();
        $response["status"] = true;
        $response["data"] = $this->getData($time);
        $response["count"] = count($response["data"]);
        $response["time"] = $time;
        echo json_encode($response);
        return;
    }

    public function actionLast() {
        print_r(Yii::app()->cache->get("chat"));
    }

    private function getData(&$time) {
        $data = NULL;

        $i = 0;
        while ($i < 200) {
            $data = Yii::app()->cache->get("chat");
            if ($data && gettype($data) == "array" && intval($data["time"]) > $time) {
                break;
            }

            usleep(100000);
            $i++;
        }

        if ($i == 200) {
            return null;
        }

        $time = intval($data["time"]);

        $result = array();
        $result[] = $data;
        return $result;
    }

    public function actionSaveData($val) {
        Yii::app()->cache->set("test", $val);
    }
    
    public function actionGetData() {
        echo Yii::app()->cache->get("test");
    }
    
}
