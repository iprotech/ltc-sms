<?php

class MemberController extends AccessController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';
    public $module = 'member';
    public $filename = 'Report_Member';

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionImport() {
        $model = new Member;
	$rs = array();
        if (isset($_POST['Member'])) {
            $model->service_id = $_POST['Member']['service_id'];
            $model->fileMember = CUploadedFile::getInstance($model, 'list_member');
            //if ($model->validate()) {
            if (isset($model->fileMember) && $model->fileMember) {
                if ($model->fileMember->saveAs('data/import/' . $model->fileMember->getName())) {
                    $sql = '';
                    $sql_before = 'INSERT INTO member (msisdn,register_date,service_id,status,end_charging_date) VALUES ';
                    $ListMember = file('/var/www/html/data/import/' . $model->fileMember->getName());
                    if (file_get_contents('/var/www/html/data/import/' . $model->fileMember->getName())) {
                        $reg = '/^(20)[0-9]{7,8}$/';
                        $reg1 = '/^\d{4}\-[0-9]{2}-[0-9]{2}\s[0-9]{2}\:[0-9]{2}:[0-9]{2}$/';

                        foreach ($ListMember as $i) {
                            $nv = explode(',', $i);
                            if (count($nv) > 1) {
                                if (preg_match($reg, $nv[0]))
                                    $msisdn = $nv[0];
                                if (preg_match($reg1, trim($nv[1])))
                                    $Time_Reg = trim($nv[1]);
                                else
                                    $Time_Reg = date('Y-m-d H:i:s', time());
                            } else {
                                if (preg_match($reg, $i))
                                    $msisdn = $i;
                                $Time_Reg = date('Y-m-d H:i:s', time());
                            }
                            if (isset($msisdn) && isset($Time_Reg) && $msisdn && $Time_Reg)
                                if (!Member::model()->findByAttributes(array('msisdn' => '856' . $msisdn, 'status' => 1, 'service_id' => $model->service_id))){
                                    if(!in_array($msisdn, $rs)){
                                        $rsValue[] = '("856' . $msisdn . '","' . $Time_Reg . '",' . $model->service_id . ',1,now())';
                                        $rs[] = $msisdn;
                                    }
                                }
                        }
                        if (isset($rsValue) && count($rsValue) > 0) {
                            $cmd = Yii::app()->db->createCommand($sql_before . implode(',', $rsValue));
                            $cmd->execute();
                        }
                    }
                }
                $this->redirect(array('member/admin'));            }
            //}
        }
        $this->render('import', array(
            'model' => $model,
        ));
    }

    public function actionCreate() {
        $model = new Member;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Member'])) {
            $model->register_date = $_SERVER['REQUEST_TIME'];
            $model->attributes = $_POST['Member'];
            $model->register_date = date('Y-m-d H:i:s', time());
            $model->register_type = 1;
            $model->status = 1;
            $model->charging_status = 0;
           
            $model->msisdn = '856' . $_POST['Member']['msisdn'] . '';
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }
        $this->render('create', array(
            'model' => $model,
        ));
    }
    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Member'])) {
            $model->attributes = $_POST['Member'];
            if (isset($_POST['Member']['charging_status'])) {
                $model->charging_status = $_POST['Member']['charging_status'];
            }

            if ($model->status == 3 || $model->status == 4 || $model->status == 5)
                $model->cancel_date = date('Y-m-d H:i:s', time());
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Member');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Member('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Member']))
            $model->attributes = $_GET['Member'];

        //if (Yii::app()->request->getParam('export')) {
	    //$this->exportData($model, array('msisdn', 'serviceName', 'register_date', 'cancel_date', 'status', 'sub_type'), $this->filename);
        //   Yii::app()->end();
        //}
		if(Yii::app()->request->getParam('export')) {
            $model->search();
			echo $model->export_id;
            while(1){
               $sql = "select status from command_query where id = $model->export_id ";
                $cmd = Yii::app()->db->createCommand($sql)->queryScalar();
                $file = "export/Report_Member"."_".$model->export_id."."."csv";
                if($cmd == 2){
                    $this->redirect($file);
                }else{
                    sleep(5);
                }
                //if($i++>3) break;
            }
            exit();

        }
        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Member::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'member-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
