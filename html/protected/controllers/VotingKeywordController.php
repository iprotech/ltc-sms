<?php

class VotingKeywordController extends AccessController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $module = 'votingKeyword';
    public $filename = 'Report_Voting';

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new VotingKeyword('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['VotingKeyword'])) {
            $model->setAttributes($_GET['VotingKeyword']);
            if (isset($_GET['VotingKeyword']['time_start'])) {
                $model->setAttribute('time_start', $_GET['VotingKeyword']['time_start']);
            }
            if (isset($_GET['VotingKeyword']['time_end'])) {
                $model->setAttribute('time_end', $_GET['VotingKeyword']['time_end']);
            }
        }

        if(Yii::app()->request->getParam('export')) {
            $model->search();
            while(1){
               $sql = "select status from command_query where  id = $model->export_id ";
                $cmd = Yii::app()->db->createCommand($sql)->queryScalar();
                $file = "export/Report_Voting"."_".$model->export_id."."."csv";
                if($cmd == 2){
                    $this->redirect($file);
                }else{
                    sleep(5);
                }
//                if($i++>3) break;
            }
            exit();

        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionDraw() {
        $this->layout = '//layouts/column3';
        Yii::import('application.extensions.jqBarGraph.*');
        $model = new VotingKeyword('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['VotingKeyword']))
            $model->attributes = $_GET['VotingKeyword'];

        $this->render('chart', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = VotingKeyword::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'voting-keyword-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
