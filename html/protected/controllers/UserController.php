<?php

class UserController extends AccessController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $module = 'user';

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new User;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];

            $model->moduleSelected = $_POST['User']['moduleSelected'];
            $model->unlimit_sms = $_POST['User']['unlimit_sms'];

            if (isset($_POST['User']['total_sms']))
                $model->total_sms = $_POST['User']['total_sms'];
            if (isset($_POST['User']['remain_sms']))
                $model->remain_sms = $_POST['User']['remain_sms'];

            if ($model->group_id == 1) {
                $cmd = Yii::app()->db->createCommand('SELECT id FROM service');
                $data = $cmd->queryAll();
                foreach ($data as $v) {
                    $model->service .= '[' . $v['id'] . ']';
                }
            } else {
                $arCate = array();
                if (isset($_POST['User']['CateSelected']) && $_POST['User']['CateSelected']) {
                    $arCate = $_POST['User']['CateSelected'];
                    $FULL   = '';
                    foreach ($arCate as $i) {
                        $cmd1 = Yii::app()->db->createCommand('SELECT id FROM service WHERE category_id='.$i);
                        $data1 = $cmd1->queryAll();
                        foreach ($data1 as $v1) {
                            $FULL .= '[' . $v1['id'] . ']';
                        }
                    }
                    $model->service = $FULL;
                } else {
                    if (isset($_POST['User']['servicesSelect']) && $_POST['User']['servicesSelect'])
                        $model->service = implode('', $_POST['User']['servicesSelect']);
                }
            }
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        $test = array();
        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            $model->moduleSelected = $_POST['User']['moduleSelected'];

            $model->unlimit_sms = $_POST['User']['unlimit_sms'];

            if (isset($_POST['User']['total_sms']))
                $model->total_sms = $_POST['User']['total_sms'];
            if (isset($_POST['User']['remain_sms']))
                $model->remain_sms = $_POST['User']['remain_sms'];

            if ($model->group_id == 1) {
                $FULL = '';
                $cmd = Yii::app()->db->createCommand('SELECT id FROM service');
                $data = $cmd->queryAll();
                foreach ($data as $v) {
                    $FULL .= '[' . $v['id'] . ']';
                }
                $model->service = $FULL;
            } else {
                $arCate = array();
                if (isset($_POST['User']['CateSelected']) && $_POST['User']['CateSelected']) {
                    $arCate = $_POST['User']['CateSelected'];
                    $FULL   = '';
                    foreach ($arCate as $i) {
                        $cmd1 = Yii::app()->db->createCommand('SELECT id FROM service WHERE category_id='.$i);
                        $data1 = $cmd1->queryAll();
                        foreach ($data1 as $v1) {
                            $FULL .= '[' . $v1['id'] . ']';
                        }
                    }
                    $model->service = $FULL;
                } else {
                    if (isset($_POST['User']['servicesSelect']) && $_POST['User']['servicesSelect'])
                        $model->service = implode('', $_POST['User']['servicesSelect']);
                }
            }

            if (isset($_POST['User']['pass_new']) && $_POST['User']['pass_new'])
                $model->password = md5($_POST['User']['pass_new']);
            else
                $model->password = $model->pass_old;
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('User');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new User('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['User']))
            $model->attributes = $_GET['User'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = User::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
