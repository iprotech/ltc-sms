<?php

class ChargingHistoryController extends AccessController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';
    public $module = 'chargingHistory';
    public $filename = 'Report_Revenue';

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new chargingHistory('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['chargingHistory'])) {
            $model->attributes = $_GET['chargingHistory'];
        }

        if (Yii::app()->request->getParam('export')) {
            $this->exportData($model, array('msisdn', 'total_money', 'start_charging_date', 'service_name', 'sub_type_name', 'shortcode','keyword_name','keyword_total'), $this->filename);
            Yii::app()->end();
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = chargingHistory::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

}
